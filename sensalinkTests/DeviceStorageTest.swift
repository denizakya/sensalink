//
//  DeviceStorageTest.swift
//  sensalinkTests
//
//  Created by Deni Zakya on 03/05/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import XCTest

@testable import sensalink

final class DeviceStorageTest: XCTestCase {

    func testInsertAndGet() {
        let deviceStorage = DeviceStorage(inMemory: true)
        let newUUID = UUID().uuidString

        let id = deviceStorage.insert(uuid: newUUID, name: "name1")

        let deviceModel = deviceStorage.get(uuid: newUUID)!
        XCTAssertEqual(deviceModel.uuid, newUUID)
        XCTAssertEqual(deviceModel.id, id)
        XCTAssertEqual(deviceModel.name, "name1")
    }

    func testInsertAndUpdate() {
        let deviceStorage = DeviceStorage(inMemory: true)
        let newUUID = UUID().uuidString

        let id = deviceStorage.insert(uuid: newUUID, name: "name1")
        deviceStorage.update(name: "name2", of: id)

        let deviceModel = deviceStorage.get(uuid: newUUID)!
        XCTAssertEqual(deviceModel.uuid, newUUID)
        XCTAssertEqual(deviceModel.id, id)
        XCTAssertEqual(deviceModel.name, "name2")
    }

}
