//
//  SQLiteWrapperTest.swift
//  sensalinkTests
//
//  Created by Deni Zakya on 03/05/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import XCTest
import SQLite

@testable import sensalink

final class SQLiteWrapperTest: XCTestCase {

    private let charUUIDString = "0cd4acb2-9575-40d5-bb70-374969a22773"

    func testPrepareAndInsertDevice() {
        let dbWrapper = SQLiteWrapper(inMemory: true)!
        let deviceUUIDString = UUID().uuidString
        let device = Table("devices")
        let uuid = Expression<String>("uuid")
        let name = Expression<String>("name")
        let insert = device.insert(uuid <- deviceUUIDString,
                                   name <- "bluetooth1")
        let rowid = try! dbWrapper.insert(row: insert)

        let rows = try! dbWrapper.getRows(table: device)
        let row = rows.last!

        XCTAssertEqual(row[uuid], deviceUUIDString)
        XCTAssertEqual(rowid, 1)
    }

    func testPrepareAndInsertCharacteristic() {
        let dbWrapper = SQLiteWrapper(inMemory: true)!
        let deviceId = 1
        let valueOfBlob = Blob(bytes: [0x00, 0x00, 0x95, 0x43])
        let characteristic = Table("characteristics")
        let charUUID = Expression<String>("uuid")
        let deviceIdColumn = Expression<Int>("device_id")
        let insertChar = characteristic.insert(charUUID <- charUUIDString)
        let characteristicId = try! dbWrapper.insert(row: insertChar)

        let characteristicValues = Table("characteristic_values")
        let characteristicIdColumn = Expression<Int64>("characteristic_id")
        let value = Expression<Blob>("value")
        let updateAt = Expression<Double>("update_at")
        let insertCharValue = characteristicValues.insert(characteristicIdColumn <- characteristicId,
                                                          value <- valueOfBlob,
                                                          updateAt <- Date().timeIntervalSince1970,
                                                          deviceIdColumn <- deviceId)
        let _ = try! dbWrapper.insert(row: insertCharValue)

        let rows = try! dbWrapper.getRows(table: characteristicValues.where(characteristicIdColumn == characteristicId))
        let row = rows.last!

        XCTAssertEqual(row[characteristicIdColumn], characteristicId)
        XCTAssertEqual(row[value], valueOfBlob)
    }

}
