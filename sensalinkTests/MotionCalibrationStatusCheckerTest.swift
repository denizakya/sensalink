//
//  MotionCalibrationStatusCheckerTest.swift
//  sensalinkTests
//
//  Created by Deni Zakya on 03/06/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import XCTest

@testable import sensalink

final class MotionCalibrationStatusCheckerTest: XCTestCase {

    let charUUID = "21142b37-72b6-4564-a1d1-3f1c62609a6f"

    func testNeedToCalibrate() {
        let checker = MotionCalibrationStatusChecker(charUUID: charUUID)
        let data = Data([0x00, 0x00, 0x00, 0x00])

        let needCalibration = checker.needCalibration(charUUID: charUUID, data: data)

        XCTAssertTrue(needCalibration)
    }

    func testAlreadyCalibrated() {
        let checker = MotionCalibrationStatusChecker(charUUID: charUUID)
        let data = Data([0x03, 0x00, 0x00, 0x00])

        let needCalibration = checker.needCalibration(charUUID: charUUID, data: data)

        XCTAssertFalse(needCalibration)
    }

    func testCalibrateWhenCharUUIDInvalid() {
        let checker = MotionCalibrationStatusChecker(charUUID: charUUID)
        let data = Data([0x00, 0x00, 0x00, 0x00])

        let needCalibration = checker.needCalibration(charUUID: "invalid-uuid", data: data)

        XCTAssertFalse(needCalibration)
    }

}
