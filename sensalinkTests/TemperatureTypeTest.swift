//
//  TemperatureTypeTest.swift
//  sensalinkTests
//
//  Created by Deni Zakya on 14/02/21.
//  Copyright © 2021 sensa. All rights reserved.
//

import XCTest

@testable import sensalink

final class TemperatureTypeTest: XCTestCase {

    func testTemperatureTypeRTD() {
        let tempType = TemperatureType("RTD")
        
        XCTAssertEqual(tempType, .RTD)
    }
    
    func testTemperatureTypeTYK() {
        let tempType = TemperatureType("TYK")
        
        XCTAssertEqual(tempType, .TYK)
    }

}
