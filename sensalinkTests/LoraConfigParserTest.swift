//
//  LoraConfigParserTest.swift
//  sensalinkTests
//
//  Created by Deni Zakya on 21/06/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import XCTest

@testable import sensalink

class LoraConfigParserTest: XCTestCase {

//    Byte 0 = Data Rate (uint8,  range region dependant, see LoRaWAN spec)
//    Byte 1 = Adaptive Data Rate (uint8, 0=off 1=on)
//    Byte 2 = TX Power (uint8, range region dependant, see LoRaWAN spec)
//    Byte 3 = Transmission Retries (uint8)
//    Byte 4 = Lora mode (uint8, 0=OTAA 1=ABP)
//    Byte 5 = use config applied by app (uint8, 0=no, 1=yes)
//    Byte 6 = Ack message (uint8, 0=no, 1=yes)
//    Byte 7 = Network public (uint8, 0=no, 1=yes)
//    Byte 8 = App port (uint8, 1-254)
//    Byte 9 - 10 = Number of Frames per Day (uint16 - little endian)

    func testParse() {
        let data = Data([0x01, 0x01, 0x0A, 0x03, 0x01, 0x01, 0x01, 0x01, 0x0A, 0x0A, 0x00, 0x01, 0x01])
        let expectedResult = LoraConfigInfo(dataRate: 1,
                                            adaptiveDatarate: true,
                                            txPower: 10,
                                            transmissionRetries: 3,
                                            loraMode: .ABP,
                                            useConfigAppliedByApp: true,
                                            ackMessage: true,
                                            networkPublic: true,
                                            appPort: 10,
                                            numberOfFramePerDay: 10,
                                            dutyCycle: true,
                                            switchLora: true)

        let actualResult = LoraConfigParser.parse(data)

        XCTAssertEqual(expectedResult, actualResult)
    }

    func testToData() {
        let expectedData = Data([0x01, 0x01, 0x0A, 0x03, 0x01, 0x01, 0x01, 0x01, 0x0A, 0x0A, 0x00, 0x01, 0x01])
        let info = LoraConfigInfo(dataRate: 1,
                                  adaptiveDatarate: true,
                                  txPower: 10,
                                  transmissionRetries: 3,
                                  loraMode: .ABP,
                                  useConfigAppliedByApp: true,
                                  ackMessage: true,
                                  networkPublic: true,
                                  appPort: 10,
                                  numberOfFramePerDay: 10,
                                  dutyCycle: true,
                                  switchLora: true)

        let actualResult = info.toData()

        XCTAssertEqual(actualResult, expectedData)
    }
}
