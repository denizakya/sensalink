//
//  DataUtilTest.swift
//  sensalinkTests
//
//  Created by Deni Zakya on 22/04/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import XCTest

@testable import sensalink

final class DataUtilTest: XCTestCase {

    func testToFloat() {
        let data = Data([0x00, 0x00, 0x95, 0x43])

        let floatFromData = data.toFloat()!

        XCTAssertEqual(Int(floatFromData), 298)
    }

    func testToUInt8() {
        let data = Data([0x62])

        let uintFromData = data.toUInt8()!

        XCTAssertEqual(uintFromData, 98)
    }

    func testToUInt16() {
        let data = Data([0x0A, 0x00])

        let uintFromData = data.toUInt16()!

        XCTAssertEqual(uintFromData, 10)
    }

    func testToUInt64() {
        let data = Data([0xA4, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])

        let uintFromData = data.toUInt64()!

        XCTAssertEqual(uintFromData, 932)
    }

    func testToString() {
        let data = Data([0x53, 0x47, 0x31, 0x38])

        let stringFromData = data.toString(encoding: .utf8)

        XCTAssertEqual(stringFromData, "SG18")
    }

    func testToHexString() {
        let data = Data([0x11, 0x99])

        let string = data.toHexString()

        XCTAssertEqual(string, "1199")
    }

    func testToInt16() {
        let data = Data([0x01, 0x00])

        let intFromData = data.toInt16()

        XCTAssertEqual(intFromData, 1)
    }

    func testFromFloat() {
        let value: Float = 3.5
        let expectedData = Data([0x00, 0x00, 0x60, 0x40])

        let floatFromData = Data.from(value)

        XCTAssertEqual(floatFromData, expectedData)
    }

    func testFromUInt16() {
        let value: UInt16 = 1
        let expectedData = Data([0x01, 0x00])

        let dataResult = Data.from(value)

        XCTAssertEqual(dataResult, expectedData)
    }

    func testReplaced() {
        var data = Data([0x00, 0x00])
        let replacedData = Data([0x01])
        let offset = 1

        data.replaced(with: replacedData, from: offset)

        XCTAssertEqual(Data([0x00, 0x01]), data)

    }

    func testHexToData() {
        let expectedData = Data([0x21, 0x11])
        let string = "2111"

        let actualData = Data(hex: string)

        XCTAssertEqual(actualData, expectedData)
    }

    func testDataToHex() {
        let expectedHex = "2111"
        let data = Data([0x21, 0x11])

        let actualHex = data.toHexString()

        XCTAssertEqual(actualHex, expectedHex)
    }
}
