//
//  ValveTurnDetectionProcessorTest.swift
//  sensalinkTests
//
//  Created by Deni Zakya on 03/07/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import XCTest

@testable import sensalink

final class ValveTurnDetectionProcessorTest: XCTestCase {

    func testToString2AndQuarter() {
        let expectedResult = "2 1/4"
        let totalAngle: Float = 810.0

        let valveTurnDetectionProcessor = ValveTurnDetectionProcessor(totalAngle: totalAngle)
        let actualResult = valveTurnDetectionProcessor.toString()

        XCTAssertEqual(actualResult, expectedResult)
    }

    func testToStringHalf() {
        let expectedResult = "1/2"
        let totalAngle: Float = 180.0

        let valveTurnDetectionProcessor = ValveTurnDetectionProcessor(totalAngle: totalAngle)
        let actualResult = valveTurnDetectionProcessor.toString()

        XCTAssertEqual(actualResult, expectedResult)
    }

    func testToStringQuarter() {
        let expectedResult = "1/4"
        let totalAngle: Float = 90.0

        let valveTurnDetectionProcessor = ValveTurnDetectionProcessor(totalAngle: totalAngle)
        let actualResult = valveTurnDetectionProcessor.toString()

        XCTAssertEqual(actualResult, expectedResult)
    }

    func testToString2Quarter() {
        let expectedResult = "3/4"
        let totalAngle: Float = 270.0

        let valveTurnDetectionProcessor = ValveTurnDetectionProcessor(totalAngle: totalAngle)
        let actualResult = valveTurnDetectionProcessor.toString()

        XCTAssertEqual(actualResult, expectedResult)
    }

    func testToString1() {
        let expectedResult = "1"
        let totalAngle: Float = 350

        let valveTurnDetectionProcessor = ValveTurnDetectionProcessor(totalAngle: totalAngle)
        let actualResult = valveTurnDetectionProcessor.toString()

        XCTAssertEqual(actualResult, expectedResult)
    }

    func testToString2() {
        let expectedResult = "2"
        let totalAngle: Float = 700

        let valveTurnDetectionProcessor = ValveTurnDetectionProcessor(totalAngle: totalAngle)
        let actualResult = valveTurnDetectionProcessor.toString()

        XCTAssertEqual(actualResult, expectedResult)
    }

    func testGetTurnValue() {
        let expectedResult: Float = 2.25
        let totalAngle: Float = 810.0

        let valveTurnDetectionProcessor = ValveTurnDetectionProcessor(totalAngle: totalAngle)
        let actualResult = valveTurnDetectionProcessor.getTurnValue()

        XCTAssertEqual(actualResult, expectedResult)
    }

}
