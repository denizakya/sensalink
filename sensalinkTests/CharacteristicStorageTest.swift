//
//  CharacteristicStorageTest.swift
//  sensalinkTests
//
//  Created by Deni Zakya on 03/05/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import XCTest
@testable import sensalink

class CharacteristicStorageTest: XCTestCase {

    func testInsertAndGet() {
        let characteristicStorage = CharacteristicStorage(inMemory: true)
        let newUUID = UUID().uuidString
        let deviceId: Int64 = 1
        let date = Date()
        let data = Data([0x00, 0x00, 0x95, 0x43])
        let model = CharacteristicModel(uuid: newUUID,
                                        id: 0,
                                        deviceId: deviceId,
                                        updateAt: date,
                                        value: data)

        characteristicStorage.insert(model)

        let characteristicModel = characteristicStorage.getLatestRow(uuid: newUUID, deviceId: deviceId)
        XCTAssertEqual(characteristicModel?.value, data)
    }

    func testMultipleInsertAndGet() {
        let dateInterval = Date().timeIntervalSince1970 - 3
        let characteristicStorage = CharacteristicStorage(inMemory: true)
        let data = Data([0x00, 0x00, 0x95, 0x43])
        let newUUID = UUID().uuidString
        let models = (1...2).map { index -> CharacteristicModel in
            let deviceId: Int64 = 1
            return CharacteristicModel(uuid: newUUID,
                                       id: Int64(index),
                                       deviceId: deviceId,
                                       updateAt: Date(timeIntervalSince1970: dateInterval + Double(index)),
                                       value: data)
        }

        for model in models {
            characteristicStorage.insert(model)
        }

        let characteristicModel = characteristicStorage.get(uuid: newUUID,
                                                            deviceId: 1,
                                                            after: Date(timeIntervalSince1970: dateInterval))

        XCTAssertEqual(characteristicModel.count, 2)
        XCTAssertEqual(characteristicModel.first?.value, data)
    }

    func testMultipleInsertAndGetLatest() {
        let characteristicStorage = CharacteristicStorage(inMemory: true)
        let data = Data([0x00, 0x00, 0x95, 0x43])
        let newUUID = UUID().uuidString
        let models = (1...2).map { index -> CharacteristicModel in
            let deviceId: Int64 = 1
            return CharacteristicModel(uuid: newUUID,
                                       id: Int64(index),
                                       deviceId: deviceId,
                                       updateAt: Date(timeIntervalSince1970:
                                                        Date().timeIntervalSince1970 - Double(index)),
                                       value: data)
        }

        for model in models {
            characteristicStorage.insert(model)
        }

        let characteristicModel = characteristicStorage.get(uuid: newUUID,
                                                            deviceId: 1,
                                                            limit: 2)

        XCTAssertEqual(characteristicModel.count, 2)
        XCTAssertEqual(characteristicModel.first?.value, data)
    }
    
    func testFailedInsertWhenDataEmpty() {
        let characteristicStorage = CharacteristicStorage(inMemory: true)
        let newUUID = UUID().uuidString
        let deviceId: Int64 = 1
        let date = Date()
        let data = Data()
        let model = CharacteristicModel(uuid: newUUID,
                                        id: 0,
                                        deviceId: deviceId,
                                        updateAt: date,
                                        value: data)

        characteristicStorage.insert(model)

        let characteristicModels = characteristicStorage.get(uuid: newUUID, deviceId: deviceId, after: date)
        XCTAssertEqual(characteristicModels.count, 0)
    }

}
