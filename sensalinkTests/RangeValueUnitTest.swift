//
//  RangeValueUnitTest.swift
//  sensalinkTests
//
//  Created by Deni Zakya on 30/05/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import XCTest

@testable import sensalink

final class RangeValueUnitTest: XCTestCase {

    func testMinZero() throws {
        let string = "00999F"
        let rangeValueUnit = try RangeValueUnit.from(string)

        XCTAssertEqual(rangeValueUnit.min, 0)
        XCTAssertEqual(rangeValueUnit.max, 999)
        XCTAssertEqual(rangeValueUnit.unit, "F")
    }

    func testMinBelowZero() throws {
        let string = "40150C"
        let rangeValueUnit = try RangeValueUnit.from(string)

        XCTAssertEqual(rangeValueUnit.min, -40)
        XCTAssertEqual(rangeValueUnit.max, 150)
        XCTAssertEqual(rangeValueUnit.unit, "C")
    }

    func testMinValueImplicit() throws {
        let string = "10000P"
        let rangeValueUnit = try RangeValueUnit.from(string)

        XCTAssertEqual(rangeValueUnit.min, 0)
        XCTAssertEqual(rangeValueUnit.max, 10000)
        XCTAssertEqual(rangeValueUnit.unit, "P")
    }

}
