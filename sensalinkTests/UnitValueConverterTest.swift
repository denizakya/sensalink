//
//  UnitValueConverterTest.swift
//  sensalinkTests
//
//  Created by Deni Zakya on 11/11/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import XCTest

@testable import sensalink

final class UnitValueConverterTest: XCTestCase {

    func testGetConvertedValuePressure() {
        let converter: UnitValueConverter = .shared
        converter.updateDeviceUnit(.pressure)

        converter.selectedUnitType = .metric
        XCTAssertEqual(converter.getConvertedValue(1000), 1000)

        converter.selectedUnitType = .imperial

        XCTAssertEqual(converter.getConvertedValue(1000), 14503.768)
    }

    func testGetConvertedValueTemperature() {
        let converter: UnitValueConverter = .shared
        converter.updateDeviceUnit(.temperature)

        converter.selectedUnitType = .imperial
        XCTAssertEqual(converter.getConvertedValue(260), 8.33)

        converter.selectedUnitType = .metric
        XCTAssertEqual(converter.getConvertedValue(283), 9.85)
    }

    func testGetConvertedUnitSymbol() {
        let converter: UnitValueConverter = .shared
        converter.updateDeviceUnit(.pressure)
        converter.selectedUnitType = .imperial

        let convertedValue = converter.getConvertedUnitSymbol()

        XCTAssertEqual(convertedValue, "psi")
    }

}
