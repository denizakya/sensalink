//
//  CalibrationStatusStorageTest.swift
//  sensalinkTests
//
//  Created by Deni Zakya on 15/07/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import XCTest
@testable import sensalink

class CalibrationStatusStorageTest: XCTestCase {

    func testInsertAndGet() {
        let deviceId: Int64 = 100
        let storage = CalibrationStatusStorage(inMemory: true)

        storage.insert(deviceId: deviceId, isCalibrated: true)

        XCTAssertTrue(storage.isCalibrated(for: deviceId))
    }

    func testInsertAndClear() {
        let deviceId: Int64 = 100
        let storage = CalibrationStatusStorage(inMemory: true)

        storage.insert(deviceId: deviceId, isCalibrated: true)
        XCTAssertTrue(storage.isCalibrated(for: deviceId))

        storage.clearAll()
        XCTAssertFalse(storage.isCalibrated(for: deviceId))
    }

    func testGetFalseWhenDeviceNotExist() {
        let deviceId: Int64 = 10
        let storage = CalibrationStatusStorage(inMemory: true)

        XCTAssertFalse(storage.isCalibrated(for: deviceId))
    }

}
