//
//  DecimalNumberTest.swift
//  sensalinkTests
//
//  Created by Deni Zakya on 09/04/21.
//  Copyright © 2021 sensa. All rights reserved.
//

import XCTest

final class DecimalNumberTest: XCTestCase {

    func testDecimalNumberComma() {
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = .init(identifier: "fr_FR")
        numberFormatter.numberStyle = .decimal
        
        let numberString = "8,9"
        
        let value = numberFormatter.number(from: numberString)?.doubleValue
        XCTAssertEqual(value, 8.9)
    }
    
    func testDecimalNumberPoint() {
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = .init(identifier: "en_US")
        numberFormatter.numberStyle = .decimal
        
        let numberString = "8.9"
        
        let value = numberFormatter.number(from: numberString)?.doubleValue
        XCTAssertEqual(value, 8.9)
    }

}
