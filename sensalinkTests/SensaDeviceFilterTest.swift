//
//  SensaDeviceFilterTest.swift
//  sensalinkTests
//
//  Created by Deni Zakya on 16/01/21.
//  Copyright © 2021 sensa. All rights reserved.
//

import XCTest

@testable import sensalink

final class SensaDeviceFilterTest: XCTestCase {

    func testValidName() throws {
        let filter = SensaDeviceFilter()

        let valid = filter.isValid(name: "SENSA")

        XCTAssertTrue(valid)
    }

    func testValidNameWithPrefix() {
        let filter = SensaDeviceFilter()

        let valid = filter.isValid(name: "SENSA123123123")

        XCTAssertTrue(valid)
    }

    func testValidNameWithSuffix() {
        let filter = SensaDeviceFilter()

        let valid = filter.isValid(name: "123123SENSA")

        XCTAssertTrue(valid)
    }

    func testInvalidName() {
        let filter = SensaDeviceFilter()

        let valid = filter.isValid(name: "SENS")

        XCTAssertFalse(valid)
    }

    func testNilName() {
        let filter = SensaDeviceFilter()

        let valid = filter.isValid(name: nil)

        XCTAssertFalse(valid)
    }

}
