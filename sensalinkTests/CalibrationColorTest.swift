//
//  CalibrationColorTest.swift
//  sensalinkTests
//
//  Created by Deni Zakya on 09/04/21.
//  Copyright © 2021 sensa. All rights reserved.
//

import XCTest

@testable import sensalink

final class CalibrationColorTest: XCTestCase {

    func testInProgress() {
        let color = CalibrationColor(rawValue: 1)?.color
        
        XCTAssertEqual(color, .yellow)
    }
    
    func testComplete() {
        let color = CalibrationColor(rawValue: 2)?.color
        
        XCTAssertEqual(color, .green)
    }

    func testFailed() {
        let color = CalibrationColor(rawValue: 3)?.color
        
        XCTAssertEqual(color, .red)
    }
}
