//
//  SerialNumberParserTest.swift
//  sensalinkTests
//
//  Created by Deni Zakya on 07/05/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import XCTest

@testable import sensalink

final class SerialNumberParserTest: XCTestCase {
    
    func testParse() throws {
        let serialNumber = "SNS-SG2102220001"

        let parsed =  try XCTUnwrap(SerialNumberParser(serialNumber: serialNumber).parse())

        XCTAssertEqual(parsed.location, .singapore)
        XCTAssertEqual(parsed.type, .sensaio)
        XCTAssertEqual(parsed.dailyManufacturingIndex, "0001")

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyMMdd"
        let dateString = dateFormatter.string(from: parsed.date!)
        XCTAssertEqual(dateString, "210222")
    }

}
