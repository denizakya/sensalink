//
//  UnitPressureTest.swift
//  sensalinkTests
//
//  Created by Deni Zakya on 18/07/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import XCTest

@testable import sensalink

class UnitPressureTest: XCTestCase {

    func testConvertBarToPsi() {
        let bar = UnitPressure.bars
        let expectedValues: [Double] = [0, 145.037]
        let values: [Double] = [0, 10]

        let actualValues = values
            .map { bar.value($0, to: .poundsForcePerSquareInch) }

        XCTAssertEqual(actualValues, expectedValues)
    }

    func testConvertPsiToBar() {
        let psi = UnitPressure.poundsForcePerSquareInch
        let values: [Double] = [0, 100]
        let expectedValues: [Double] = [0, 6.894]

        let actualValues = values
            .map { psi.value($0, to: .bars) }

        XCTAssertEqual(actualValues, expectedValues)
    }

}
