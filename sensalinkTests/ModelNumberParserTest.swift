//
//  ModelNumberParserTest.swift
//  sensalinkTests
//
//  Created by Deni Zakya on 30/05/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import XCTest

@testable import sensalink

final class ModelNumberParserTest: XCTestCase {

    func testParsePressure() throws {
        let modelNumber = "SENSA.PRES-STD-2-1-LW-1-1-1"

        let parser = ModelNumberParser(modelNumber: modelNumber)

        let modelInfo = try! parser.parse()

        let info = try XCTUnwrap(modelInfo)
        XCTAssertEqual(info.model, .pressure)
        XCTAssertEqual(info.housing, "LW")
        XCTAssertEqual(info.rangeWithUnit, .init(min: 0, max: 100, unit: "B"))
        XCTAssertEqual(info.modelSubType, "STD")
        XCTAssertEqual(info.mountingOption, "1")
        XCTAssertEqual(info.frequencyPlan, "1")
        XCTAssertEqual(info.safety, "1")
        XCTAssertEqual(info.calibration, "1")
    }

    func testParseTemperature() throws {
        let modelNumber = "SENSA.TEMP-RTD-2-1-LW-1-1-1-100"

        let parser = ModelNumberParser(modelNumber: modelNumber)

        let modelInfo = try! parser.parse()

        let info = try XCTUnwrap(modelInfo)
        XCTAssertEqual(info.model, .temperature)
        XCTAssertEqual(info.housing, "LW")
        XCTAssertEqual(info.rangeWithUnit, ModelInfo.RangeWithUnit(min: -40, max: 250, unit: "k"))
        XCTAssertEqual(info.modelSubType, "RTD")
        XCTAssertEqual(info.mountingOption, "1")
        XCTAssertEqual(info.frequencyPlan, "1")
        XCTAssertEqual(info.safety, "1")
        XCTAssertEqual(info.calibration, "1")
        
        XCTAssertEqual(info.getTemperatureType(), .RTD)
    }

    func testParseValve() throws {
        let modelNumber = "SENSA.VALV-STD-0100-1-LW-1-1-1"

        let parser = ModelNumberParser(modelNumber: modelNumber)

        let modelInfo = try! parser.parse()

        let info = try XCTUnwrap(modelInfo)
        XCTAssertEqual(info.model, .valve)
        XCTAssertEqual(info.housing, "LW")
        XCTAssertEqual(info.rangeWithUnit, .init(min: 0, max: 100, unit: "%"))
        XCTAssertEqual(info.modelSubType, "STD")
        XCTAssertEqual(info.mountingOption, "1")
        XCTAssertEqual(info.frequencyPlan, "1")
        XCTAssertEqual(info.safety, "1")
        XCTAssertEqual(info.calibration, "1")
    }

}
