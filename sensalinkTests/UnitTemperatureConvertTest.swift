//
//  UnitTemperatureConvertTest.swift
//  sensalinkTests
//
//  Created by Deni Zakya on 18/07/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import XCTest

@testable import sensalink

final class UnitTemperatureConvertTest: XCTestCase {

    func testConvertCelsiusToFahrenheit() {
        let celcius = UnitTemperature.celsius
        let expectedValues = [32.0, 80.6, 212]
        let values = [0, 27.0, 100]

        let actualValues = values
            .map { celcius.value($0, to: .fahrenheit) }

        XCTAssertEqual(actualValues, expectedValues)
    }

    func testConvertFahrenheitToCelsius() {
        let fahrenheit = UnitTemperature.fahrenheit
        let values = [32.0, 80.6, 212]
        let expectedValues = [0, 27.0, 100]

        let actualValues = values
            .map { fahrenheit.value($0, to: .celsius) }

        XCTAssertEqual(actualValues, expectedValues)
    }

}
