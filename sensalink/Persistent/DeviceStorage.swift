//
//  DeviceStorage.swift
//  sensalink
//
//  Created by Deni Zakya on 03/05/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import SQLite

struct DeviceModel {
    let uuid: String
    let id: Int64
    let name: String
}

struct DeviceStorage {
    private let storage: SQLiteWrapper?
    private let deviceTable = Table("devices")
    private let uuidColumn = Expression<String>("uuid")
    private let idColumn = Expression<Int64>("id")
    private let nameColumn = Expression<String>("name")

    init(inMemory: Bool = false) {
        storage = inMemory ? SQLiteWrapper(inMemory: true) : SQLiteWrapper.shared
    }

    func insert(uuid: String, name: String?) -> Int64 {
        guard let storage = storage else { return 0 }
        let uuidColumn = Expression<String>("uuid")
        let insert = deviceTable.insert(uuidColumn <- uuid,
                                        nameColumn <- name ?? "")
        do {
            return try storage.insert(row: insert)
        } catch {
            Logging.shared.log.error("[ERROR] failed to insert device \(error)")
        }

        return 0
    }

    func update(name: String, of deviceId: Int64) {
        guard let storage = storage else { return }

        do {
            let device = deviceTable.filter(idColumn == deviceId)
            try storage.update(device.update(nameColumn <- name))
        } catch {
            Logging.shared.log.error("[ERROR] failed update device \(error)")
        }

    }

    func get(uuid: String) -> DeviceModel? {
        guard let storage = storage else { return nil }

        do {
            let rows = try storage.getRows(table: deviceTable.where(uuidColumn == uuid))
            guard let firstRow = rows.first else { return nil }
            return DeviceModel(uuid: firstRow[uuidColumn], id: firstRow[idColumn], name: firstRow[nameColumn])
        } catch { return nil }
    }
}
