//
//  CharacteristicStorage.swift
//  sensalink
//
//  Created by Deni Zakya on 03/05/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import SQLite

struct CharacteristicModel {
    let uuid: String
    let id: Int64
    let deviceId: Int64
    let updateAt: Date
    let value: Data
}

struct CharacteristicStorage {
    private let storage: SQLiteWrapper?
    private let characteristicTable = Table("characteristics")
    private let characteristicValueTable = Table("characteristic_values")

    private let charUUIDColumn = Expression<String>("uuid")
    private let charIdColumn = Expression<Int64>("id")
    private let characteristicIdColumn = Expression<Int64>("characteristic_id")
    private let deviceIdColumn = Expression<Int64>("device_id")
    private let valueColumn = Expression<Blob>("value")
    private let updateAtColumn = Expression<Double>("update_at")

    init(inMemory: Bool = false) {
        storage = inMemory ? SQLiteWrapper(inMemory: true) : SQLiteWrapper.shared
    }

    func insert(_ model: CharacteristicModel) {
        guard let storage = storage, model.value.count > 0 else { return }

        var charId = model.id
        if let row = getCharacteristic(uuid: model.uuid) {
            charId = row[charIdColumn]
        } else {
            let insertChar = characteristicTable.insert(charUUIDColumn <- model.uuid)
            do {
                charId = try storage.insert(row: insertChar)
            } catch {
                Logging.shared.log.error(error)
                return
            }
        }

        let insertCharValue = characteristicValueTable.insert(characteristicIdColumn <- charId,
                                                              updateAtColumn <- model.updateAt.timeIntervalSince1970,
                                                              valueColumn <- model.value.datatypeValue,
                                                              deviceIdColumn <- model.deviceId)
        do {
            let _ = try storage.insert(row: insertCharValue)
        } catch { Logging.shared.log.error("[ERROR] failed to insert characteristic \(error)") }
    }

    func getLatestRow(uuid: String, deviceId: Int64) -> CharacteristicModel? {
        guard let storage = storage else { return nil }

        do {
            let characteristicRow = try storage.getFirstRow(table: characteristicTable.where(charUUIDColumn == uuid))
            if let characteristicItem = characteristicRow {
                return try getLatestValue(characteristicId: characteristicItem[charIdColumn],
                                          uuid: characteristicItem[charUUIDColumn],
                                          deviceId: deviceId)
            }
        } catch { Logging.shared.log.error(error) }

        return nil
    }

    func get(uuid: String, deviceId: Int64, after date: Date) -> [CharacteristicModel] {
        guard let storage = storage else { return [] }

        do {
            let characteristicRow = try storage.getFirstRow(table: characteristicTable.where(charUUIDColumn == uuid))
            if let characteristicItem = characteristicRow {
                return try getValues(characteristicId: characteristicItem[charIdColumn],
                                     uuid: characteristicItem[charUUIDColumn],
                                     deviceId: deviceId,
                                     after: date)
            }
        } catch { Logging.shared.log.error(error) }

        return []
    }
    
    func get(uuid: String, deviceId: Int64, limit: Int = 20) -> [CharacteristicModel] {
        guard let storage = storage else { return [] }

        do {
            let characteristicRow = try storage.getFirstRow(table: characteristicTable
                                                                .where(charUUIDColumn == uuid))
            if let characteristicItem = characteristicRow {
                return try getLatestValues(characteristicId: characteristicItem[charIdColumn],
                                           uuid: characteristicItem[charUUIDColumn],
                                           deviceId: deviceId,
                                           limit: limit)
            }
        } catch { Logging.shared.log.error(error) }

        return []
    }

    func clearAllValue() {
        guard let storage = storage else { return }
        do {
            try storage.delete(characteristicValueTable.delete())
        } catch {
            Logging.shared.log.error("[ERROR] failed to delete characteristic value \(error)")
        }
    }

    private func getCharacteristic(uuid: String) -> Row? {
        guard let storage = storage
            else { return nil }

        do {
            let characteristicRow = try storage
                .getFirstRow(table: characteristicTable.where(charUUIDColumn == uuid))
            if let characteristicItem = characteristicRow {
                return characteristicItem
            }
        } catch { Logging.shared.log.error(error) }

        return nil
    }

    private func getValues(characteristicId: Int64,
                           uuid: String,
                           deviceId: Int64,
                           after date: Date) throws -> [CharacteristicModel] {
        guard let storage = storage
            else { return [] }

        let characteristicValueRows = try storage
            .getRows(table: characteristicValueTable
                .where(characteristicIdColumn == characteristicId && updateAtColumn >= date.timeIntervalSince1970))
        return characteristicValueRows.map {
            let date = Date(timeIntervalSince1970: $0[updateAtColumn])
            let data = Data($0[valueColumn].bytes)
            return CharacteristicModel(uuid: uuid,
                                       id: characteristicId,
                                       deviceId: deviceId,
                                       updateAt: date,
                                       value: data)
        }

    }
    
    private func getLatestValues(characteristicId: Int64,
                                 uuid: String,
                                 deviceId: Int64,
                                 limit: Int) throws -> [CharacteristicModel] {
        guard let storage = storage
            else { return [] }

        let characteristicValueRows = try storage
            .getRows(table: characteristicValueTable
                        .where(characteristicIdColumn == characteristicId)
                        .order(updateAtColumn.desc)
                        .limit(limit))
        return characteristicValueRows.map {
            let date = Date(timeIntervalSince1970: $0[updateAtColumn])
            let data = Data($0[valueColumn].bytes)
            return CharacteristicModel(uuid: uuid,
                                       id: characteristicId,
                                       deviceId: deviceId,
                                       updateAt: date,
                                       value: data)
        }
        .reversed()
    }

    private func getLatestValue(characteristicId: Int64,
                                uuid: String,
                                deviceId: Int64) throws -> CharacteristicModel? {
        guard let storage = storage
            else { return nil }

        if let row = try storage
            .getFirstRow(table: characteristicValueTable
                .where(characteristicIdColumn == characteristicId)
                .order(updateAtColumn.desc)) {
            return CharacteristicModel(uuid: uuid,
                                       id: characteristicId,
                                       deviceId: deviceId,
                                       updateAt: Date(timeIntervalSince1970: row[updateAtColumn]),
                                       value: Data(row[valueColumn].bytes))
        } else { return nil }
    }
}
