//
//  UnitTypeSelection.swift
//  sensalink
//
//  Created by Deni Zakya on 30/11/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation

struct UnitTypeSelection {

    private let key = "com.sensalink.unittypeselection"
    private let store = UserDefaults.standard

    func get() -> UnitType? {
        unitType(of: store.string(forKey: key))
    }

    func set(_ unitType: UnitType) {
        store.setValue(getString(of: unitType), forKey: key)
    }

    private func getString(of unitType: UnitType) -> String {
        switch unitType {
        case .imperial: return "i"
        case .metric: return "m"
        }
    }

    private func unitType(of string: String?) -> UnitType? {
        switch string {
        case "i": return .imperial
        case "m": return .metric
        default: return nil
        }
    }
}
