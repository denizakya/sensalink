//
//  SQLiteWrapper.swift
//  sensalink
//
//  Created by Deni Zakya on 03/05/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import SQLite

struct SQLiteWrapper {
    static let shared = SQLiteWrapper()

    private let dbConnection: Connection
    private let dbVersion = 2

    init?(inMemory: Bool = false) {
        if inMemory {
            do {
                dbConnection = try Connection(.inMemory)
                try createTable()
            } catch { return nil }
        } else {
            let createTableNeeded = SQLiteWrapper.isDBFileExists() == false
            guard let connection = SQLiteWrapper.openConnection() else { return nil }

            dbConnection = connection

            if createTableNeeded {
                do {
                    try createTable()
                } catch {
                    Logging.shared.log.error(error)
                    return nil
                }
                dbConnection.userVersion = dbVersion
            } else {
                if dbConnection.userVersion == 0 {
                    do {
                        try createCalibrationStatusTable()
                        dbConnection.userVersion = 1
                    } catch {
                        Logging.shared.log.error(error)
                        return nil
                    }
                }

                if dbConnection.userVersion == 1 {
                    do {
                        let devices = Table("devices")
                        let deviceName = Expression<String>("name")
                        try dbConnection.run(devices.addColumn(deviceName, defaultValue: ""))
                        dbConnection.userVersion = 2
                    } catch {
                        Logging.shared.log.error(error)
                        return nil
                    }
                }
            }
        }
    }

    private static func openConnection() -> Connection? {
        guard let filePath = getDatabasePath() else { return nil }
        do {
            return try Connection(filePath.absoluteString)
        } catch { return nil }
    }

    private static func getDatabasePath() -> URL? {
        guard let filePath = SQLiteWrapper.getDocumentsFolder()?.appendingPathComponent("db")
            else { return nil }

        return filePath
    }

    private static func getDocumentsFolder() -> URL? {
        do {
            return try FileManager.default.url(for: .documentDirectory,
                                               in: .userDomainMask,
                                               appropriateFor: nil,
                                               create: false)
        } catch { return nil }
    }

    private static func isDBFileExists() -> Bool {
        guard let path = getDatabasePath() else { return false }
        return FileManager.default.fileExists(atPath: path.path)
    }


    private func createTable() throws {
        try createDeviceTable()
        try createCharacteristicTable()
        try createCharacteristicValueTable()
        try createCalibrationStatusTable()
    }

    private func createCalibrationStatusTable() throws {
        let calibrationStatus = Table("calibration_status")
        let deviceId = Expression<Int64>("device_id")
        let isCalibrated = Expression<Bool>("is_calibrated")

        try dbConnection.run(calibrationStatus.create { table in
            table.column(deviceId)
            table.column(isCalibrated)
        })
    }

    private func createCharacteristicTable() throws {
        let characteristics = Table("characteristics")
        let uuid = Expression<String>("uuid")
        let id = Expression<Int64>("id")

        try dbConnection.run(characteristics.create { table in
            table.column(uuid, unique: true)
            table.column(id, primaryKey: .autoincrement)
        })
    }

    private func createCharacteristicValueTable() throws {
        let characteristicValues = Table("characteristic_values")
        let characteristicId = Expression<Int64>("characteristic_id")
        let value = Expression<Blob>("value")
        let updateAt = Expression<Double>("update_at")
        let deviceId = Expression<Int64>("device_id")

        try dbConnection.run(characteristicValues.create { table in
            table.column(characteristicId)
            table.column(value)
            table.column(updateAt)
            table.column(deviceId)
        })
    }

    private func createDeviceTable() throws {
        let devices = Table("devices")
        let id = Expression<Int64>("id")
        let uuid = Expression<String>("uuid")
        let name = Expression<String>("name")

        try dbConnection.run(devices.create { table in
            table.column(id, primaryKey: .autoincrement)
            table.column(uuid, unique: true)
            table.column(name)
        })
    }

    func update(_ action: Update) throws {
        try dbConnection.run(action)
    }

    func delete(_ action: Delete) throws {
        try dbConnection.run(action)
    }

    func insert(row: Insert) throws -> Int64 {
        return try dbConnection.run(row)
    }

    func getRows(table: Table) throws -> [Row] {
        return try dbConnection.prepare(table).map { $0 }
    }

    func getFirstRow(table: Table) throws -> Row? {
        return try dbConnection.pluck(table)
    }

    func destroyDatabase() {
        guard let path = SQLiteWrapper.getDatabasePath() else { return }
        try? FileManager.default.removeItem(at: path)
    }
}

extension Connection {
    public var userVersion: Int {
        get { return Int(try! scalar("PRAGMA user_version") as! Int64)}
        set { try! run("PRAGMA user_version = \(newValue)") }
    }
}
