//
//  CalibrationStatusStorage.swift
//  sensalink
//
//  Created by Deni Zakya on 15/07/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import SQLite

struct CalibrationStatusStorage {
    private let storage: SQLiteWrapper?
    private let calibrationStatusTable = Table("calibration_status")
    private let deviceIdColumn = Expression<Int64>("device_id")
    private let isCalibratedColumn = Expression<Bool>("is_calibrated")

    init(inMemory: Bool = false) {
        storage = inMemory ? SQLiteWrapper(inMemory: true) : SQLiteWrapper.shared
    }

    func insert(deviceId: Int64, isCalibrated: Bool) {
        guard let storage = storage else { return }

        let insert = calibrationStatusTable.insert(deviceIdColumn <- deviceId,
                                                   isCalibratedColumn <- isCalibrated)
        do {
            _ = try storage.insert(row: insert)
        } catch {
            Logging.shared.log.error("[ERROR] failed to insert device \(error)")
        }
    }

    func clearAll() {
        guard let storage = storage else { return }
        do {
            try storage.delete(calibrationStatusTable.delete())
        } catch {
            Logging.shared.log.error("[ERROR] failed to delete device \(error)")
        }
    }

    func isCalibrated(for deviceId: Int64) -> Bool {
        guard let storage = storage else { return false }
        do {
            let rows = try storage.getRows(table: calibrationStatusTable.where(deviceIdColumn == deviceId))
            if let firstRow = rows.first {
                return firstRow[isCalibratedColumn]
            } else { return false }
        } catch { return false }
    }
}
