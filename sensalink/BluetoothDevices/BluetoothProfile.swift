//
//  BluetoothProfiles.swift
//  sensalink
//
//  Created by Deni Zakya on 26/04/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation

struct BluetoothProfile {
    enum Service: String {
        case temperature = "46d25008-d3ee-439c-8674-19ec50ea4923"
        case pressure = "4ec0a83a-c68d-42e3-8b07-ff4bb3be245d"
        case motion = "64a77b1a-3ee3-49e9-88b5-ae7130cb1700"
        case quality = "e3d047f2-3fa7-477a-8a85-cf1a2b8372d1"
        case battery = "8ab53fab-f797-4792-a4d4-951eeac9ef88"
        case alarm = "b3eefd5c-ca01-4eca-a87b-d3b522be478b"
        case batteryBasic = "180f"
        case deviceInfoBasic = "180a"
        case deviceInfoExtend = "1800"
        case unknown
    }

    enum Characteristic: String {
        case temperature = "0cd4acb2-9575-40d5-bb70-374969a22773"
        case pressure = "b231cd48-efe3-4ac9-98ed-9983d409ad5f"
        case motion = "c1312860-3a13-480d-a3d8-fec4832de421"
        case counterAngle = "cf3c8559-963f-4460-a688-aaa021b64465"
        case motionConfig = "d574a30e-84bf-46d1-ae6f-e1a2a382ed8e"
        case calibrationStatus = "21142b37-72b6-4564-a1d1-3f1c62609a6f"
        case shock = "ced7e0c3-0f8d-4bdb-9fcb-11573d6a3a4b"
        case deviceTemperature = "e7f685ba-1822-4513-9d48-64a367cde668"
        case batteryLife = "aa6673e9-285b-4d19-8386-22256fec332b"
        case batteryLevel = "2a19"
        case modelNumber = "2a24"
        case serialNumber = "2a25"
        case firmwareVersion = "2a26"
        case deviceName = "2a00"
        case loraRegion = "67b3c12a-bc36-49e7-8d45-a04deb6b71dd"
        case loraConfig = "be5dc2ce-1059-4bfa-a646-693e660ab792"
        case loraDeviceEUI = "f3f21ced-ae8d-4ea5-9909-296f379f471a"
        case loraAppEUI = "58c76121-31ab-4c48-83c6-c5cad246836d"
        case loraAppKey = "e79412ec-cab5-4c59-8719-7f8490538127"
        case loraNetworkSessionKey = "3dc8b1c5-4335-43fb-825b-faf5081f828f"
        case loraApplicationSessionKey = "d85e3a53-2960-43a9-85e9-775ae0713454"
        case loraDeviceAddress = "ca6e47d7-f083-40a6-a813-3836cfcd52f3"
        case loraStatus = "c44d95c1-2c7d-4f39-865e-a676ea4df623"
        case temperatureCalibration = "a54d312d-c688-4bf8-ad60-4805cc32447b"
        case temperatureCalibrationCMD = "ca29cbce-eef0-41fd-a696-4708f36ad735"
        case temperatureCalibrationStatus = "2a2ec73e-75bc-11eb-9439-0242ac130002"
        case alarm = "a4a730bf-618f-4b98-bfa5-6da7e292ba2a"
        case unknown
    }

    static var resetValues: [String: Data] {
        ["d574a30e-84bf-46d1-ae6f-e1a2a382ed8e":Data([0x02]),
        "5fc01792-b2eb-45f2-848a-a19279f4be3f":Data([0x00, 0x00, 0x00, 0x00]),
        "cf3c8559-963f-4460-a688-aaa021b64465":Data([0x00]),
        "f30c1bd5-13c6-43a8-a10b-5a734823869c":Data([0x00, 0x00, 0x00, 0x00]),
        "0a0fbfd9-e55d-4743-a5a6-25e91db73dee":Data([0x00]),
        "9fe32436-33eb-43f3-b91b-c3e2ad80b34f":Data([0x00, 0x00, 0x00, 0x00]),
        "7e3a7d68-ed6f-44a7-8cac-a31f06d54d7b":Data([0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]),
        "be5dc2ce-1059-4bfa-a646-693e660ab792":Data([0x00]),
        "10e18a20-63d2-40b6-8e87-355071859b09":Data([0x01]),
        "66b81981-a665-4bd8-abb4-26e5a2e58fd2":Data([0x00]),
        "0f37ecd2-9a56-44d3-bc18-54e850e1bf32":Data([0x05, 0x00, 0x00, 0x00]),
        "f3f21ced-ae8d-4ea5-9909-296f379f471a":Data([0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88]),
        "58c76121-31ab-4c48-83c6-c5cad246836d":Data([0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88]),
        "e79412ec-cab5-4c59-8719-7f8490538127":Data([0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x00, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff]),
        "3dc8b1c5-4335-43fb-825b-faf5081f828f":Data([0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x00, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff]),
        "d85e3a53-2960-43a9-85e9-775ae0713454":Data([0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x00, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff]),
        "ca6e47d7-f083-40a6-a813-3836cfcd52f3":Data([0x11, 0x22, 0x33, 0x44]),
        "28a4f386-3c41-4653-94cc-09594220350d":Data([0x00, 0x00, 0x00, 0x00]),
        "b51c10fc-3031-4271-921e-82e28b55c4fb":Data([0x00]),
        "c44d95c1-2c7d-4f39-865e-a676ea4df623":Data([0x00]),
        "cff4c295-37d0-4acd-98d8-5793cd8be30e":Data([0x00]),
        "77167fcd-4b57-41e8-b1cb-bf804be47cea":Data([0x64, 0x00])]
    }

    static var factoryResetValues: [String: Data] {
        ["d574a30e-84bf-46d1-ae6f-e1a2a382ed8e":Data([0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,]),
         "cf3c8559-963f-4460-a688-aaa021b64465":Data([0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]),
         "be5dc2ce-1059-4bfa-a646-693e660ab792":Data([0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x64, 0x00]),
         "f3f21ced-ae8d-4ea5-9909-296f379f471a":Data([0x1e, 0xa5, 0x56, 0x9e, 0x90, 0xcc, 0x90, 0x20]),
         "58c76121-31ab-4c48-83c6-c5cad246836d":Data([0x2e, 0xa5, 0x56, 0x9e, 0x90, 0xcc, 0x90, 0x20]),
         "e79412ec-cab5-4c59-8719-7f8490538127":Data([0xf7, 0x6d, 0x94, 0xc3, 0xc8, 0xa6, 0x61, 0x40, 0xad, 0x2f, 0xfd, 0xc5, 0x7d, 0x7d, 0xb3, 0xf0]),
         "3dc8b1c5-4335-43fb-825b-faf5081f828f":Data([0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x00, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff]),
         "d85e3a53-2960-43a9-85e9-775ae0713454":Data([0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x00, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff]),
         "ca6e47d7-f083-40a6-a813-3836cfcd52f3":Data([0x11, 0x22, 0x33, 0x44]),
         "c44d95c1-2c7d-4f39-865e-a676ea4df623":Data([0x00, 0x00, 0x00, 0x00, 0x00])]
    }
}

struct BluetoothService {
    let service: BluetoothProfile.Service
    let characteristics: [BluetoothCharacteristic]
}

struct BluetoothCharacteristic {
    let characteristic: BluetoothProfile.Characteristic
}

extension BluetoothProfile.Service {
    var uuid: String { rawValue }
}

extension BluetoothProfile.Characteristic {
    var uuid: String { rawValue }
}

struct BluetoothServiceFactory {
    static func create() -> [BluetoothService] {
        let temperatureService = BluetoothService(service: .temperature,
                                                  characteristics: [BluetoothCharacteristic(characteristic: .temperature)])
        let pressureService = BluetoothService(service: .pressure,
                                               characteristics: [BluetoothCharacteristic(characteristic: .pressure)])
        let motionService = BluetoothService(service: .motion,
                                             characteristics: [BluetoothCharacteristic(characteristic: .calibrationStatus)])
        let qualityService = BluetoothService(service: .quality,
                                              characteristics: [BluetoothCharacteristic(characteristic: .shock)])
        let batteryService = BluetoothService(service: .battery,
                                              characteristics: [BluetoothCharacteristic(characteristic: .batteryLife)])
        let batteryBasicService = BluetoothService(service: .batteryBasic,
                                                   characteristics: [BluetoothCharacteristic(characteristic: .batteryLevel)])
        let deviceInfoBasicService = BluetoothService(service: .deviceInfoBasic,
                                                      characteristics: [BluetoothCharacteristic(characteristic: .serialNumber)])
        let deviceInfoExtendService = BluetoothService(service: .deviceInfoExtend,
                                                       characteristics: [BluetoothCharacteristic(characteristic: .deviceName)])

        return [
            temperatureService,
            pressureService,
            motionService,
            qualityService,
            batteryService,
            batteryBasicService,
            deviceInfoBasicService,
            deviceInfoExtendService
        ]
    }

    static func createWhitelist() -> [BluetoothService] {
        let temperatureService = BluetoothService(service: .temperature,
                                                  characteristics: [BluetoothCharacteristic(characteristic: .temperature)])
        let pressureService = BluetoothService(service: .pressure,
                                               characteristics: [BluetoothCharacteristic(characteristic: .pressure)])
        let motionService = BluetoothService(service: .motion,
                                             characteristics: [BluetoothCharacteristic(characteristic: .calibrationStatus)])
        let qualityService = BluetoothService(service: .quality,
                                              characteristics: [BluetoothCharacteristic(characteristic: .shock)])

        return [
            temperatureService,
            pressureService,
            motionService,
            qualityService
        ]
    }

    static func createPrimaryKnownServices() -> [BluetoothService] {
        let temperatureService = BluetoothService(service: .temperature,
                                                  characteristics: [BluetoothCharacteristic(characteristic: .temperature)])
        let pressureService = BluetoothService(service: .pressure,
                                               characteristics: [BluetoothCharacteristic(characteristic: .pressure)])
        let motionService = BluetoothService(service: .motion,
                                             characteristics: [BluetoothCharacteristic(characteristic: .calibrationStatus)])

        return [
            temperatureService,
            pressureService,
            motionService
        ]
    }
}
