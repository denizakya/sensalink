//
//  SerialNumberParser.swift
//  sensalink
//
//  Created by Deni Zakya on 07/05/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation

struct DeviceInfo {
    let dailyManufacturingIndex: String
    let date: Date?
    let type: DeviceType
    let location: Location

    enum DeviceType: String {
        case sensaio = "SNS"
        case lite = "LIT"
        case sat = "SAT"
    }
    
    enum Location: String {
        case france = "FR"
        case singapore = "SG"
    }
}

struct SerialNumberParser {
    let serialNumber: String

    func parse() -> DeviceInfo? {
        guard serialNumber.count == 16 else { return nil }
        guard let deviceType = DeviceInfo.DeviceType(rawValue:
                                                        String(serialNumber.prefix(3)))
        else { return nil }
        
        let locationStartIndex = serialNumber.index(serialNumber.startIndex, offsetBy: 4)
        let locationEndIndex = serialNumber.index(locationStartIndex, offsetBy: 1)
        let locationString = String(serialNumber[locationStartIndex...locationEndIndex])
        guard let location = DeviceInfo
            .Location(rawValue: locationString)
        else { return nil }

        let dateStartIndex = serialNumber.index(serialNumber.startIndex,
                                                offsetBy: 6)
        let dateEndIndex = serialNumber.index(dateStartIndex, offsetBy: 5)
        let dateString = serialNumber[dateStartIndex...dateEndIndex]
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyMMdd"
        let date = dateFormatter.date(from: String(dateString))

        let manufacturingIndex = String(serialNumber.suffix(4))
        
        return DeviceInfo(dailyManufacturingIndex: manufacturingIndex,
                          date: date,
                          type: deviceType,
                          location: location)
    }
}
