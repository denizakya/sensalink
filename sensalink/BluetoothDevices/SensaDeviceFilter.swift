//
//  SensaDeviceFilter.swift
//  sensalink
//
//  Created by Deni Zakya on 16/01/21.
//  Copyright © 2021 sensa. All rights reserved.
//

import Foundation

struct SensaDeviceFilter {

    func isValid(name: String?) -> Bool {
        return name?.lowercased().contains("sensa") ?? false
    }
}
