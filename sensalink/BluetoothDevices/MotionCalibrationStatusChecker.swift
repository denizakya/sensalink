//
//  MotionCalibrationStatusChecker.swift
//  sensalink
//
//  Created by Deni Zakya on 03/06/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation

struct MotionCalibrationStatusChecker {
    let charUUID: String

    func needCalibration(charUUID: String, data: Data) -> Bool {
        guard charUUID == self.charUUID,
            data.count > 0 else { return false }
        let overallCalibration = data[0]
        return overallCalibration < 3
    }
}
