//
//  LoraConfigParser.swift
//  sensalink
//
//  Created by Deni Zakya on 21/06/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation

enum LoraMode: Int, CaseIterable {
    case OTAA, ABP

    func string() -> String {
        switch self {
        case .OTAA: return "OTAA"
        case .ABP: return "ABP"
        }
    }
}

struct LoraConfigInfo: Equatable {
    let dataRate: UInt8
    let adaptiveDatarate: Bool
    let txPower: UInt8
    let transmissionRetries: UInt8
    let loraMode: LoraMode
    let useConfigAppliedByApp: Bool
    let ackMessage: Bool
    let networkPublic: Bool
    let appPort: UInt8
    let numberOfFramePerDay: UInt16
    let dutyCycle: Bool
    let switchLora: Bool

    func toData() -> Data {
        let numberOfFramePerDayData = Data.from(numberOfFramePerDay)

        return Data(
            [
                dataRate,
                adaptiveDatarate ? 0x01 : 0x00,
                txPower,
                transmissionRetries,
                UInt8(loraMode.rawValue.littleEndian),
                useConfigAppliedByApp ? 0x01 : 0x00,
                ackMessage ? 0x01 : 0x00,
                networkPublic ? 0x01 : 0x00,
                appPort,
                numberOfFramePerDayData[0], numberOfFramePerDayData[1],
                dutyCycle ? 0x01 : 0x00,
                switchLora ? 0x01 : 0x00
            ]
        )
    }
}

struct LoraConfigParser {
    static func parse(_ value: Data) -> LoraConfigInfo {
        if value.count >= 13 {
            let numberOfFramePerDay = Data(Array(value[9...10]))
            return LoraConfigInfo(dataRate: value[0].littleEndian,
                                  adaptiveDatarate: value[1].littleEndian == 1,
                                  txPower: value[2].littleEndian,
                                  transmissionRetries: value[3].littleEndian,
                                  loraMode: LoraMode(rawValue: Int(value[4].littleEndian)) ?? .OTAA,
                                  useConfigAppliedByApp: value[5].littleEndian == 1,
                                  ackMessage: value[6].littleEndian == 1,
                                  networkPublic: value[7].littleEndian == 1,
                                  appPort: value[8].littleEndian,
                                  numberOfFramePerDay: numberOfFramePerDay.toUInt16() ?? 0,
                                  dutyCycle: value[11].littleEndian == 1,
                                  switchLora: value[12].littleEndian == 1)
        } else {
            return LoraConfigInfo(dataRate: 0,
                                  adaptiveDatarate: false,
                                  txPower: 0,
                                  transmissionRetries: 0,
                                  loraMode: .OTAA,
                                  useConfigAppliedByApp: false,
                                  ackMessage: false,
                                  networkPublic: false,
                                  appPort: 0,
                                  numberOfFramePerDay: 0,
                                  dutyCycle: false,
                                  switchLora: false)
        }
    }
}
