//
//  AvailableCharacteristic.swift
//  sensalink
//
//  Created by Deni Zakya on 22/08/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import CoreBluetooth

final class AvailableCharacteristic {

    static let shared = AvailableCharacteristic()

    private(set) var characteristics: [String: CBCharacteristic] = [:]

    func clear() {
        characteristics = [:]
    }

    func insert(_ characteristic: CBCharacteristic) {
        let key = characteristic.uuid.uuidString.lowercased()
        characteristics[key] = characteristic
    }
}
