//
//  ModelNumberParser.swift
//  sensalink
//
//  Created by Deni Zakya on 30/05/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation

struct ModelInfo {
    let model: ModelType
    let modelSubType: String
    let rangeWithUnit: RangeWithUnit
    let mountingOption: String
    let housing: String
    let safety: String
    let frequencyPlan: String
    let calibration: String

    enum ModelType: String {
        case pressure = "PRES"
        case temperature = "TEMP"
        case valve = "VALV"
        case vlit = "VLIT"
        case unknown = ""
    }
    
    struct RangeWithUnit: Equatable {
        let min: Int
        let max: Int
        let unit: String
    }
}

extension ModelInfo {
    func getTemperatureType() -> TemperatureType? { TemperatureType(modelSubType) }
}

enum TemperatureType {
    case RTD
    case TYK //TCK
    
    init?(_ value: String) {
        switch value.uppercased() {
        case "TYK", "TCK": self = .TYK
        case "RTD": self = .RTD
        default: return nil
        }
    }
}

enum ModelNumberError: Error {
    case parseError
}

struct ModelNumberParser {

    let modelNumber: String

    func parse() throws -> ModelInfo? {
        Logging.shared.log.info("[INFO] Parse model number of \(modelNumber)")
        
        let prefixSensa = "SENSA."
        let index = modelNumber.index(modelNumber.startIndex, offsetBy: prefixSensa.count)
        let removedPrefixSensa = modelNumber[index...]

        let partNumbers = removedPrefixSensa.components(separatedBy: "-")
        guard partNumbers.count >= 7 else { return nil }

        let modelTypeString = partNumbers.first ?? ""
        let modelType = ModelInfo.ModelType(rawValue: modelTypeString) ?? .unknown

        let modelSubType = partNumbers[1]
        let range = partNumbers[2]
        let mountingOption = partNumbers[3]
        let housing = partNumbers[4]
        let safety = partNumbers[5]
        let frequency = partNumbers[6]
        let calibration = partNumbers[7]
        
        guard let rangeWithUnit = getRangeWithUnit(range, type: modelType)
        else { throw ModelNumberError.parseError }

        return ModelInfo(model: modelType,
                         modelSubType: modelSubType,
                         rangeWithUnit: rangeWithUnit,
                         mountingOption: mountingOption,
                         housing: housing,
                         safety: safety,
                         frequencyPlan: frequency,
                         calibration: calibration)
    }
    
    private func getRangeWithUnit(_ value: String, type: ModelInfo.ModelType) -> ModelInfo.RangeWithUnit? {
        
        let actions = [
            ModelInfo.ModelType.temperature: temperatureRangeWithUnit,
            ModelInfo.ModelType.pressure: pressureRangeWithUnit,
            ModelInfo.ModelType.valve: valveRangeWithUnit,
            ModelInfo.ModelType.vlit: valveRangeWithUnit
        ]
        
        if let execute = actions[type] {
            return execute(value)
        } else { return nil }
    }
}

private func valveRangeWithUnit(_ value: String) -> ModelInfo.RangeWithUnit? {
    .init(min: 0, max: 100, unit: "%")
}

private func temperatureRangeWithUnit(_ value: String) -> ModelInfo.RangeWithUnit? {
    switch value {
    case "1": return .init(min: -40, max: 150, unit: "k")
    case "2": return .init(min: -40, max: 250, unit: "k")
    case "3": return .init(min: -40, max: 450, unit: "k")
    case "4": return .init(min: -40, max: 650, unit: "k")
    default: return nil
    }
}

private func pressureRangeWithUnit(_ value: String) -> ModelInfo.RangeWithUnit? {
    switch value {
    case "1": return .init(min: 0, max: 10, unit: "B")
    case "2": return .init(min: 0, max: 100, unit: "B")
    case "3": return .init(min: 0, max: 200, unit: "B")
    case "4": return .init(min: 0, max: 400, unit: "B")
    case "5": return .init(min: 0, max: 600, unit: "B")
    case "6": return .init(min: 0, max: 1000, unit: "B")
    default: return nil
    }
}
