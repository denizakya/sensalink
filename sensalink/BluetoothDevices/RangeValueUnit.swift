//
//  RangeValueUnit.swift
//  sensalink
//
//  Created by Deni Zakya on 30/05/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation

enum RangeValueUnitError: Error {
    case unableParse
}

struct RangeValueUnit {

    let min: Int
    let max: Int
    let unit: String

    static func from(_ string: String) throws -> Self {
        let unit = String(string.suffix(1))

        let stringWithoutUnit = String( string[string.startIndex..<string.index(string.endIndex, offsetBy: -1)])
        let minValue = try getMinValue(stringWithoutUnit)
        let maxValue = try getMaxValue(stringWithoutUnit)

        var min = 0
        var max = 0
        if maxValue == 0 {
            min = 0
            if let value = Int(stringWithoutUnit) {
                max = value
            } else { throw RangeValueUnitError.unableParse }
        } else {
            min = minValue
            max = maxValue
        }
        assert(min < max, "min value need to be less than max value")
        
        return RangeValueUnit(min: min, max: max, unit: unit)
    }
}

private func getMinValue(_ string: String) throws -> Int {
    let minString = String(string.prefix(2))
    if let value = Int(minString) {
        return value > 0 ? -value : value
    } else { throw RangeValueUnitError.unableParse }
}

private func getMaxValue(_ string: String) throws -> Int {
    let maxStartIndex = string.index(string.startIndex, offsetBy: 2)
    let maxSubString = string[maxStartIndex..<string.endIndex]
    let maxString = String(maxSubString)

    if let value = Int(maxString) {
        return value
    } else { throw RangeValueUnitError.unableParse }
}
