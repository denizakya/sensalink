//
//  BluetoothManagerWrapper.swift
//  sensalink
//
//  Created by Deni Zakya on 25/04/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import CoreBluetooth

struct DiscoveredPeripheral {
    let peripheral: CBPeripheral
    let advertisementData: [String: Any]
    let RSSI: NSNumber
}

struct CharacteristicValue {
    let characteristic: BluetoothProfile.Characteristic
    let value: Data
}

typealias OnUpdateCharacteristicValue = (CBCharacteristic, Data) -> Void

final class BluetoothManagerWrapper {
    static let shared = BluetoothManagerWrapper()

    let scanTimer = 2.0
    var peripherals = [DiscoveredPeripheral]()
    var connectedPeripheral: CBPeripheral?

    let centralManager : CBCentralManager
    let bluetoothDelegate = BluetoothDelegate()

    let knownPeripheralServices: [BluetoothService]

    init() {
        knownPeripheralServices = BluetoothServiceFactory.create()

        centralManager = CBCentralManager(delegate: bluetoothDelegate, queue: DispatchQueue.main)

        setupDelegate()
    }

    var onScanFinished: ([DiscoveredPeripheral]) -> Void = { _ in  }
    var onPeripheralConnected: (CBPeripheral) -> Void = { _ in }
    var onDiscoverServices: (CBPeripheral, [CBService]) -> Void = { _,_  in }
    var onUpdateCharacteristicValue: OnUpdateCharacteristicValue = { _,_ in }

    func startScan() {
        peripherals = []

        centralManager.scanForPeripherals(withServices: nil,
                                          options: nil)

        DispatchQueue.main.asyncAfter(deadline: .now() + scanTimer) { [unowned self] in
            self.stopScan()
            self.peripherals.sort(by: { (lhs, rhs) -> Bool in
                lhs.RSSI.floatValue > rhs.RSSI.floatValue
            })

            self.onScanFinished(self.peripherals)
        }
    }

    func stopScan() {
        centralManager.stopScan()
    }

    func connect(to peripheral: CBPeripheral) {
        cancelCurrentPeripheralConnection()
        centralManager.connect(peripheral, options: nil)
    }

    func discoverServices(uuids: [CBUUID]?) {
        connectedPeripheral?.discoverServices(uuids)
    }

    func updateCharacteristicValue(for characteristic: CBCharacteristic) {
        Logging.shared.log
            .debug("Read value of \(characteristic.uuid.uuidString.lowercased())")

        connectedPeripheral?.readValue(for: characteristic)
    }

    func writeCharacteristicValue(value: Data, characteristic: CBCharacteristic) {
        Logging.shared.log
            .info("[INFO] Write \(characteristic.uuid.uuidString.lowercased()) > \(value.toHexString())")

        connectedPeripheral?.writeValue(value, for: characteristic, type: .withResponse)
    }

    func selectDevice(for row: Int) -> CBPeripheral {
        let selectedPeripheral = peripherals[row]
        return selectedPeripheral.peripheral
    }

    func cancelCurrentPeripheralConnection() {
        guard let peripheral = connectedPeripheral else { return }

        centralManager.cancelPeripheralConnection(peripheral)
        connectedPeripheral = nil
    }

    private func setupDelegate() {
        bluetoothDelegate.onDiscoverPeripheral = { [unowned self] item in
            self.peripherals.removeAll { $0.peripheral.identifier == item.peripheral.identifier }
            self.peripherals.append(item)
        }
        bluetoothDelegate.onPoweredOn = { [unowned self] in
            self.startScan()
        }
        bluetoothDelegate.onPeripheralConnected = { [unowned self] peripheral in
            self.connectedPeripheral = peripheral
            self.connectedPeripheral?.delegate = self.bluetoothDelegate
            self.onPeripheralConnected(peripheral)
        }
        bluetoothDelegate.onDiscoverServices = { [unowned self] peripheral in
            guard let services = peripheral.services else { return }

            for service in services {
                Logging.shared.log.info("[INFO] [Service] [\(service.uuid.uuidString)]")

                peripheral.discoverCharacteristics(nil,
                                                   for: service)
            }

            self.onDiscoverServices(peripheral, services)
        }
        bluetoothDelegate.onDiscoverCharacteristics = { [unowned self] service in
            guard let characteristics = service.characteristics else { return }

            for characteristic in characteristics {
                var capabilities = "[Characteristic] [\(characteristic.uuid.uuidString)]"
                capabilities += " ["
                if characteristic.properties.contains(.read) { capabilities += " read" }
                if characteristic.properties.contains(.notify) { capabilities += " notify" }
                if characteristic.properties.contains(.write) { capabilities += " write" }
                capabilities += " ]"

                Logging.shared.log.info(capabilities)

                if characteristic.properties.contains(.read) {
                    self.connectedPeripheral?.readValue(for: characteristic)
                }
                if characteristic.properties.contains(.notify) {
                    self.connectedPeripheral?.setNotifyValue(true, for: characteristic)
                }
            }
        }
        bluetoothDelegate.onCharacteristicValueUpdated = { [unowned self] characteristic in
            AvailableCharacteristic.shared.insert(characteristic)

            guard let data = characteristic.value else { return }
            self.onUpdateCharacteristicValue(characteristic, data)

            switch characteristic.uuid {
            case CBUUID(string: BluetoothProfile.Characteristic.temperature.uuid):
                if let data = characteristic.value {
                    Logging.shared.log.verbose(data.toFloat() ?? 0)
                }
            case CBUUID(string: BluetoothProfile.Characteristic.serialNumber.uuid),
                 CBUUID(string: BluetoothProfile.Characteristic.modelNumber.uuid):
                if let data = characteristic.value {
                    Logging.shared.log.verbose("Value of \(characteristic.uuid.uuidString) > \(data.toString(encoding: .utf8) ?? "N/A")")
                }
            case CBUUID(string: BluetoothProfile.Characteristic.batteryLife.uuid):
                if let data = characteristic.value {
                    Logging.shared.log.verbose("Value of \(characteristic.uuid.uuidString) > \(data.toUInt64() ?? 0)")
                }
            default: break
            }
        }
    }

    private var knownCharacteristicUUIDs: [String] {
        self.knownPeripheralServices
            .flatMap { $0.characteristics.map { $0.characteristic.uuid } }
    }
}

final class BluetoothDelegate: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {

    var onPoweredOn: () -> Void = {}
    var onDiscoverPeripheral: (DiscoveredPeripheral) -> Void = { _ in }
    var onPeripheralConnected: (CBPeripheral) -> Void = { _ in }
    var onDiscoverServices: (CBPeripheral) -> Void = { _ in }
    var onDiscoverCharacteristics: (CBService) -> Void = { _ in }
    var onCharacteristicValueUpdated: (CBCharacteristic) -> Void = { _ in }

    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        Logging.shared.log.verbose(central.state.rawValue)

        switch central.state {
        case .poweredOn: onPoweredOn()
        case .poweredOff: break
        case .resetting: break
        case .unauthorized: break
        case .unsupported: break
        case .unknown: break
        @unknown default: break
        }
    }

    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral,
                        advertisementData: [String : Any], rssi RSSI: NSNumber) {
        let discoveredPeripherals = DiscoveredPeripheral(peripheral: peripheral,
                                                         advertisementData: advertisementData,
                                                         RSSI: RSSI)

        onDiscoverPeripheral(discoveredPeripherals)
    }

    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        Logging.shared.log.info("[Connect] [DeviceName: \(peripheral.name ?? "N/A")]")

        onPeripheralConnected(peripheral)
    }

    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        Logging.shared.log.info("[Disconnect] [DeviceName: \(peripheral.name ?? "N/A")]")
        
        AvailableCharacteristic.shared.clear()
    }

    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        if let error = error { Logging.shared.log.error(error) }
        onDiscoverServices(peripheral)
    }

    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if let error = error { Logging.shared.log.error(error) }

        onDiscoverCharacteristics(service)
    }

    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if let error = error { Logging.shared.log.error(error) }

        onCharacteristicValueUpdated(characteristic)
    }

    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        if let error = error {
            Logging.shared.log.error(error.localizedDescription)
        }
    }
}
