//
//  DeviceConnectViewController.swift
//  sensalink
//
//  Created by Deni Zakya on 10/05/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth

final class DeviceConnectViewController : UIViewController {

    @IBOutlet weak var messageLabel: UILabel!

    private let deviceStorage = DeviceStorage()
    private let characteristicStorage = CharacteristicStorage()

    var peripheral: CBPeripheral?

    var onKnownServiceFound: () -> Void = {}
    var onKnownServiceMissing: () -> Void = {}

    private var services: [CBService] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        BluetoothManagerWrapper.shared.onPeripheralConnected = { [weak self] peripheral in
            BluetoothManagerWrapper.shared.discoverServices(uuids: nil)

            let peripheralName = String(describing: peripheral.name ?? "N/A")
            self?.messageLabel.text = "Connected to \(peripheralName)"

            self?.insertDevice(peripheral: peripheral)
        }
        BluetoothManagerWrapper.shared.onDiscoverServices = { [weak self] peripheral, services in
            self?.onServiceFound(peripheral: peripheral, services: services)
        }
        BluetoothManagerWrapper.shared.onUpdateCharacteristicValue = { [weak self] characteristic, data in
            self?.onUpdateCharacteristicValue(characteristic, data: data)
        }

        connect(peripheral)
    }

    private func insertDevice(peripheral: CBPeripheral) {
        let peripheralUUID = peripheral.identifier.uuidString.lowercased()
        let newDeviceId = deviceStorage.insert(uuid: peripheralUUID, name: peripheral.name)
        if newDeviceId > 0 {
            Logging.shared.log.info("Insert \(peripheralUUID) has id \(newDeviceId)")
        } else {
            Logging.shared.log.error("[ERROR] Unable to insert \(peripheralUUID)")
        }
    }

    private func onUpdateCharacteristicValue(_ characteristic: CBCharacteristic, data: Data) {
        let peripheralUUID = characteristic.service.peripheral.identifier.uuidString.lowercased()
        let characteristicUUID = characteristic.uuid.uuidString.lowercased()

        insertCharacteristicValue(deviceUUID: peripheralUUID,
                                  characteristicUUID: characteristicUUID,
                                  data: data)

        if !isDeviceInformationAvailable(for: peripheralUUID) {
            requestNecessaryDeviceInformation(for: peripheralUUID)
        } else {
            checkServiceFound()
        }
    }

    private func onServiceFound(peripheral: CBPeripheral, services: [CBService]) {
        self.services = services
        let peripheralUUID = peripheral.identifier.uuidString.lowercased()

        if !isDeviceInformationAvailable(for: peripheralUUID) {
            requestNecessaryDeviceInformation(for: peripheralUUID)
            messageLabel.text = "Requesting Serial & Model Number..."
        } else {
            checkServiceFound()
        }
    }

    private func checkServiceFound() {
        guard services.count > 0 else { return }

        let knownServices = BluetoothServiceFactory.createPrimaryKnownServices()
        let serviceUUIDs = services.map { $0.uuid.uuidString.lowercased() }

        for knownServiceItem in knownServices {
            let knownUUIDService = knownServiceItem.service.uuid
            if serviceUUIDs.contains(knownUUIDService) {
                onKnownServiceFound()
                messageLabel.text = "Find sensaio services with UUID \(knownUUIDService)"
                resetEvent()
                return
            }
        }

        messageLabel.text = "Couldn't find any sensaio devices."
        onKnownServiceMissing()
    }

    private func insertCharacteristicValue(deviceUUID: String,
                                           characteristicUUID: String,
                                           data: Data) {
        if let deviceId = getDeviceId(uuid: deviceUUID) {
            let model = CharacteristicModel(uuid: characteristicUUID,
                                            id: 0,
                                            deviceId: deviceId,
                                            updateAt: Date(),
                                            value: data)
            characteristicStorage.insert(model)
        } else { Logging.shared.log.error("[ERROR] Unable insert characteristic value to fetch device id ") }
    }

    private func isDeviceInformationAvailable(for deviceUUID: String) -> Bool {
        return isSerialNumberExist(for: deviceUUID) && isModelNumberExist(for: deviceUUID)
    }

    private func requestNecessaryDeviceInformation(for deviceUUID: String) {
        if !isSerialNumberExist(for: deviceUUID) {
            requestSerialNumber()
        } else if !isModelNumberExist(for: deviceUUID) {
            requestModelNumber()
        }
    }

    private func resetEvent() {
        onKnownServiceFound = {}
        onKnownServiceMissing = {}
    }

    private func connect(_ peripheral: CBPeripheral?) {
        guard let peripheral = peripheral else { return }
        BluetoothManagerWrapper.shared.connect(to: peripheral)

        let peripheralName = String(describing: peripheral.name ?? "N/A")
        messageLabel.text = "Connecting to \(peripheralName)"
    }

    private func getDeviceId(uuid: String?) -> Int64? {
        guard let uuid = uuid else { return nil }
        return deviceStorage.get(uuid: uuid)?.id
    }

    private func getSerialNumber(deviceId: Int64) -> CharacteristicModel? {
        let serialNumberCharacteristicUUID = BluetoothProfile.Characteristic.serialNumber.uuid
        return characteristicStorage
            .getLatestRow(uuid: serialNumberCharacteristicUUID, deviceId: deviceId)
    }

    private func isSerialNumberExist(for deviceUUID: String) -> Bool {
        guard let deviceId = getDeviceId(uuid: deviceUUID) else { return false }
        return getSerialNumber(deviceId: deviceId) != nil
    }

    private func requestSerialNumber() {
        guard let characteristic = AvailableCharacteristic.shared
            .characteristics[BluetoothProfile.Characteristic.serialNumber.uuid]
            else { return }

        BluetoothManagerWrapper.shared
            .updateCharacteristicValue(for: characteristic)
    }

    private func isModelNumberExist(for deviceUUID: String) -> Bool {
        guard let deviceId = getDeviceId(uuid: deviceUUID) else { return false }
        return getModelNumber(deviceId: deviceId) != nil
    }

    private func getModelNumber(deviceId: Int64) -> CharacteristicModel? {
        return characteristicStorage
            .getLatestRow(uuid: BluetoothProfile.Characteristic.modelNumber.uuid,
                          deviceId: deviceId)
    }

    private func requestModelNumber() {
        guard let characteristic = AvailableCharacteristic.shared
            .characteristics[BluetoothProfile.Characteristic.modelNumber.uuid]
            else { return }

        BluetoothManagerWrapper.shared
            .updateCharacteristicValue(for: characteristic)
    }

}
