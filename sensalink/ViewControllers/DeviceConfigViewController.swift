//
//  DeviceConfigViewController.swift
//  sensalink
//
//  Created by Deni Zakya on 07/06/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth

enum LoraRegion: Int {
    case EU, US, AS1, AS2
    
    func string() -> String {
        switch self {
        case .EU: return "EU"
        case .US: return "US"
        case .AS1: return "AS-1"
        case .AS2: return "AS-2"
        }
    }
}

final class DeviceConfigViewController: UIViewController, ViewDeviceValueRefreshable {
    
    var deviceUUID: String!
    var loraConfigCharacteristic: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.loraConfig.uuid]
    }
    var deviceEUICharacteristic: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.loraDeviceEUI.uuid]
    }
    var appEUICharacteristic: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.loraAppEUI.uuid]
    }
    var appKeyCharacteristic: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.loraAppKey.uuid]
    }
    var deviceAddressCharacteristic: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.loraDeviceAddress.uuid]
    }
    var networkSessionCharacteristic: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.loraNetworkSessionKey.uuid]
    }
    var appSessionCharacteristic: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.loraApplicationSessionKey.uuid]
    }
    var regionCharacteristic: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.loraRegion.uuid]
    }
    
    @IBOutlet weak var regionLabel: UILabel!
    @IBOutlet weak var modeLabel: UILabel!
    @IBOutlet weak var deviceEUILabel: UILabel!
    @IBOutlet weak var appEUILabel: UILabel!
    @IBOutlet weak var appKeyLabel: UILabel!
    @IBOutlet weak var deviceEUIView: UIView!
    @IBOutlet weak var deviceAddressView: UIView!
    @IBOutlet weak var deviceAddressLabel: UILabel!
    @IBOutlet weak var networkSessionKeyView: UIView!
    @IBOutlet weak var appEUIView: UIView!
    @IBOutlet weak var networkSessionKeyLabel: UILabel!
    @IBOutlet weak var appSessionKeyView: UIView!
    @IBOutlet weak var appKeyView: UIView!
    @IBOutlet weak var appSessionKeyLabel: UILabel!
    @IBOutlet weak var modeSelectorView: UIView!
    @IBOutlet weak var modeShadeView: UIView!
    @IBOutlet weak var otaaSettingsView: UIScrollView!
    @IBOutlet weak var abpSettingsView: UIScrollView!
    @IBOutlet weak var loraSettingsView: UIScrollView!
    @IBOutlet weak var loraNetworkStatusLabel: UILabel!

    @IBOutlet weak var deviceEUITextField: UITextField!
    @IBOutlet weak var appEUITextField: UITextField!
    @IBOutlet weak var appKeyTextField: UITextField!
    @IBOutlet weak var txRetriesOTAATextField: UITextField!
    @IBOutlet weak var adaptiveDataRateSwitch: UISwitch!
    @IBOutlet weak var numberOfFrameTextField: UITextField!
    @IBOutlet weak var ackMessageSwitch: UISwitch!
    @IBOutlet weak var networkPublicSwitch: UISwitch!
    @IBOutlet weak var appPortTextField: UITextField!
    @IBOutlet weak var dataRateOTAALabel: UILabel!
    @IBOutlet weak var txPowerOTAALabel: UILabel!
    @IBOutlet weak var useConfigAppliedOTAASwitch: UISwitch!
    @IBOutlet weak var adaptiveSubConfigView: UIView!
    @IBOutlet weak var adaptiveSubConfigViewHeight: NSLayoutConstraint!
    @IBOutlet weak var dutyCycleOTAASwitch: UISwitch!
    
    @IBOutlet weak var deviceAddressTextField: UITextField!
    @IBOutlet weak var networkSessionKeyTextField: UITextField!
    @IBOutlet weak var appSessionKeyTextField: UITextField!
    @IBOutlet weak var txRetriesABPTextField: UITextField!
    @IBOutlet weak var adaptiveDataRateABPSwitch: UISwitch!
    @IBOutlet weak var dataRateLabel: UILabel!
    @IBOutlet weak var txPowerLabel: UILabel!
    @IBOutlet weak var useConfigAppliedSwitch: UISwitch!
    @IBOutlet weak var ackMessageABPSwitch: UISwitch!
    @IBOutlet weak var networkPublicABPSwitch: UISwitch!
    @IBOutlet weak var dutyCycleABPSwitch: UISwitch!
    
    @IBOutlet weak var dataRatePickerView: UIPickerView!
    @IBOutlet weak var dataRatePickerContainerView: UIView!
    private var dataRatePickerDelegate: DataRatePickerDelegate?
    
    @IBOutlet weak var txPowerPickerContainerView: UIView!
    @IBOutlet weak var txPowerPickerView: UIPickerView!
    private var txPowerPickerDelegate: TXPowerPickerDelegate?
    
    @IBOutlet weak var bluetoothUUID: UILabel!
    @IBOutlet weak var bluetoothSignalStrength: UILabel!
    
    private let charStorage = CharacteristicStorage()
    private let deviceStorage = DeviceStorage()
    private let deviceInfoFetcher = DeviceInfoFetcher()
    
    private var mode = LoraMode.OTAA
    private var currentLoraRegion = LoraRegion.US
    
    private var needRefresh = false

    private var activeTextInput: UITextField?

    private var rootNavigationItem: UINavigationItem?
    
    func setDeviceUUID(_ uuid: String) {
        deviceUUID = uuid
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidShow(notification:)),
                                               name: UIResponder.keyboardDidShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidHide(notification:)),
                                               name: UIResponder.keyboardDidHideNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(refreshLoraCharacteristicValue),
                                               name: .refreshLoraValue, object: nil)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        rootNavigationItem?.leftBarButtonItem = nil
    }

    @objc func refreshLoraCharacteristicValue() {
        refreshLoraValue()
    }
    
    @objc func keyboardDidShow(notification: Notification) {
        if let userInfo = notification.userInfo,
            let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            let scrollView = otaaSettingsView.isHidden == false ? otaaSettingsView : abpSettingsView
            scrollView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height, right: 0)
            
            if numberOfFrameTextField.isFirstResponder {
                scrollView?.scrollRectToVisible(numberOfFrameTextField.frame, animated: true)
            } else if appPortTextField.isFirstResponder {
                scrollView?.scrollRectToVisible(appPortTextField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardDidHide(notification: Notification) {
        let scrollView = otaaSettingsView.isHidden == false ? otaaSettingsView : abpSettingsView
        scrollView?.contentInset = UIEdgeInsets.zero
    }
    
    @IBAction func doneDataRatePicker(_ sender: Any) {
        dataRatePickerContainerView.isHidden = true
        modeShadeView.isHidden = true
    }
    
    @IBAction func doneTXPicker(_ sender: Any) {
        txPowerPickerContainerView.isHidden = true
        modeShadeView.isHidden = true
    }
    
    @IBAction func doneModeSelectorTapped(_ sender: Any) {
        modeSelectorView.isHidden = true
        modeShadeView.isHidden = true
    }
    
    @IBAction func configButtonTapped(_ sender: Any) {
        switch mode {
        case .OTAA:
            otaaSettingsView.isHidden = false
            abpSettingsView.isHidden = true
            loraSettingsView.isHidden = true
        case .ABP:
            abpSettingsView.isHidden = false
            otaaSettingsView.isHidden = true
            loraSettingsView.isHidden = true
        }
        showBackButton()
    }
    
    @IBAction func showMode(_ sender: Any) {
        modeSelectorView.isHidden = false
        modeShadeView.isHidden = false
    }
    
    @IBAction func backToView(_ sender: Any) {
        showLoraSettingView()
    }
    
    @IBAction func applyABP(_ sender: Any) {
        let appPort = UInt8(appPortTextField.text ?? "") ?? 0
        if appPort == 0 {
            showInvalidAppPortAlert()
            return
        }

        let loadingView = LoadingView.instance
        loadingView.show(in: view)

        writeABP()
        
        loadingView.hide(after: 1) { [unowned self] in
            let refreshLoadingView = LoadingView.instance
            refreshLoadingView.show(in: self.view)

            self.refreshLoraValue()
            
            self.needRefresh = true
            self.backToView(self)

            refreshLoadingView.hide()
        }
    }
    
    @IBAction func resetABP(_ sender: Any) {
        guard let characteristic = loraConfigCharacteristic else {
            Logging.shared.log.warning("Unable to reset ABP settings, characteristic is not set")
            return
        }
        
        let loadingView = LoadingView.instance
        loadingView.show(in: view)
        
        resetABP(characteristic: characteristic)
        resetValue()

        needRefresh = true

        BluetoothManagerWrapper.shared.updateCharacteristicValue(for: characteristic)
        refreshLoraValue()
        
        loadingView.hide()
        needRefresh = true
    }
    
    @IBAction func applyOTAA(_ sender: Any) {
        let appPort = UInt8(appPortTextField.text ?? "") ?? 0
        if appPort == 0 {
            showInvalidAppPortAlert()
            return
        }

        let loadingView = LoadingView.instance
        loadingView.show(in: view)
        
        writeOTAA()
        
        loadingView.hide(after: 1) { [unowned self] in
            let refreshLoadingView = LoadingView.instance
            refreshLoadingView.show(in: self.view)

            self.refreshLoraValue()

            self.needRefresh = true
            self.backToView(self)

            refreshLoadingView.hide()
        }
    }
    
    @IBAction func resetOTAA(_ sender: Any) {
        guard let characteristic = loraConfigCharacteristic else {
            Logging.shared.log.warning("Unable to reset OTAA settings, characteristic is not set")
            return
        }
        
        let loadingView = LoadingView.instance
        loadingView.show(in: view)
        
        resetOTAA(characteristic: characteristic)
        resetValue()

        needRefresh = true

        BluetoothManagerWrapper.shared.updateCharacteristicValue(for: characteristic)
        refreshLoraValue()
        
        loadingView.hide()
        needRefresh = true
    }
    
    @IBAction func showDataRatePicker(_ sender: Any) {
        dataRatePickerDelegate = DataRatePickerFactory.getDelegate(loraRegion: currentLoraRegion)
        dataRatePickerView.delegate = dataRatePickerDelegate
        dataRatePickerView.dataSource = dataRatePickerDelegate
        dataRatePickerView.reloadAllComponents()
        
        dataRatePickerContainerView.isHidden = false
        modeShadeView.isHidden = false
        
        dataRatePickerDelegate?.selectedItem = { [unowned self] selectedItem in
            self.dataRateLabel.text = "\(selectedItem)"
            self.dataRateOTAALabel.text = "\(selectedItem)"
        }
    }
    
    @IBAction func showTXPowerPicker(_ sender: Any) {
        txPowerPickerDelegate = TXPowerPickerFactory.getDelegate(loraRegion: currentLoraRegion)
        txPowerPickerView.delegate = txPowerPickerDelegate
        txPowerPickerView.dataSource = txPowerPickerDelegate
        txPowerPickerView.reloadAllComponents()
        
        txPowerPickerContainerView.isHidden = false
        modeShadeView.isHidden = false
        
        txPowerPickerDelegate?.selectedItem = { [unowned self] selectedItem in
            self.txPowerLabel.text = "\(selectedItem)"
            self.txPowerOTAALabel.text = "\(selectedItem)"
        }
    }
    
    @IBAction func adaptiveDataRateChanged(_ sender: Any) {
        guard let valueSwitch = sender as? UISwitch,
              valueSwitch == adaptiveDataRateSwitch else { return }
        if valueSwitch.isOn {
            adaptiveSubConfigView.isHidden = true
            adaptiveSubConfigViewHeight.constant = 0
        } else {
            adaptiveSubConfigView.isHidden = false
            adaptiveSubConfigViewHeight.constant = 66
        }
    }
    
    func refresh() {
        guard let deviceId = deviceInfoFetcher.getDeviceId(uuid: deviceUUID) else {
            assertionFailure("Can't run without any device id")
            return
        }
        guard needRefresh || (loraSettingsView != nil && loraSettingsView.isHidden == false) else {
            return
        }
        
        
        updateRegionValue(deviceId: deviceId)
        updateDeviceEUIValue(deviceId: deviceId)
        updateAppEUIValue(deviceId: deviceId)
        updateAppKeyValue(deviceId: deviceId)
        updateDeviceAddressValue(deviceId: deviceId)
        updateNetworkSessionKeyValue(deviceId: deviceId)
        updateAppSessionKeyValue(deviceId: deviceId)
        updateLoraConfigValue(deviceId: deviceId)
        updateLoraStatus(deviceId: deviceId)
        updateRSSI()
        
        
        if let uuid = deviceUUID, bluetoothUUID != nil {
            bluetoothUUID.text = uuid
        }
        
        needRefresh = false
    }

    override func setRootNavigationItem(item: UINavigationItem?) {
        rootNavigationItem = item
    }

    override func backToMain() {
        showLoraSettingView()
    }

    @objc
    private func showLoraSettingView() {
        guard loraSettingsView != nil else { return }
        
        loraSettingsView.isHidden = false
        abpSettingsView.isHidden = true
        otaaSettingsView.isHidden = true

        activeTextInput?.resignFirstResponder()
        hideBackButton()
    }

    private func showBackButton() {
        let back = UIBarButtonItem(image: UIImage(named: "left_arrow"), style: .plain,
                                   target: self, action: #selector(showLoraSettingView))
        rootNavigationItem?.leftBarButtonItem = back
    }

    private func hideBackButton() {
        rootNavigationItem?.leftBarButtonItem = nil
    }

    private func resetValue() {
        for (uuid, value) in BluetoothProfile.resetValues {
            if let char = AvailableCharacteristic.shared.characteristics[uuid] {
                BluetoothManagerWrapper.shared
                    .writeCharacteristicValue(value: value,
                                              characteristic: char)
            } else {
                Logging.shared.log.error("[ERROR] Unable to reset for uuid \(uuid)")
            }
        }
    }
    
    private func writeOTAA() {
        guard let loraConfigcharacteristic = loraConfigCharacteristic,
            let deviceEUI = deviceEUICharacteristic,
            let appEUI = appEUICharacteristic,
            let appKey = appKeyCharacteristic else {
                Logging.shared.log.warning("Unable to write OTAA settings, characteristic is not set")
                return
        }
        
        let dataRate = UInt8(dataRateOTAALabel.text ?? "") ?? 0
        let txPower = UInt8(txPowerOTAALabel.text ?? "") ?? 0
        let transmissionRate = UInt8(txRetriesOTAATextField.text ?? "") ?? 0
        let numberOfFrame = UInt16(numberOfFrameTextField.text ?? "") ?? 0
        let appPort = UInt8(appPortTextField.text ?? "") ?? 0
        
        let info = LoraConfigInfo(dataRate: dataRate,
                                  adaptiveDatarate: adaptiveDataRateSwitch.isOn,
                                  txPower: txPower,
                                  transmissionRetries: transmissionRate,
                                  loraMode: .OTAA,
                                  useConfigAppliedByApp: useConfigAppliedOTAASwitch.isOn,
                                  ackMessage: ackMessageSwitch.isOn,
                                  networkPublic: networkPublicSwitch.isOn,
                                  appPort: appPort,
                                  numberOfFramePerDay: numberOfFrame,
                                  dutyCycle: dutyCycleOTAASwitch.isOn,
                                  switchLora: true)
        
        let infoData = info.toData()
        Logging.shared.log.info("Write into Lora Config > \(infoData.toHexString())")
        
        BluetoothManagerWrapper.shared
            .writeCharacteristicValue(value: infoData,
                                      characteristic: loraConfigcharacteristic)
        
        if let deviceEUIString = deviceEUITextField.text {
            BluetoothManagerWrapper.shared
                .writeCharacteristicValue(value: Data(hex: deviceEUIString),
                                          characteristic: deviceEUI)
            Logging.shared.log.info("Write into Lora Device EUI > \(deviceEUIString)")
        }
        
        if let appEUIString = appEUITextField.text {
            BluetoothManagerWrapper.shared
                .writeCharacteristicValue(value: Data(hex: appEUIString),
                                          characteristic: appEUI)
            Logging.shared.log.info("Write into Lora App EUI > \(appEUIString)")
        }
        
        if let appKeyString = appKeyTextField.text {
            BluetoothManagerWrapper.shared
                .writeCharacteristicValue(value: Data(hex: appKeyString),
                                          characteristic: appKey)
            Logging.shared.log.info("Write into Lora App Key > \(appKeyString)")
        }
    }
    
    private func resetOTAA(characteristic: CBCharacteristic) {
        let info = LoraConfigInfo(dataRate: 0,
                                  adaptiveDatarate: false,
                                  txPower: 0,
                                  transmissionRetries: 0,
                                  loraMode: .OTAA,
                                  useConfigAppliedByApp: false,
                                  ackMessage: false,
                                  networkPublic: false,
                                  appPort: 0,
                                  numberOfFramePerDay: 0,
                                  dutyCycle: false,
                                  switchLora: false)
        
        let infoData = info.toData()
        Logging.shared.log.info("Write into Lora Config > \(infoData.toHexString())")
        
        BluetoothManagerWrapper.shared.writeCharacteristicValue(value: infoData,
                                                                characteristic: characteristic)
    }
    
    private func writeABP() {
        guard let loraConfigCharacteristic = loraConfigCharacteristic,
            let deviceAddress = deviceAddressCharacteristic,
            let networkSession = networkSessionCharacteristic,
            let appSession = appSessionCharacteristic else {
                Logging.shared.log.warning("Unable to write ABP settings, characteristic is not set")
                return
        }
        
        let dataRate = UInt8(dataRateLabel.text ?? "") ?? 0
        let txPower = UInt8(txPowerLabel.text ?? "") ?? 0
        let transmissionRate = UInt8(txRetriesABPTextField.text ?? "") ?? 0
        let numberOfFrame = UInt16(numberOfFrameTextField.text ?? "") ?? 0
        let appPort = UInt8(appPortTextField.text ?? "") ?? 0
        
        let info = LoraConfigInfo(dataRate: dataRate,
                                  adaptiveDatarate: adaptiveDataRateABPSwitch.isOn,
                                  txPower: txPower,
                                  transmissionRetries: transmissionRate,
                                  loraMode: .ABP,
                                  useConfigAppliedByApp: useConfigAppliedSwitch.isOn,
                                  ackMessage: ackMessageABPSwitch.isOn,
                                  networkPublic: networkPublicABPSwitch.isOn,
                                  appPort: appPort,
                                  numberOfFramePerDay: numberOfFrame,
                                  dutyCycle: dutyCycleABPSwitch.isOn,
                                  switchLora: true)
        
        let infoData = info.toData()
        Logging.shared.log.info("Write into Lora Config > \(infoData.toHexString())")
        
        BluetoothManagerWrapper.shared
            .writeCharacteristicValue(value: infoData,
                                      characteristic: loraConfigCharacteristic)
        
        if let deviceAddressString = deviceAddressTextField.text {
            BluetoothManagerWrapper.shared
                .writeCharacteristicValue(value: Data(hex: deviceAddressString),
                                          characteristic: deviceAddress)
            Logging.shared.log.info("Write into Lora Device Address > \(deviceAddressString)")
        }
        
        if let networkSessionKeyString = networkSessionKeyTextField.text {
            BluetoothManagerWrapper.shared
                .writeCharacteristicValue(value: Data(hex: networkSessionKeyString),
                                          characteristic: networkSession)
            Logging.shared.log.info("Write into Lora Network Session Key > \(networkSessionKeyString)")
        }
        
        if let appSessionKeyString = appSessionKeyTextField.text {
            BluetoothManagerWrapper.shared
                .writeCharacteristicValue(value: Data(hex: appSessionKeyString),
                                          characteristic: appSession)
            Logging.shared.log.info("Write into Lora App Session Key > \(appSessionKeyString)")
        }
    }
    
    private func resetABP(characteristic: CBCharacteristic) {
        let info = LoraConfigInfo(dataRate: 0,
                                  adaptiveDatarate: false,
                                  txPower: 0,
                                  transmissionRetries: 0,
                                  loraMode: .ABP,
                                  useConfigAppliedByApp: false,
                                  ackMessage: false,
                                  networkPublic: false,
                                  appPort: 0,
                                  numberOfFramePerDay: 0,
                                  dutyCycle: false,
                                  switchLora: false)
        
        let infoData = info.toData()
        Logging.shared.log.info("Write into Lora Config > \(infoData.toHexString())")
        
        BluetoothManagerWrapper.shared.writeCharacteristicValue(value: infoData,
                                                                characteristic: characteristic)
    }

    private func refreshLoraValue() {
        let characteristics = [
            loraConfigCharacteristic,
            deviceEUICharacteristic,
            appEUICharacteristic,
            appKeyCharacteristic,
            deviceAddressCharacteristic,
            networkSessionCharacteristic,
            appSessionCharacteristic,
            regionCharacteristic
        ]
        characteristics
            .compactMap{ $0 }
            .forEach {
                BluetoothManagerWrapper.shared.updateCharacteristicValue(for: $0)
        }
    }

    private func showInvalidAppPortAlert() {
        let alert = UIAlertController(title: nil, message: "Invalid app ports",
                                      preferredStyle: .alert)
        alert.addAction(.init(title: "OK", style: .default, handler: { _ in
            alert.dismiss(animated: true, completion: nil)
        }))

        self.present(alert, animated: true, completion: nil)
    }
    
    private func updateRSSI() {
        if bluetoothSignalStrength == nil { return }
        
        let deviceRSSI = BluetoothManagerWrapper.shared.peripherals
            .first { $0.peripheral.identifier.uuidString.lowercased() == deviceUUID.lowercased() }?
            .RSSI
        if let rssi = deviceRSSI?.intValue {
            bluetoothSignalStrength.text = "\(rssi) dbm"
        }
    }
    
    private func updateRegionValue(deviceId: Int64) {
        guard regionLabel != nil else { return }
        
        let region = charStorage
            .getLatestRow(uuid: BluetoothProfile.Characteristic.loraRegion.uuid, deviceId: deviceId)?
            .value.toUInt8()
        if let region = region,
            let loraRegion = LoraRegion(rawValue: Int(region)) {
            regionLabel.text = loraRegion.string()
            
            currentLoraRegion = loraRegion
        }
    }
    
    private func updateModeValue(deviceId: Int64) {
        guard modeLabel != nil else { return }
        
        let configValue = charStorage
            .getLatestRow(uuid: BluetoothProfile.Characteristic.loraConfig.uuid, deviceId: deviceId)?
            .value
        if let value = configValue,
            let mode = LoraMode(rawValue: Int(value[4])) {
            modeLabel.text = mode.string()
        }
    }
    
    private func updateDeviceEUIValue(deviceId: Int64) {
        guard deviceEUILabel != nil else { return }
        
        let deviceEUIValue = charStorage
            .getLatestRow(uuid: BluetoothProfile.Characteristic.loraDeviceEUI.uuid,
                          deviceId: deviceId)?.value
        
        if let value = deviceEUIValue {
            deviceEUILabel.text = value.toHexString()
            deviceEUITextField.text = value.toHexString()
        }
    }
    
    private func updateDeviceAddressValue(deviceId: Int64) {
        guard deviceAddressLabel != nil else { return }
        
        let deviceAddressValue = charStorage
            .getLatestRow(uuid: BluetoothProfile.Characteristic.loraDeviceAddress.uuid,
                          deviceId: deviceId)?.value
        
        if let value = deviceAddressValue {
            deviceAddressLabel.text = value.toHexString()
            deviceAddressTextField.text = value.toHexString()
        }
    }
    
    private func updateAppEUIValue(deviceId: Int64) {
        guard appEUILabel != nil else { return }
        
        let appEUIValue = charStorage
            .getLatestRow(uuid: BluetoothProfile.Characteristic.loraAppEUI.uuid,
                          deviceId: deviceId)?
            .value
        
        if let value = appEUIValue {
            appEUILabel.text = value.toHexString()
            appEUITextField.text = value.toHexString()
        }
    }
    
    private func updateNetworkSessionKeyValue(deviceId: Int64) {
        guard networkSessionKeyLabel != nil else { return }
        
        let networkSessionKey = charStorage
            .getLatestRow(uuid: BluetoothProfile.Characteristic.loraNetworkSessionKey.uuid,
                          deviceId: deviceId)?.value
        
        if let value = networkSessionKey {
            networkSessionKeyLabel.text = value.toHexString()
            networkSessionKeyTextField.text = value.toHexString()
        }
    }
    
    private func updateAppKeyValue(deviceId: Int64) {
        guard appKeyLabel != nil else { return }
        
        let appKeyValue = charStorage
            .getLatestRow(uuid: BluetoothProfile.Characteristic.loraAppKey.uuid,
                          deviceId: deviceId)?.value
        
        if let value = appKeyValue {
            appKeyLabel.text = value.toHexString()
            appKeyTextField.text = value.toHexString()
        }
    }
    
    private func updateAppSessionKeyValue(deviceId: Int64) {
        guard appSessionKeyLabel != nil else { return }
        
        let appSessionKeyValue = charStorage
            .getLatestRow(uuid: BluetoothProfile.Characteristic.loraApplicationSessionKey.uuid,
                          deviceId: deviceId)?.value
        
        if let value = appSessionKeyValue {
            appSessionKeyLabel.text = value.toHexString()
            appSessionKeyTextField.text = value.toHexString()
        }
    }
    
    private func updateLoraConfigValue(deviceId: Int64) {
        guard otaaSettingsView != nil else { return }
        guard let loraConfigValueData = charStorage
            .getLatestRow(uuid: BluetoothProfile.Characteristic.loraConfig.uuid,
                          deviceId: deviceId)?.value else { return }
        
        let configInfo = LoraConfigParser.parse(loraConfigValueData)
        
        txRetriesOTAATextField.text = "\(configInfo.transmissionRetries)"
        adaptiveDataRateSwitch.setOn(configInfo.adaptiveDatarate, animated: true)
        numberOfFrameTextField.text = "\(configInfo.numberOfFramePerDay)"
        ackMessageSwitch.setOn(configInfo.ackMessage, animated: true)
        networkPublicSwitch.setOn(configInfo.networkPublic, animated: true)
        appPortTextField.text = "\(configInfo.appPort)"
        dutyCycleOTAASwitch.setOn(configInfo.dutyCycle, animated: true)
        
        txRetriesABPTextField.text = "\(configInfo.transmissionRetries)"
        adaptiveDataRateABPSwitch.setOn(configInfo.adaptiveDatarate, animated: true)
        dataRateLabel.text = "\(configInfo.dataRate)"
        txPowerLabel.text = "\(configInfo.txPower)"
        useConfigAppliedSwitch.setOn(configInfo.useConfigAppliedByApp, animated: true)
        ackMessageABPSwitch.setOn(configInfo.ackMessage, animated: true)
        networkPublicABPSwitch.setOn(configInfo.ackMessage, animated: true)
        dutyCycleABPSwitch.setOn(configInfo.dutyCycle, animated: true)
    }

    private func updateLoraStatus(deviceId: Int64) {
        guard loraNetworkStatusLabel != nil else { return }
        let configValue = charStorage
            .getLatestRow(uuid: BluetoothProfile.Characteristic.loraStatus.uuid, deviceId: deviceId)?
            .value
        if let value = configValue,
           let networkStatus = value.toUInt8() {
            loraNetworkStatusLabel.text = networkStatus > 0 ? "Joined" : "Not Joined"
        } else {
            loraNetworkStatusLabel.text = "Not Joined"
        }
    }
    
    private func updateMode(_ newMode: LoraMode) {
        mode = newMode
        
        switch mode {
        case .OTAA: showOTAAView()
        case .ABP: showABPView()
        }
        
        refresh()
    }
    
    private func showOTAAView() {
        deviceAddressView.isHidden = true
        deviceEUIView.isHidden = false
        networkSessionKeyView.isHidden = true
        appEUIView.isHidden = false
        appSessionKeyView.isHidden = true
        appKeyView.isHidden = false
        modeLabel.text = LoraMode.OTAA.string()
    }
    
    private func showABPView() {
        deviceAddressView.isHidden = false
        deviceEUIView.isHidden = true
        networkSessionKeyView.isHidden = false
        appEUIView.isHidden = true
        appSessionKeyView.isHidden = false
        appKeyView.isHidden = true
        modeLabel.text = LoraMode.ABP.string()
    }
    
}

extension DeviceConfigViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextInput = textField
    }
}

extension DeviceConfigViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return LoraMode.allCases.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return LoraMode.allCases[row].string()
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        updateMode(LoraMode.allCases[row])
    }
}
