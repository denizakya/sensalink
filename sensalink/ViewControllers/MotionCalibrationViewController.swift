//
//  MotionCalibrationViewController.swift
//  sensalink
//
//  Created by Deni Zakya on 31/05/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth

final class MotionCalibrationViewController: UIViewController, CharacteristicValueRefreshable {

    enum SensorType {
        case accelerometer
        case magnetometer
        case gyroscope
        case none
    }

    enum CalibrationLEDColor: Int {
        case uncalibrated = 0, oncalibration1, oncalibration2, fullyCalibrated

        var color: UIColor {
            switch self {
            case .fullyCalibrated: return .green
            case .uncalibrated: return .red
            case .oncalibration1: return .orange
            case .oncalibration2: return .yellow
            }
        }
    }

    var deviceType = DeviceInfo.DeviceType.sensaio {
        didSet {
            Logging.shared.log.info("Device Type is \(deviceType)")
        }
    }
    
    var configCharacteristic: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.motionConfig.uuid]
    }
    var counterAngleCharacteristic: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.counterAngle.uuid]
    }
    var calibrationStatusCharacteristic: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.calibrationStatus.uuid]
    }

    private var currentSensorCalibrate = SensorType.accelerometer
    private let deviceStorage = DeviceStorage()
    private let charStorage = CharacteristicStorage()
    private let calibrationStatusStorage = CalibrationStatusStorage()

    private var timer: Timer?

    private let pickerItems = [
        ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        ["0", "1/4", "1/2", "3/4"]
    ]
    private var selectedTurn = [0:0, 1:0]
    private var valveTurnDetectedValue: Float = 0

    @IBOutlet weak var accelerometerView: UIView!
    @IBOutlet weak var accelerometerImageView: UIImageView!
    @IBOutlet weak var magnetometerView: UIView!
    @IBOutlet weak var magnetometerImageView: UIImageView!
    @IBOutlet weak var closeValveView: UIView!
    @IBOutlet weak var closeValveImageView: UIImageView!
    @IBOutlet weak var putSensorValveView: UIView!
    @IBOutlet weak var putSensorValveImageView: UIImageView!
    @IBOutlet weak var accelerometerCalibrationView: UIView!
    @IBOutlet weak var accelerometerNextButton: UIButton!
    @IBOutlet weak var magnetometerCalibrationView: UIView!
    @IBOutlet weak var magnetometerNextButton: UIButton!
    @IBOutlet weak var gyroscopeCalibrationView: UIView!
    @IBOutlet weak var gyroscropeView: UIView!
    @IBOutlet weak var gyroscopeNextButton: UIButton!
    @IBOutlet weak var valveTurnNumberView: UIView!
    @IBOutlet weak var valveTurnNumberImageView: UIImageView!
    @IBOutlet weak var valveTurnNumberDetectionView: UIView!
    @IBOutlet weak var turnNumberDetectionLabel: UILabel!
    @IBOutlet weak var closeValveToggle1: UISwitch!
    @IBOutlet weak var closeValveToggle2: UISwitch!
    @IBOutlet weak var closeValveNextButton: UIButton!
    @IBOutlet weak var valveTurnPickerView: UIView!

    deinit {
        timer?.invalidate()
        timer = nil
    }

    override func didReceiveMemoryWarning() {
        accelerometerImageView.image = nil
        magnetometerImageView.image = nil
        closeValveImageView.image = nil
        putSensorValveImageView.image = nil
        valveTurnNumberImageView.image = nil
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.bringSubviewToFront(accelerometerView)

        Logging.shared.log.info("Show Accelerometer View")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        fetchCalibrationStatus()
        restartValueUpdaterTimer()

        accelerometerImageView.image = accelerometerImage(type: deviceType)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        Logging.shared.log.info("Close Motion Calibration View")

        timer?.invalidate()
        timer = nil
    }

    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)

        Logging.shared.log.info("Hide Motion Calibration View")
    }

    @IBAction func magnetometerNext(_ sender: Any) {
        view.bringSubviewToFront(closeValveView)
        currentSensorCalibrate = .gyroscope
        fetchCalibrationStatus()

        Logging.shared.log.info("Show Close Valve View")
        closeValveImageView.image = closeValveImage(type: deviceType)
    }

    @IBAction func accelerometerNext(_ sender: Any) {
        view.bringSubviewToFront(magnetometerView)
        currentSensorCalibrate = .magnetometer
        fetchCalibrationStatus()

        Logging.shared.log.info("Show Magnetometer View")

        magnetometerImageView.image = magnetometerImage(type: deviceType)
    }

    @IBAction func closeValveNext(_ sender: Any) {
        view.bringSubviewToFront(putSensorValveView)

        Logging.shared.log.info("Show Put Sensor Valve View")
        putSensorValveImageView.image = putSensorValve(type: deviceType)
    }

    @IBAction func putSensorValveNext(_ sender: Any) {
        view.bringSubviewToFront(gyroscropeView)

        Logging.shared.log.info("Show Gyroscope View")
    }

    @IBAction func gyroscopeNext(_ sender: Any) {
        view.bringSubviewToFront(valveTurnNumberView)
        currentSensorCalibrate = .none

        Logging.shared.log.info("Show Valve Turn Number View")
        valveTurnNumberImageView.image = valveTurnNumber(type: deviceType)
        
        setCalibrationModeWithApply(value: 1)
    }

    @IBAction func valveTurnNumberNext(_ sender: Any) {
        view.bringSubviewToFront(valveTurnNumberDetectionView)
        fetchCounterAngleValue()

        Logging.shared.log.info("Show Valve Turn Number Detection View")
    }

    @IBAction func turnNumberAgree(_ sender: Any) {
        let alert = UIAlertController(title: nil,
                                      message: "Calibration complete",
                                      preferredStyle: .alert)
        alert.addAction(
            UIAlertAction(title: "dismiss", style: .default, handler: { [unowned self] _ in
                self.dismiss(self)
            })
        )

        present(alert, animated: true) { [unowned self] in
            self.updateCalibrationStatus()
        }

        setCalibrationCounterAngle(value: valveTurnDetectedValue)
    }

    @IBAction func turnNumberDisagree(_ sender: Any) {
        valveTurnPickerView.isHidden = false

        Logging.shared.log.info("Turn Number Disagree View")
    }

    @IBAction func closeValveValueChanged(_ sender: Any) {
        closeValveNextButton.isEnabled = closeValveToggle1.isOn && closeValveToggle2.isOn
    }

    @IBAction func confirmManualTurnValve(_ sender: Any) {
        let lhs = selectedTurn[0] ?? 0
        let rhs = selectedTurn[1] ?? 0

        var value: Float32 = 0.0
        value += Float32(lhs)

        let secondPickers: [Float32] = [0.0, 0.25, 0.5, 0.75]
        value += secondPickers[rhs]

        setCalibrationCounterAngle(value: value)
        
        let alert = UIAlertController(title: nil,
                                      message: "Calibration complete",
                                      preferredStyle: .alert)
        alert.addAction(
            UIAlertAction(title: "dismiss", style: .default, handler: { [unowned self] _ in
                self.dismiss(self)
            })
        )

        self.present(alert, animated: true) { [unowned self] in
            self.updateCalibrationStatus()
        }
    }

    func onUpdateCharacteristicValue(characteristic: CBCharacteristic, data: Data) {
        Logging.shared.log.info("New Value of \(characteristic.uuid.uuidString) > \(data.toHexString())")

        checkForCounterAngle(characteristic: characteristic, data: data)
        checkForCalibrationStatus(characteristic: characteristic, data: data)

        if characteristic.uuid.uuidString.lowercased() ==
            BluetoothProfile.Characteristic.calibrationStatus.uuid {
            restartValueUpdaterTimer()
        }
    }

    private func restartValueUpdaterTimer() {
        Logging.shared.log.info("Restart Timer")

        timer?.invalidate()
        timer = nil
        timer = Timer.scheduledTimer(timeInterval: 3, target: self,
                                     selector: #selector(timerExecution), userInfo: nil,
                                     repeats: true)
    }

    @objc
    private func timerExecution() {
        fetchCalibrationStatus()
    }

    private func updateCalibrationStatus() {
        guard let configChar = counterAngleCharacteristic else {
            Logging.shared.log.warning("[Motion] Config Counter Angle Characteristic is nil, unable to update calibration status")
            return
        }

        let deviceUUID = configChar.service.peripheral.identifier.uuidString.lowercased()
        guard let deviceId = deviceStorage.get(uuid: deviceUUID)?.id else {
            Logging.shared.log.warning("[Motion] deviceId not found, unable to update calibration status")
            return
        }

        calibrationStatusStorage.insert(deviceId: deviceId, isCalibrated: true)
    }

    private func fetchCalibrationStatus() {
        if let characteristic = calibrationStatusCharacteristic {
            BluetoothManagerWrapper.shared.updateCharacteristicValue(for: characteristic)
        } else {
            Logging.shared.log.warning("Calibration Status Characteristic is nil")
        }
    }

    private func fetchCounterAngleValue() {
        if let characteristic = counterAngleCharacteristic {
            BluetoothManagerWrapper.shared.updateCharacteristicValue(for: characteristic)
        } else {
            Logging.shared.log.warning("Counter Angle Characteristic is nil")
        }
    }

    private func checkForCounterAngle(characteristic: CBCharacteristic, data: Data) {
        guard
            characteristic.uuid.uuidString.lowercased() == BluetoothProfile.Characteristic.counterAngle.uuid
        else { return }

        let turnDetectionString = getValveTurnDetectionString(data)
        turnNumberDetectionLabel.text = "We found \(turnDetectionString) turns of the valve."
    }

    private func getValveTurnDetectionString(_ data: Data) -> String {
        var valveTurnString = ""
        if data.count >= 8 {
            let totalAngle = data[4...7].toFloat() ?? 0
            let valveTurnDetectionProcessor = ValveTurnDetectionProcessor(totalAngle: totalAngle)

            valveTurnDetectedValue = valveTurnDetectionProcessor.getTurnValue()
            if valveTurnDetectedValue < 0.25 {
                valveTurnDetectedValue = 0.25
            }

            valveTurnString = valveTurnDetectionProcessor.toString()
        }

        return valveTurnString
    }

    private func checkForCalibrationStatus(characteristic: CBCharacteristic, data: Data) {
        guard
            characteristic.uuid.uuidString.lowercased() == BluetoothProfile.Characteristic.calibrationStatus.uuid
        else { return }

        Logging.shared.log.info("Check Value of \(characteristic.uuid.uuidString) > \(data.toHexString())")

        updateCalibrationViewForAccelerometer(data: data)
        updateCalibrationViewForMagnetometer(data: data)
        updateCalibrationViewForGyroscope(data: data)
    }

    private func getCalibrationForGyroscope(data: Data) -> UInt8? {
        guard data.count >= 2 else { return nil }
        return data[1]
    }

    private func getCalibrationForMagnetometer(data: Data) -> UInt8? {
        guard data.count >= 3 else { return nil }
        return data[2]
    }

    private func getCalibrationForAccelerometer(data: Data) -> UInt8? {
        guard data.count >= 4 else { return nil }
        return data[3]
    }

    private func updateCalibrationViewForAccelerometer(data: Data) {
        guard let status = getCalibrationForAccelerometer(data: data),
            let ledColor = CalibrationLEDColor(rawValue: Int(status)) else {
                Logging.shared.log.info("Failed Update Accelerometer > \(data.toHexString())")
                return
        }

        accelerometerCalibrationView.backgroundColor = ledColor.color

        switch ledColor {
        case .fullyCalibrated:
            accelerometerNextButton.isEnabled = true
        default:
            accelerometerNextButton.isEnabled = false
        }

        Logging.shared.log.info("Update Accelerometer > \(data.toHexString())")
    }

    private func updateCalibrationViewForMagnetometer(data: Data) {
        guard let status = getCalibrationForMagnetometer(data: data),
            let ledColor = CalibrationLEDColor(rawValue: Int(status)) else {
                Logging.shared.log.info("Failed Update Magnetometer > \(data.toHexString())")
                return
        }

        magnetometerCalibrationView.backgroundColor = ledColor.color

        switch ledColor {
        case .fullyCalibrated:
            magnetometerNextButton.isEnabled = true
        default:
            magnetometerNextButton.isEnabled = false
        }

        Logging.shared.log.info("Update Magnetometer > \(data.toHexString())")
    }

    private func updateCalibrationViewForGyroscope(data: Data) {
        guard let status = getCalibrationForGyroscope(data: data),
            let ledColor = CalibrationLEDColor(rawValue: Int(status)) else {
                Logging.shared.log.info("Failed Update Gyroscope > \(data.toHexString())")
                return
        }

        gyroscopeCalibrationView.backgroundColor = ledColor.color

        switch ledColor {
        case .fullyCalibrated:
            gyroscopeNextButton.isEnabled = true
        default:
            gyroscopeNextButton.isEnabled = false
        }

        Logging.shared.log.info("Update Gyroscope > \(data.toHexString())")
    }

    private func setCalibrationApply() {
        setCalibrationConfig(offset: 6, value: 1) //should be called in pair with mode
    }
    
    private func setCalibrationModeWithApply(value: UInt8) {
        guard let configChar = configCharacteristic else {
            Logging.shared.log.warning("Config Calibration Characteristic is nil")
            return
        }
        let deviceUUID = configChar.service.peripheral.identifier.uuidString.lowercased()
        guard let deviceId = deviceStorage.get(uuid: deviceUUID)?.id else { return }
        guard let charValue = charStorage
            .getLatestRow(uuid: configChar.uuid.uuidString.lowercased(),
                          deviceId: deviceId)?.value else { return }
        var newValue = charValue
        if newValue.count > 6 {
            newValue[5] = 1
            newValue[6] = 1
        }
        BluetoothManagerWrapper.shared.writeCharacteristicValue(value: newValue,
                                                                characteristic: configChar)
    }

    private func setCalibrationConfig(offset: Int, value: UInt8) {
        guard let configChar = configCharacteristic else {
            Logging.shared.log.warning("Config Calibration Characteristic is nil")
            return
        }
        let deviceUUID = configChar.service.peripheral.identifier.uuidString.lowercased()
        guard let deviceId = deviceStorage.get(uuid: deviceUUID)?.id else { return }
        guard let charValue = charStorage
            .getLatestRow(uuid: configChar.uuid.uuidString.lowercased(),
                          deviceId: deviceId)?.value else { return }
        var newValue = charValue
        if newValue.count > offset + 1 {
            newValue[offset] = value
        }
        BluetoothManagerWrapper.shared.writeCharacteristicValue(value: newValue,
                                                                characteristic: configChar)
    }

    private func setCalibrationCounterAngle(value: Float32) {
        guard let configChar = counterAngleCharacteristic else {
            Logging.shared.log.warning("[Motion] Config Counter Angle Characteristic is nil")
            Logging.shared.log.warning("[Motion] Unable to write into Counter Angle Characteristic")
            
            showFailedCalibrateCounterAngleAlert()
            return
        }
        let deviceUUID = configChar.service.peripheral.identifier.uuidString.lowercased()
        guard let deviceId = deviceStorage.get(uuid: deviceUUID)?.id else { return }
        guard var charValue = charStorage
            .getLatestRow(uuid: configChar.uuid.uuidString.lowercased(),
                          deviceId: deviceId)?.value
        else {
            Logging.shared.log.warning("[Motion] Unable to get latest counter angle value")
            
            showFailedCalibrateCounterAngleAlert()
            return
        }

        let offset = 8
        charValue.replaced(with: .from(value), from: offset)
        BluetoothManagerWrapper.shared.writeCharacteristicValue(value: charValue,
                                                                characteristic: configChar)
    }
    
    private func showFailedCalibrateCounterAngleAlert() {
        let alert = UIAlertController(title: "Calibration failed",
                                      message: "Please retry the calibration",
                                      preferredStyle: .alert)
        alert.addAction(
            UIAlertAction(title: "dismiss", style: .default, handler: { _ in
                alert.dismiss(animated: true)
            })
        )

        present(alert, animated: true) { [unowned self] in
            self.fetchCounterAngleValue()
        }
    }

    private func accelerometerImage(type: DeviceInfo.DeviceType) -> UIImage? {
        switch type {
            case .sensaio: return UIImage.gifImageWithName("SensaIOStandingPosition")
            case .lite: return UIImage.gifImageWithName("SensaliteStandingPosition")
            default: return nil
        }
    }

    private func magnetometerImage(type: DeviceInfo.DeviceType) -> UIImage? {
        switch type {
            case .sensaio: return UIImage.gifImageWithName("SensaMagnetometer")
            case .lite: return UIImage.gifImageWithName("SensaliteMagnetometer")
            default: return nil
        }
    }

    private func closeValveImage(type: DeviceInfo.DeviceType) -> UIImage? {
        switch type {
            case .sensaio: return UIImage.gifImageWithName("SensaFullyClosed")
            case .lite: return UIImage.gifImageWithName("SensaliteFullyClosed")
            default: return nil
        }
    }

    private func putSensorValve(type: DeviceInfo.DeviceType) -> UIImage? {
        switch type {
            case .sensaio: return UIImage.gifImageWithName("SensaPutSensor")
            case .lite: return UIImage.gifImageWithName("SensalitePutSensor")
            default: return nil
        }
    }

    private func valveTurnNumber(type: DeviceInfo.DeviceType) -> UIImage? {
        switch type {
            case .sensaio: return UIImage.gifImageWithName("SensaOpenValve")
            case .lite: return UIImage.gifImageWithName("SensaliteOpenValve")
            default: return nil
        }
    }
}

extension MotionCalibrationViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return pickerItems.count
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0: return pickerItems[0].count
        case 1: return pickerItems[1].count
        default: return 0
        }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerItems[component][row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedTurn[component] = row
    }
}
