//
//  SettingPageDetailViewController.swift
//  sensalink
//
//  Created by Deni Zakya on 14/06/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import UIKit

final class SettingPageDetailViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {

    var allViewControllers: [UIViewController]!
    var onViewControllerIndexChanged: (Int) -> Void = { _ in }

    var historyLogView: HistoryLogViewController {
        let historyLogView = storyboard!
            .instantiateViewController(withIdentifier: "HistoryLogViewController") as! HistoryLogViewController
        return historyLogView
    }

    var contactUsView: ContactUsViewController {
        let contactUsView = storyboard!
            .instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        return contactUsView
    }

    override func viewDidLoad() {
        allViewControllers = [historyLogView, contactUsView]
        dataSource = self
        delegate = self
        setViewControllers([allViewControllers.first!], direction: .forward, animated: false, completion: nil)
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let currentIndex = allViewControllers.firstIndex(of: viewController) {
            let index = currentIndex.advanced(by: -1)
            if index > -1 && index < allViewControllers.count {
                return allViewControllers[index]
            }
        }

        return nil
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let currentIndex = allViewControllers.firstIndex(of: viewController) {
            let index = currentIndex.advanced(by: 1)
            if index > -1 && index < allViewControllers.count {
                return allViewControllers[index]
            }
        }

        return nil
    }

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let currentIndex = getIndex(from: pageViewController.viewControllers?.first),
            completed {
            onViewControllerIndexChanged(currentIndex)
        }
    }

    private func getIndex(from view: UIViewController?) -> Int? {
        guard let view = view else { return nil }

        if let _ = view as? HistoryLogViewController {
            return 0
        }
        if let _ = view as? ContactUsViewController {
            return 1
        }

        return nil
    }
}
