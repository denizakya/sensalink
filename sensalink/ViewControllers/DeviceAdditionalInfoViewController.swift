//
//  DeviceAdditionalInfoViewController.swift
//  sensalink
//
//  Created by Deni Zakya on 01/05/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth

extension Notification.Name {
    static let refreshLoraValue = Notification.Name("refreshLORAValue")
    static let unitChanged = Notification.Name("unitChanged")
}

final class DeviceAdditionalInfoViewController: UIViewController,
                                                ViewDeviceValueRefreshable {
    
    @IBOutlet weak var batteryLevelLabel: UILabel!
    @IBOutlet weak var shockLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var deviceNameLabel: UILabel!
    @IBOutlet weak var deviceNameDetailLabel: UILabel!
    @IBOutlet weak var batteryLifeLabel: UILabel!
    @IBOutlet weak var batteryLevelDetailLabel: UILabel!
    @IBOutlet weak var firmwareVersionLabel: UILabel!
    @IBOutlet weak var lastCalibrationLabel: UILabel!
    @IBOutlet weak var manufacturingDateLabel: UILabel!
    @IBOutlet weak var serialNumberLabel: UILabel!
    @IBOutlet weak var metricRadioImage: UIImageView!
    @IBOutlet weak var imperialRadioImage: UIImageView!
    @IBOutlet weak var enableAlarmSwitch: UISwitch!
    @IBOutlet weak var thresholdHighAlarmTextField: UITextField!
    @IBOutlet weak var hysteresisThreshold: UISwitch!
    @IBOutlet weak var thresholdLowAlarmTextField: UITextField!
    @IBOutlet weak var enableAlarmNotificationSwitch: UISwitch!
    
    @IBOutlet weak var unitTypeView: UIStackView!
    @IBOutlet weak var deviceDetailView: UIStackView!
    @IBOutlet weak var batteryDetailView: UIStackView!
    @IBOutlet weak var mainItemViews: UIStackView!
    @IBOutlet weak var alarmDetailView: UIStackView!
    @IBOutlet weak var calibrateView: UIView!
    @IBOutlet weak var unitTypeLabel: UILabel!
    @IBOutlet weak var alarmStatusLabel: UILabel!
    @IBOutlet var alarmDetailSettingsView: [UIView]!
    
    private let unitValueConverter = UnitValueConverter.shared
    private let deviceStorage = DeviceStorage()
    private let charStorage = CharacteristicStorage()
    private let deviceInfoFetcher = DeviceInfoFetcher()

    private var rootNavigationItem: UINavigationItem?

    var executeCalibrate: () -> Void = {}

    var deviceUUID: String!

    func setDeviceUUID(_ uuid: String) {
        deviceUUID = uuid
    }

    func refresh() {
        updateBatteryLevel()
        updateBatteryLife()
        updateShock()
        updateDeviceTemperature()
        updateDeviceName()
        updateSerialNumber()
        updateManufacturingDate()
        updateLastCalibrationDate()
        updateFirmwareVersion()
        updateUnitTypeLabel()
        updateTemperatureCalibrateAction()
        updateAlarm()
    }

    override func setRootNavigationItem(item: UINavigationItem?) {
        rootNavigationItem = item
    }

    @IBAction func returnToMainTap(_ sender: Any) {
        showMainView()
        hideBackButton()
    }

    @IBAction func showBatteryDetail(_ sender: Any) {
        batteryDetailView.isHidden = false
        deviceDetailView.isHidden = true
        mainItemViews.isHidden = true
        alarmDetailView.isHidden = true
        unitTypeView.isHidden = true

        showBackButton()
    }

    @IBAction func showDeviceDetail(_ sender: Any) {
        batteryDetailView.isHidden = true
        deviceDetailView.isHidden = false
        mainItemViews.isHidden = true
        alarmDetailView.isHidden = true
        unitTypeView.isHidden = true

        showBackButton()
    }

    @IBAction func showUnitType(_ sender: Any) {
        batteryDetailView.isHidden = true
        deviceDetailView.isHidden = true
        mainItemViews.isHidden = true
        alarmDetailView.isHidden = true
        unitTypeView.isHidden = false

        showBackButton()
    }
    
    @IBAction func showAlarmDetail(_ sender: Any) {
        batteryDetailView.isHidden = true
        deviceDetailView.isHidden = true
        mainItemViews.isHidden = true
        unitTypeView.isHidden = true
        alarmDetailView.isHidden = false
        
        showBackButton()
    }
    
    @IBAction func applyAlarmTap(_ sender: Any) {
        guard let characteristic = AvailableCharacteristic.shared
                .characteristics[BluetoothProfile.Characteristic.alarm.uuid]
        else { return }
        
        let loadingView = LoadingView.instance
        loadingView.show(in: self.view)
        
        var latestValue = getAlarmLatestValue()
        latestValue?[0] = 1
        let thresholdHigh = Float(thresholdHighAlarmTextField.text ?? "0") ?? 0
        latestValue?.replaced(with: Data.from(thresholdHigh), from: 1)
        
        let thresholdLow = Float(thresholdLowAlarmTextField.text ?? "0") ?? 0
        latestValue?.replaced(with: Data.from(thresholdLow), from: 5)
        
        latestValue?[9] = hysteresisThreshold.isOn ? 1 : 0
        latestValue?[10] = enableAlarmNotificationSwitch.isOn ? 1 : 0
        
        if let value = latestValue {
            BluetoothManagerWrapper.shared
                .writeCharacteristicValue(value: value,
                                          characteristic: characteristic)
        }
        
        loadingView.hide(after: 1) {
            Logging.shared.log.info("[Temp] TYK Calibration done")
        }
    }
    
    @IBAction func resetAlarmTap(_ sender: Any) {
        thresholdHighAlarmTextField.text = "1"
        thresholdLowAlarmTextField.text = "0"
        hysteresisThreshold.isOn = false
        enableAlarmNotificationSwitch.isOn = false
    }
    
    @IBAction func enableAlarmValueChanged(_ sender: Any) {
        alarmDetailSettingsView.forEach {
            $0.isHidden = !enableAlarmSwitch.isOn
        }
    }
    
    @IBAction func metricButtonTap(_ sender: Any) {
        unitValueConverter.selectedUnitType = .metric
        updateUnitTypeLabel()
    }

    @IBAction func imperialButtonTap(_ sender: Any) {
        unitValueConverter.selectedUnitType = .imperial
        updateUnitTypeLabel()
    }

    @IBAction func calibrateTapped(_ sender: Any) {
        executeCalibrate()
    }

    @IBAction func resetTapped(_ sender: Any) {
        let loadingView = LoadingView.instance
        loadingView.show(in: view)

        for (uuid, value) in BluetoothProfile.factoryResetValues {
            if let char = AvailableCharacteristic.shared.characteristics[uuid] {
                BluetoothManagerWrapper.shared
                    .writeCharacteristicValue(value: value,
                                              characteristic: char)
            } else {
                Logging.shared.log.error("[ERROR] Unable to reset for uuid \(uuid)")
            }
        }

        NotificationCenter.default.post(name: .refreshLoraValue, object: nil)

        loadingView.hide()
    }

    @objc
    private func showMainView() {
        batteryDetailView.isHidden = true
        deviceDetailView.isHidden = true
        mainItemViews.isHidden = false
        unitTypeView.isHidden = true

        hideBackButton()
    }

    private func showBackButton() {
        let back = UIBarButtonItem(image: UIImage(named: "left_arrow"), style: .plain,
                                   target: self, action: #selector(showMainView))
        rootNavigationItem?.leftBarButtonItem = back
    }

    private func hideBackButton() {
        rootNavigationItem?.leftBarButtonItem = nil
    }
    
    private func updateAlarm() {
        guard let alarmValue = getAlarmLatestValue(),
              alarmValue.count >= 11,
              thresholdHighAlarmTextField != nil
        else {
            Logging.shared.log.error("Unable to get alarm value")
            return
        }
        
        let enableAlarmValue = alarmValue[0] == 1
        let thresholdHighValue = alarmValue[1...4].toFloat()
        let thresholdLowValue = alarmValue[5...8].toFloat()
        let hysteresisValue = alarmValue[9] == 1
        let enableAlarmNotifValue = alarmValue[10] == 1
        
        enableAlarmSwitch.isOn = enableAlarmValue
        thresholdHighAlarmTextField.text = "\(thresholdHighValue ?? 0)"
        thresholdLowAlarmTextField.text = "\(thresholdLowValue ?? 0)"
        hysteresisThreshold.isOn = hysteresisValue
        enableAlarmNotificationSwitch.isOn = enableAlarmNotifValue
    }
    
    private func getAlarmLatestValue() -> Data? {
        guard let deviceId = deviceStorage.get(uuid: deviceUUID)?.id else {
            assertionFailure("Can't run without any device id")
            return nil
        }
        let characteristicUUID = BluetoothProfile.Characteristic.alarm.uuid
        return charStorage
            .getLatestRow(uuid: characteristicUUID, deviceId: deviceId)?.value
    }

    private func updateUnitTypeLabel() {
        if unitTypeLabel == nil { return }
        
        var unitType: UnitType?
        if let selectedUnitType =  unitValueConverter.selectedUnitType {
            unitType = selectedUnitType
        } else if let currentUnitType = unitValueConverter.currentUnitType {
            unitType = currentUnitType
        } else {
            unitType = nil
        }

        switch unitType {
        case .metric:
            unitTypeLabel.text = "Metric"
            metricRadioImage.image = UIImage(named: "radio_selected")
            imperialRadioImage.image = UIImage(named: "radio_unselected")
        case .imperial:
            unitTypeLabel.text = "Imperial"
            metricRadioImage.image = UIImage(named: "radio_unselected")
            imperialRadioImage.image = UIImage(named: "radio_selected")
        case .none:
            metricRadioImage.image = UIImage(named: "radio_unselected")
            imperialRadioImage.image = UIImage(named: "radio_unselected")
        }

        NotificationCenter.default.post(name: .unitChanged, object: nil)
    }

    private func updateDeviceName() {
        guard let deviceId = deviceStorage.get(uuid: deviceUUID)?.id else {
            assertionFailure("Can't run without any device id")
            return
        }
        let characteristicUUID = BluetoothProfile.Characteristic.deviceName.uuid
        let valueString = charStorage
            .getLatestRow(uuid: characteristicUUID, deviceId: deviceId)?
            .value.toString(encoding: .utf8)

        if deviceNameLabel == nil { return }
        deviceNameLabel.text = valueString
        deviceNameDetailLabel.text = valueString
    }

    private func updateDeviceTemperature() {
        guard let deviceId = deviceStorage.get(uuid: deviceUUID)?.id else {
            assertionFailure("Can't run without any device id")
            return
        }
        let characteristicUUID = BluetoothProfile.Characteristic.deviceTemperature.uuid
        let value = charStorage
            .getLatestRow(uuid: characteristicUUID, deviceId: deviceId)?
            .value.toFloat()
        let temperatureString = value != nil ? "\(value!)" : "-"

        if temperatureLabel == nil { return }
        temperatureLabel.text = temperatureString
    }

    private func updateShock() {
        guard let deviceId = deviceStorage.get(uuid: deviceUUID)?.id else {
            assertionFailure("Can't run without any device id")
            return
        }
        let characteristicUUID = BluetoothProfile.Characteristic.shock.uuid
        let value = charStorage
            .getLatestRow(uuid: characteristicUUID, deviceId: deviceId)?
            .value.toFloat()
        let shockValueString = value != nil ? "\(value!)" : "-"

        if shockLabel == nil { return }
        shockLabel.text = shockValueString
    }

    private func updateBatteryLevel() {
        guard let deviceId = deviceStorage.get(uuid: deviceUUID)?.id else {
            assertionFailure("Can't run without any device id")
            return
        }
        let characteristicUUID = BluetoothProfile.Characteristic.batteryLevel.uuid
        let level = charStorage
            .getLatestRow(uuid: characteristicUUID, deviceId: deviceId)?
            .value.toUInt8()
        let batteryLevelString = level != nil ? "\(level!)%" : "-"

        if batteryLevelLabel == nil { return }
        batteryLevelLabel.text = batteryLevelString
        batteryLevelDetailLabel.text = batteryLevelString
    }

    private func updateBatteryLife() {
        guard let deviceId = deviceStorage.get(uuid: deviceUUID)?.id else {
            assertionFailure("Can't run without any device id")
            return
        }
        let characteristicUUID = BluetoothProfile.Characteristic.batteryLife.uuid
        let life = charStorage
            .getLatestRow(uuid: characteristicUUID, deviceId: deviceId)?.value.toUInt64()
        let batteryLifeString = life != nil ? "\(life!) days" : "-"

        if batteryLifeLabel == nil { return }
        batteryLifeLabel.text = batteryLifeString
    }

    private func updateSerialNumber() {
        guard let deviceId = deviceStorage.get(uuid: deviceUUID)?.id else {
            assertionFailure("Can't run without any device id")
            return
        }
        let characteristicUUID = BluetoothProfile.Characteristic.serialNumber.uuid
        let serialNumber = charStorage
            .getLatestRow(uuid: characteristicUUID, deviceId: deviceId)?.value.toString(encoding: .utf8)
        let serialNumberString = serialNumber != nil ? "\(serialNumber!)" : "-"

        if serialNumberLabel == nil { return }
        serialNumberLabel.text = serialNumberString
    }

    private func updateManufacturingDate() {
        guard let deviceId = deviceStorage.get(uuid: deviceUUID)?.id else {
            assertionFailure("Can't run without any device id")
            return
        }
        let characteristicUUID = BluetoothProfile.Characteristic.serialNumber.uuid
        let serialNumber = charStorage
            .getLatestRow(uuid: characteristicUUID, deviceId: deviceId)?.value.toString(encoding: .utf8)
        let serialNumberString = serialNumber != nil ? "\(serialNumber!)" : "-"

        let parser = SerialNumberParser(serialNumber: serialNumberString)
        if let date = parser.parse()?.date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"

            if manufacturingDateLabel == nil { return }
            manufacturingDateLabel.text = dateFormatter.string(from: date)
        }
    }

    private func updateLastCalibrationDate() {}

    private func updateFirmwareVersion() {
        guard let deviceId = deviceStorage.get(uuid: deviceUUID)?.id else {
            assertionFailure("Can't run without any device id")
            return
        }
        let characteristicUUID = BluetoothProfile.Characteristic.firmwareVersion.uuid
        let firmwareVersion = charStorage
            .getLatestRow(uuid: characteristicUUID, deviceId: deviceId)?.value.toString(encoding: .utf8)
        let firmwareVersionString = firmwareVersion != nil ? "\(firmwareVersion!)" : "-"

        if firmwareVersionLabel == nil { return }
        firmwareVersionLabel.text = firmwareVersionString
    }
    
    private func updateTemperatureCalibrateAction() {
        guard let deviceId = deviceInfoFetcher.getDeviceId(uuid: deviceUUID) else { return }
        let temperatureType = deviceInfoFetcher.getModelInfo(deviceId: deviceId)?.getTemperatureType()
        
        switch temperatureType {
        case .TYK: setupActionForTYKTemperature()
        case .RTD: setupActionForRTDTemperature()
        case .none: break
        }
    }
    
    private func setupActionForRTDTemperature() {
        executeCalibrate = { [unowned self] in
            let viewController = storyboard!
                .instantiateViewController(withIdentifier: "TemperatureCalibrationRTDViewController")
                as! TemperatureCalibrationRTDViewController
            viewController.deviceUUID = deviceUUID
            self.present(viewController, animated: true)
        }
    }
    
    private func setupActionForTYKTemperature() {
        executeCalibrate = { [unowned self] in
            guard let characteristic = AvailableCharacteristic.shared
                    .characteristics[BluetoothProfile.Characteristic.temperatureCalibrationCMD.uuid]
            else { return }
            
            let loadingView = LoadingView.instance
            loadingView.show(in: self.view)
            
            BluetoothManagerWrapper.shared
                .writeCharacteristicValue(value: Data([0x01]),
                                          characteristic: characteristic)
            
            loadingView.hide(after: 1) {
                Logging.shared.log.info("[Temp] TYK Calibration done")
            }
        }
    }
}
