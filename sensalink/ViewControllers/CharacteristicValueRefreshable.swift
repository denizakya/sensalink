//
//  CharacteristicValueRefreshable.swift
//  sensalink
//
//  Created by Deni Zakya on 31/03/21.
//  Copyright © 2021 sensa. All rights reserved.
//

import Foundation
import CoreBluetooth

protocol CharacteristicValueRefreshable: AnyObject {
    func onUpdateCharacteristicValue(characteristic: CBCharacteristic, data: Data)
}
