//
//  TemperatureCalibrationRTDViewController.swift
//  sensalink
//
//  Created by Deni Zakya on 17/02/21.
//  Copyright © 2021 sensa. All rights reserved.
//

import UIKit
import CoreBluetooth

final class TemperatureCalibrationRTDViewController: UIViewController {

    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var cancelCalibrateButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var calibrateButton: UIButton!
    @IBOutlet weak var tempRefValue: UITextField!
    @IBOutlet weak var tempRefContainer: UIView!
    @IBOutlet weak var calibrationContainer: UIView!
    @IBOutlet weak var imageCalibration: UIImageView!
    @IBOutlet weak var temperatureUnit: UISegmentedControl!
    @IBOutlet weak var calibrationStatus: UIView!
    @IBOutlet weak var doneButton: UIButton!
    
    private let deviceStorage = DeviceStorage()
    private let calibrationStatusStorage = CalibrationStatusStorage()
    private let characteristicStorage = CharacteristicStorage()
    
    private let loadingView = LoadingView.instance
    private var selectedUnitTemperature = UnitTemperature.celsius {
        willSet(newUnitTemperature) {
            guard newUnitTemperature != selectedUnitTemperature else { return }
            guard let refValue = temperatureRefValue(),
                  let value = selectedUnitTemperature
                    .value(refValue, to: newUnitTemperature) else { return }
            tempRefValue.text = "\(value)"
        }
    }
    
    private var timer: Timer?
    var deviceUUID: String!
    
    var temperatureCalibration: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.temperatureCalibration.uuid]
    }
    
    var temperatureCalibrationCMD: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.temperatureCalibrationCMD.uuid]
    }
    
    var temperatureCalibrationStatus: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.temperatureCalibrationStatus.uuid]
    }
    
    deinit {
        timer?.invalidate()
        timer = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cancelCalibrateButton.addTarget(self, action: #selector(cancelTap), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(cancelTap), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(nextTap), for: .touchUpInside)
        calibrateButton.addTarget(self, action: #selector(calibrateTap), for: .touchUpInside)
        tempRefValue.keyboardType = .decimalPad
        tempRefValue.addDoneButton()
        
        calibrationStatus.backgroundColor = .red
        doneButton.isEnabled = false
        
        showCalibrationView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        timer?.invalidate()
        timer = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        imageCalibration.image = UIImage.gifImageWithName("temp_calibration_2")
        restartValueUpdaterTimer()
    }
    
    @IBAction
    private func temperatureTypeChanged(_ sender: Any) {
        guard let segmentControl = sender as? UISegmentedControl,
           segmentControl == temperatureUnit else { return }
        guard let unitTemperature =
                getSelectedTemperatureType(index: temperatureUnit.selectedSegmentIndex)
        else { return }
        
        selectedUnitTemperature = unitTemperature
    }
    
    @IBAction
    private func doneTap() {
        sendCalibrationStatusZeroValue()
        
        dismiss(animated: true)
    }
    
    @objc
    private func cancelTap() {
        dismiss(animated: true)
    }
    
    @objc
    private func nextTap() {
        showTemperatureRefView()
    }
    
    @objc
    private func calibrateTap() {
        guard let temperatureValue = temperatureRefValueInSI(),
              let characteristic = temperatureCalibration
              else { return }
        
        let data = Data.from(Float(temperatureValue))
        
        BluetoothManagerWrapper.shared
            .writeCharacteristicValue(value: data,
                                      characteristic: characteristic)
        Logging.shared.log.info("[Temp] RTD Send TempRef done")
        
        loadingView.show(in: view)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.sendCalibrationCMD()
        }
    }
    
    private func updateCalibrationLed() {
        guard let temperatureCalibrationStatusUUID = temperatureCalibrationStatus?
                .uuid.uuidString.lowercased(),
            let deviceUUID = deviceUUID,
              let device = deviceStorage.get(uuid: deviceUUID),
              let data = characteristicStorage.getLatestRow(uuid: temperatureCalibrationStatusUUID,
                                                            deviceId: device.id)?.value
        else { return }
        
        guard let tempCalibStatus = data.toUInt32()
        else {
            Logging.shared.log.error("[ERROR] Unable to convert calibration status data")
            return
        }
        
        if let calibrationColor = CalibrationColor(rawValue: Int(tempCalibStatus)) {
            calibrationStatus.backgroundColor = calibrationColor.color
            doneButton.isEnabled = calibrationColor == .complete
        }
    }
    
    private func updateCalibrationStatus() {
        guard let char = temperatureCalibration else {
            Logging.shared.log.warning("[Temperature] Characteristic is nil, unable to update calibration status")
            return
        }

        let deviceUUID = char.service.peripheral.identifier.uuidString.lowercased()
        guard let deviceId = deviceStorage.get(uuid: deviceUUID)?.id else {
            Logging.shared.log.warning("[Temperature] deviceId not found, unable to update calibration status")
            return
        }

        calibrationStatusStorage.insert(deviceId: deviceId, isCalibrated: true)
    }
    
    private func getSelectedTemperatureType(index: Int) -> UnitTemperature? {
        switch index {
        case 0: return UnitTemperature.fahrenheit
        case 1: return UnitTemperature.celsius
        default: return nil
        }
    }
    
    private func temperatureRefValue() -> Double? {
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = .current
        numberFormatter.numberStyle = .decimal
        
        guard let text = tempRefValue.text,
              let value = numberFormatter.number(from:text)?.doubleValue
        else { return nil }
        return value
    }
    
    private func temperatureRefValueInSI() -> Double? {
        guard let value = temperatureRefValue() else { return nil }
        
        return selectedUnitTemperature.value(value, to: .kelvin)
    }
    
    private func sendCalibrationCMD() {
        guard let characteristic = temperatureCalibrationCMD
              else { return }
        BluetoothManagerWrapper.shared
            .writeCharacteristicValue(value: Data([0x01]),
                                      characteristic: characteristic)
        
        updateCalibrationStatus()
        loadingView.hide(after: 1) { [weak self] in
            Logging.shared.log.info("[Temp] RTD Calibration done")
            
            self?.fetchCalibrationStatus()
        }
    }
    
    private func sendCalibrationStatusZeroValue() {
        guard let characteristic = temperatureCalibrationStatus else { return }
        BluetoothManagerWrapper.shared
            .writeCharacteristicValue(value: Data([0x00]),
                                      characteristic: characteristic)
    }
    
    private func showTemperatureRefView() {
        tempRefContainer.isHidden = false
        calibrationContainer.isHidden = true
    }
    
    private func showCalibrationView() {
        tempRefContainer.isHidden = true
        calibrationContainer.isHidden = false
    }
    
    private func fetchCalibrationStatus() {
        if let characteristic = temperatureCalibrationStatus {
            BluetoothManagerWrapper.shared.updateCharacteristicValue(for: characteristic)
        } else {
            Logging.shared.log.warning("[WARNING] Calibration Status Characteristic is nil")
        }
    }
    
    @objc
    private func timerExecution() {
        fetchCalibrationStatus()
        updateCalibrationLed()
        restartValueUpdaterTimer()
    }
    
    private func restartValueUpdaterTimer() {
        Logging.shared.log.info("Restart Timer")

        timer?.invalidate()
        timer = nil
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,
                                     selector: #selector(timerExecution), userInfo: nil,
                                     repeats: true)
    }
    
}

extension UITextField {
    func addDoneButton() {
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        toolbar.sizeToFit()
        toolbar.barStyle = .default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.endEditing(_:)))
        toolbar.items = [flexSpace, doneBarButton]
        self.inputAccessoryView = toolbar
    }
}
