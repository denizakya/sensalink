//
//  ViewController.swift
//  sensalink
//
//  Created by Deni Zakya on 18/04/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import UIKit
import CoreBluetooth

final class DeviceListViewController: UIViewController {

    @IBOutlet weak var devicesTableView: UITableView!
    let refreshControl = UIRefreshControl()

    var peripherals = [DiscoveredPeripheral]()
    var selectedPeripheral: CBPeripheral?

    override func viewDidLoad() {
        super.viewDidLoad()

        refreshControl.addTarget(self, action: #selector(self.handleRefresh), for: .valueChanged)
        devicesTableView.refreshControl = refreshControl

        BluetoothManagerWrapper.shared.onScanFinished = { [unowned self] items in
            items.map(\.peripheral.name).forEach {
                Logging.shared.log.info("Scanned device \($0 ?? "nil")")
            }

            self.peripherals = items
            
            self.devicesTableView.reloadData()
            self.refreshControl.endRefreshing()
        }

        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu"),
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(showMenuView(sender:)))
    }

    @objc
    func showMenuView(sender: Any) {
        performSegue(withIdentifier: "showMenuView", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ContainerViewController {
            destination.peripheral = selectedPeripheral
        }
        if let destination = segue.destination as? DeviceConnectViewController {
            destination.peripheral = selectedPeripheral

            destination.onKnownServiceMissing = {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    destination.dismiss(animated: true, completion: nil)
                }
            }
            destination.onKnownServiceFound = { [unowned self] in
                self.performSegue(withIdentifier: "showDetail", sender: self)
                destination.dismiss(animated: true, completion: nil)
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        navigationItem.title = ""
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationItem.title = "SENSALINK"
    }

    @objc
    func handleRefresh() {
        BluetoothManagerWrapper.shared.startScan()
    }
}

extension DeviceListViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return peripherals.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DeviceItemCell") as? DeviceItemCell
            else { return UITableViewCell() }

        let item = peripherals[indexPath.row]
        cell.setup(name: item.peripheral.name ?? "N/A",
                   identifier: item.peripheral.identifier.uuidString,
                   rssi: item.RSSI)

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedPeripheral = BluetoothManagerWrapper.shared.selectDevice(for: indexPath.row)
        performSegue(withIdentifier: "showDeviceConnect", sender: self)

        tableView.deselectRow(at: indexPath, animated: true)
    }
}

