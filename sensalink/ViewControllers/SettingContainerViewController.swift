//
//  SettingContainerViewController.swift
//  sensalink
//
//  Created by Deni Zakya on 14/06/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import UIKit

final class SettingContainerViewController: UIViewController {

    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var historyLabel: UILabel!
    @IBOutlet weak var contactUs: UILabel!
    @IBOutlet weak var tabView: UIView!

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? SettingPageDetailViewController,
            segue.identifier == "showSettingPageDetail" {
            viewController.onViewControllerIndexChanged = { [weak self] index in
                self?.onIndexChanged(index: index)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "SENSALINK"
        navigationController?.navigationBar.shadowImage = UIImage()
        tabView.backgroundColor = UIColor(red: 115.0/255.0,
                                          green: 105.0/255.0,
                                          blue: 151.0/255.0,
                                          alpha: 1)
    }

    private func onIndexChanged(index: Int) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 3,
                       options: .curveEaseOut,
                       animations: { [unowned self] in
                        switch index {
                        case 0:
                            self.indicatorView.transform = .identity
                        case 1:
                            self.indicatorView.transform =
                                CGAffineTransform(translationX: self.indicatorView.frame.width, y: 0)
                        default: break
                        }

                        self.historyLabel.textColor = index == 0 ? .white : .lightGray
                        self.contactUs.textColor = index == 1 ? .white : .lightGray
        })
    }
}
