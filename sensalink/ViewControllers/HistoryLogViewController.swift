//
//  HistoryLogViewController.swift
//  sensalink
//
//  Created by Deni Zakya on 16/05/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

final class HistoryLogViewController: UIViewController {

    @IBOutlet weak var logTextView: UITextView!

    private let getZippedLogging = GetZippedLogging()

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let logPath = Logging.shared.logPath else { return }
        do {
            logTextView.text = try String(contentsOf: logPath)
        } catch {
            logTextView.text = error.localizedDescription
        }
    }

    @IBAction func sendLogTapped(_ sender: Any) {
        do {
            try getZippedLogging.get { filePath in
                DispatchQueue.main.async { [weak self] in
                    self?.sendLogWithEmail(filePath: filePath)
                }
            }
        } catch {
            let alert = UIAlertController(title: "Send Log Failed",
                                          message: error.localizedDescription,
                                          preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "Dismiss", style: .cancel,
                                            handler: nil)
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
        }
    }

    func sendLogWithEmail(filePath: URL?) {
        guard MFMailComposeViewController.canSendMail()
            else {
                showAlert(message: "Unable to send mail.")
                return
        }

        let mailer = MFMailComposeViewController()
        mailer.setSubject("Sensalink log file")
        mailer.mailComposeDelegate = self

        if let path = filePath {
            do {
                let data = try Data(contentsOf: path)
                mailer.addAttachmentData(data, mimeType: "application/zip", fileName: "log.zip")

                present(mailer, animated: true, completion: nil)
            } catch {
                showAlert(message: error.localizedDescription)
            }
        } else {
            showAlert(message: "Log file is missing")
        }
    }
}

extension HistoryLogViewController: MFMailComposeViewControllerDelegate {

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        
        switch result {
        case .sent: showAlert(message: "Mail sent")
        case .failed: showAlert(message: "Mail failed to sent")
        default: break
        }
    }

    private func showAlert(message: String) {
        let alert = UIAlertController(title: "Mail Result", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)

        alert.addAction(action)

        present(alert, animated: true, completion: nil)
    }
}
