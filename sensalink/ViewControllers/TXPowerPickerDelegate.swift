//
//  TXPowerPickerDelegate.swift
//  sensalink
//
//  Created by Deni Zakya on 05/07/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import UIKit

struct TXPowerPickerFactory {
    static func getDelegate(loraRegion: LoraRegion) -> TXPowerPickerDelegate {
        switch loraRegion {
        case .US:
            return USTXPowerPicker()
        default:
            return OthersTXPowerPicker()
        }
    }
}

class TXPowerPickerDelegate: NSObject, UIPickerViewDelegate, UIPickerViewDataSource {

    var selectedItem: (Int) -> Void = { _ in }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 0
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 0
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return nil
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {}
}

final class USTXPowerPicker: TXPowerPickerDelegate {

    private let items = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    override func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    override func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return items.count
    }

    override func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(items[row])"
    }

    override func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedItem(items[row])
    }
}

final class OthersTXPowerPicker: TXPowerPickerDelegate {

    private let items = [0, 1, 2, 3, 4, 5, 6, 7]

    override func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    override func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return items.count
    }

    override func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(items[row])"
    }

    override func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedItem(row)
    }
}
