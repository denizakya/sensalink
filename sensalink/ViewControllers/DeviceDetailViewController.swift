//
//  DeviceDetailViewController.swift
//  sensalink
//
//  Created by Deni Zakya on 27/04/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth
import Charts
import AVFoundation
import Photos
import ZoomImageView

struct RangeValueSensor {
    let min: Int
    let max: Int
    let isMetric: Bool
}

final class DeviceDetailViewController: UIViewController, ViewDeviceValueRefreshable, UINavigationControllerDelegate {

    @IBOutlet weak var deviceTypeLabel: UILabel!
    @IBOutlet weak var deviceNameLabel: UILabel!
    @IBOutlet weak var deviceTypeImage: UIImageView!
    @IBOutlet weak var deviceRangeValueLabel: UILabel!
    @IBOutlet weak var deviceValueLabel: UILabel!
    @IBOutlet weak var deviceUnitLabel: UILabel!
    @IBOutlet weak var gaugeBarView: GaugeBarView!
    @IBOutlet weak var chart: LineChartView!
    @IBOutlet weak var deviceImageButton: UIButton!
    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var deviceImageFull: ZoomImageView!

    var counterAngleChar: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.counterAngle.uuid]
    }
    var temperatureChar: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.temperature.uuid]
    }
    var pressureChar: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.pressure.uuid]
    }
    var modelNumberChar: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.modelNumber.uuid]
    }
    var serialNumberChar: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.serialNumber.uuid]
    }

    var deviceUUID: String?

    private var lineChartEntries: [ChartDataEntry] = []

    private let charStorage = CharacteristicStorage()
    private let deviceStorage = DeviceStorage()
    private let unitValueConverter = UnitValueConverter.shared
    private let deviceInfoFetcher = DeviceInfoFetcher()

    private weak var timer: Timer?

    deinit {
        timer?.invalidate()
        timer = nil
        NotificationCenter.default.removeObserver(self,
                                                  name: .unitChanged,
                                                  object: nil)
    }

    override func viewDidLoad() {
        deviceRangeValueLabel.text = ""
        deviceNameLabel.text = ""
        deviceTypeLabel.text = ""

        setupChart()

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(forceInitChart),
                                               name: .unitChanged, object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        restartValueUpdaterTimer()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        timer?.invalidate()
        timer = nil
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let view = segue.destination as? UpdateDeviceNameViewController,
           let uuid = deviceUUID {
            view.name = getDevice(uuid: uuid)?.name
            view.delegate = self
        }
    }

    @IBAction func cameraTap(_ sender: Any) {
        do {
            if let image = try getDeviceImage() {
                imageContainer.isHidden = false
                deviceImageFull.image = image
            } else {
                showImagePickerMode()
            }
        } catch {
            showImagePickerMode()
        }
    }

    @IBAction
    func nameTap(_ sender: Any) {
        performSegue(withIdentifier: "showUpdateDeviceName", sender: nil)
    }

    @IBAction
    func closeImageFull(_ sender: Any) {
        imageContainer.isHidden = true
    }

    @IBAction
    func changeImageTap(_ sender: Any) {
        showImagePickerMode()
    }

    func setDeviceUUID(_ uuid: String) {
        deviceUUID = uuid

        do {
            try setupDeviceImage()
        } catch { Logging.shared.log.debug("[ERROR] Device image is not exist") }

        initializeChart()
    }

    func refresh() {
        updateDeviceName()

        guard let deviceId = getDeviceId(uuid: deviceUUID) else {
            assertionFailure("[ERROR] Can't run without any device id")
            return
        }
        if let deviceInfo = getModelInfo(deviceId: deviceId) {
            self.deviceTypeLabel.text = self.getTypeLabel(model: deviceInfo.model)
            self.deviceTypeImage.image = self.getImage(model: deviceInfo.model)
            self.deviceUnitLabel.text = unitValueConverter.getConvertedUnitSymbol()

            updateDeviceValue(from: deviceInfo.model, deviceId: deviceId)
            updateGaugeBar(from: deviceInfo.model, deviceId: deviceId)
            updateChart(model: deviceInfo.model, deviceId: deviceId)
        } else {
            Logging.shared.log.error("[ERROR] Can't get deviceInfo for update Chart, Gauge & DeviceValue")
        }
    }

    private func setupDeviceImage() throws {
        guard let image = try getDeviceImage() else { return }
        deviceImageButton.setImage(image, for: .normal)
        deviceImageFull.image = image
    }

    private func getDeviceImage() throws -> UIImage? {
        guard let deviceUUID = deviceUUID,
            let path = getImageFilePath(fileName: deviceUUID) else { return nil }
        let imageData = try Data(contentsOf: path)
        return UIImage(data: imageData)
    }

    private func showImagePickerMode() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        alert.addAction(
            UIAlertAction(title: "Camera", style: .default, handler: { [weak self] _ in
                self?.checkCameraPermission {
                    DispatchQueue.main.async { [weak self] in
                        self?.showCamera()
                    }
                }
            }))

        alert.addAction(
            UIAlertAction(title: "Gallery", style: .default, handler: { [weak self] _ in
                self?.checkGalleryPermission { [weak self] in
                    DispatchQueue.main.async { [weak self] in
                        self?.showGallery()
                    }
                }
            }))

        alert.addAction(
            UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
                alert.dismiss(animated: true)
            }))

        present(alert, animated: true)
    }

    private func checkGalleryPermission(onPermit: @escaping () -> Void) {
        switch PHPhotoLibrary.authorizationStatus() {
        case .authorized, .limited: onPermit()
        case .notDetermined: askGalleryPermission { onPermit() }
        case .denied, .restricted: showAlertForGalleryPermission()
        @unknown default: showAlertForGalleryPermission()
        }
    }

    private func checkCameraPermission(onPermit: @escaping () -> Void) {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized: onPermit()
        case .notDetermined: askCameraPermission { onPermit() }
        case .denied, .restricted: showAlertForCameraPermission()
        @unknown default: showAlertForCameraPermission()
        }
    }

    private func askCameraPermission(onGranted: @escaping () -> Void) {
        AVCaptureDevice.requestAccess(for: .video) { [weak self] granted in
            if granted { onGranted() }
            else { self?.showAlertForCameraPermission() }
        }
    }

    private func askGalleryPermission(onGranted: @escaping () -> Void) {
        PHPhotoLibrary.requestAuthorization { [weak self] status in
            switch status {
            case .authorized: onGranted()
            default: self?.showAlertForGalleryPermission()
            }
        }
    }

    private func showAlertForCameraPermission() {
        //TODO
    }

    private func showAlertForGalleryPermission() {
        //TODO
    }

    private func showCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        present(imagePicker, animated: true)
    }

    private func showGallery() {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary

        imagePicker.delegate = self
        present(imagePicker, animated: true)
    }

    private func getDevice(uuid: String) -> DeviceModel? {
        return deviceStorage.get(uuid: uuid)
    }

    private func getDeviceId(uuid: String?) -> Int64? {
        guard let uuid = uuid else { return nil }
        return getDevice(uuid: uuid)?.id
    }

    private func updateCurrentDeviceName(with name: String?) {
        guard let uuid = deviceUUID,
              let deviceId = getDeviceId(uuid: uuid),
              let name = name else { return }
        deviceStorage.update(name: name, of: deviceId)
    }

    private func updateGaugeBar(from model: ModelInfo.ModelType, deviceId: Int64) {
        let modelNumberFromStorage = getModelNumber(deviceId: deviceId)
        guard let modelNumber = modelNumberFromStorage
        else {
            Logging.shared.log.error("[ERROR] unable to get model number from storage")
            return
        }
        
        do {
            guard let modelInfo = try ModelNumberParser(modelNumber: modelNumber).parse()
            else {
                Logging.shared.log.error("[ERROR] unable to parse model number from storage \(String(describing: modelNumberFromStorage)) | 1")
                return
            }
            let rangeValueUnit = RangeValueUnit(min: modelInfo.rangeWithUnit.min,
                                                max: modelInfo.rangeWithUnit.max,
                                                unit: modelInfo.rangeWithUnit.unit)

            updateDeviceUnit(modelInfo.model)
            updateUnitRangeLabel(rangeValueUnit)

            gaugeBarView.set(minValue: Float(rangeValueUnit.min),
                             maxValue: Float(rangeValueUnit.max))
            let value = getDeviceValue(from: model, deviceId: deviceId) ?? 0
            gaugeBarView.updateValue(value: value)
        } catch {
            Logging.shared.log.error("[ERROR] unable to parse model number from storage \(String(describing: modelNumberFromStorage)) | 2")
        }
    }

    private func updateDeviceUnit(_ type: ModelInfo.ModelType) {
        switch type {
        case .pressure: unitValueConverter.updateDeviceUnit(.pressure)
        case .temperature: unitValueConverter.updateDeviceUnit(.temperature)
        default: break
        }
    }

    private func getModelNumber(deviceId: Int64) -> String? {
        charStorage
            .getLatestRow(uuid: BluetoothProfile.Characteristic.modelNumber.uuid,
                          deviceId: deviceId)?
            .value.toString(encoding: .utf8)
    }

    private func needToRequestModelDevice() {
        guard let characteristic = modelNumberChar
            else {
                Logging.shared.log.error("[ERROR] model number char is nil")
                return
        }
        Logging.shared.log.verbose("ask for model device")

        BluetoothManagerWrapper.shared.updateCharacteristicValue(for: characteristic)
    }

    private func needToRequestSerialDevice() {
        guard let characteristic = serialNumberChar
            else {
                Logging.shared.log.error("[ERROR] serial number char is nil")
                return
        }
        Logging.shared.log.verbose("ask for serial device")

        BluetoothManagerWrapper.shared.updateCharacteristicValue(for: characteristic)
    }

    private func updateUnitRangeLabel(_ rangeValueUnit: RangeValueUnit) {
        deviceRangeValueLabel.text = "Range : \(rangeValueUnit.min) - \(rangeValueUnit.max) \(rangeValueUnit.unit)"
    }

    private func restartValueUpdaterTimer() {
        Logging.shared.log.verbose("Restart Timer")

        timer?.invalidate()
        timer = nil
        timer = Timer.scheduledTimer(timeInterval: 3, target: self,
                                     selector: #selector(fetchValue), userInfo: nil,
                                     repeats: true)
    }

    private func getValueFromConverter(_ value: Double, model: ModelInfo.ModelType) -> Double {
        switch model {
        case .pressure, .temperature:
            return unitValueConverter.getConvertedValue(value)
        default:
            return value
        }
    }

    @objc private func fetchValue() {
        guard let deviceId = getDeviceId(uuid: deviceUUID),
            let deviceInfo = getModelInfo(deviceId: deviceId),
            let characteristic = getActiveCharacteristic(deviceModel: deviceInfo.model)
            else {
                Logging.shared.log.error("[ERROR] Can't run without any device id")
                return
        }

        BluetoothManagerWrapper.shared.updateCharacteristicValue(for: characteristic)
    }

    private func getActiveCharacteristic(deviceModel: ModelInfo.ModelType) -> CBCharacteristic? {
        switch deviceModel {
        case .temperature:
            if let characteristic = temperatureChar {
                return characteristic
            }
        case .pressure:
            if let characteristic = pressureChar {
                return characteristic
            }
        case .valve, .vlit:
            if let characteristic = counterAngleChar {
                return characteristic
            }
        default: return nil
        }

        return nil
    }
    
    private func getModelInfo(deviceId: Int64) -> ModelInfo? {
        return deviceInfoFetcher.getModelInfo(deviceId: deviceId)
    }

    private func getSerialNumber(deviceId: Int64) -> String? {
        let serialNumber = deviceInfoFetcher.getSerialNumber(deviceId: deviceId)
        Logging.shared.log.info("[INFO] Retrieved serial number \(String(describing: serialNumber))")
        
        return serialNumber
    }

    private func updateDeviceName() {
        guard let uuid = deviceUUID, let device = getDevice(uuid: uuid)
        else { return }

        deviceNameLabel.text = device.name.isEmpty ?
            getSerialNumber(deviceId: device.id) : device.name
    }

    private func getDeviceValue(from model: ModelInfo.ModelType, deviceId: Int64) -> Float? {
        let characteristicType = getValueType(from: model)
        guard let data = charStorage
            .getLatestRow(uuid: characteristicType.uuid, deviceId: deviceId)?
            .value else { return nil }

        return dataValue(of: model, data: data)
    }

    private func updateDeviceValue(from model: ModelInfo.ModelType, deviceId: Int64) {
        if let value = getDeviceValue(from: model, deviceId: deviceId) {
            let convertedValue = getValueFromConverter(Double(value), model: model)
            deviceValueLabel.text = String(format: "%.2f",
                                           getNormalizedValue(convertedValue, model: model))
        } else {
            deviceValueLabel.text = "-"
        }
    }
    
    private func getNormalizedValue(_ value: Double, model: ModelInfo.ModelType) -> Double {
        switch model {
        case .valve, .vlit: return max(-10, min(110, value))
        default: return value
        }
    }

    private func initializeChart() {
        forceInitChart()
    }

    @objc
    private func forceInitChart() {
        if let deviceId = getDeviceId(uuid: deviceUUID),
            let deviceInfo = getModelInfo(deviceId: deviceId) {
            initChart(modelType: deviceInfo.model, deviceId: deviceId)
        } else {
            Logging.shared.log.error("[ERROR] Can't init chart without any device id")
        }
    }

    private func updateChart(model: ModelInfo.ModelType, deviceId: Int64) {
        guard let charValue = getLatestValue(from: model, deviceId: deviceId)
        else {
            Logging.shared.log.error("[ERROR] Unable to get value of model \(model.rawValue) - device id \(deviceId)")
            return
        }
        guard let value = dataValue(of: model, data: charValue.value)
            else {
                Logging.shared.log.error("[ERROR] Unable to update chart, data value is missing")
                return
        }

        let entry = ChartDataEntry(x: charValue.updateAt.timeIntervalSince1970,
                                   y: getValueFromConverter(Double(value), model: model))
        lineChartEntries.append(entry)

        updateXAxisChart()

        chart.data?.addEntry(entry, dataSetIndex: 0)
        chart.data?.notifyDataChanged()
        chart.notifyDataSetChanged()
    }

    private func initChart(modelType: ModelInfo.ModelType, deviceId: Int64) {
        let characteristicValues = getDeviceLatestValues(from: modelType, deviceId: deviceId, limit: 30)
        Logging.shared.log.info("[INFO] char values for chart is \(characteristicValues.count)-items")
        
        lineChartEntries = characteristicValues
            .map { (dataValue(of: modelType, data: $0.value), $0.updateAt) }
            .filter { $0.0 != nil }
            .map { (value, updateAt) -> ChartDataEntry in
                return ChartDataEntry(x: updateAt.timeIntervalSince1970,
                                      y: getValueFromConverter(Double(value!), model: modelType)) }

        let dataSet = LineChartDataSet(entries: lineChartEntries, label: "")
        let lineChartData = LineChartData(dataSet: dataSet)
        lineChartData.addDataSet(dataSet)
        chart.data = lineChartData

        updateXAxisChart()
    }

    private func setupChart() {
        chart.legend.form = .none
        chart.rightAxis.drawLabelsEnabled = false
        chart.xAxis.valueFormatter = ChartValueFormatter(chart: chart)
        chart.dragXEnabled = true
        chart.dragYEnabled = false
        chart.pinchZoomEnabled = true
    }

    private func updateXAxisChart() {
        let now = Date().timeIntervalSince1970
        chart.xAxis.axisMaximum = now
        chart.xAxis.axisMinimum = now - 60
    }

    private func getLatestValue(from model: ModelInfo.ModelType,
                                deviceId: Int64) -> CharacteristicModel? {
        let characteristicType = getValueType(from: model)
        return charStorage.getLatestRow(uuid: characteristicType.uuid, deviceId: deviceId)
    }

    private func getDeviceValues(from model: ModelInfo.ModelType,
                                 deviceId: Int64,
                                 after date: Date) -> [CharacteristicModel] {
        let characteristicType = getValueType(from: model)
        return charStorage
            .get(uuid: characteristicType.uuid,
                 deviceId: deviceId,
                 after: date)
    }
    
    private func getDeviceLatestValues(from model: ModelInfo.ModelType,
                                       deviceId: Int64,
                                       limit: Int) -> [CharacteristicModel] {
        let characteristicType = getValueType(from: model)
        return charStorage
            .get(uuid: characteristicType.uuid,
                 deviceId: deviceId,
                 limit: limit)
    }

    private func getValueType(from model: ModelInfo.ModelType) -> BluetoothProfile.Characteristic {
        switch model {
        case .valve, .vlit: return BluetoothProfile.Characteristic.counterAngle
        case .temperature: return BluetoothProfile.Characteristic.temperature
        case .pressure: return BluetoothProfile.Characteristic.pressure
        default: return BluetoothProfile.Characteristic.unknown
        }
    }

    private func getTypeLabel(model: ModelInfo.ModelType) -> String {
        switch model {
        case .valve, .vlit: return "Valve Position"
        case .temperature: return "Temperature"
        case .pressure: return "Pressure"
        case .unknown: return "N/A"
        }
    }

    private func getImage(model: ModelInfo.ModelType) -> UIImage? {
        switch model {
        case .valve, .vlit: return UIImage(named: "motion")
        case .temperature: return UIImage(named: "temperature")
        case .pressure: return UIImage(named: "pressure")
        case .unknown: return UIImage()
        }
    }

    private func getImageFilePath(fileName: String) -> URL? {
        do {
            let dir = try FileManager.default.url(for: .documentDirectory,
                                                  in: .userDomainMask,
                                                  appropriateFor: nil,
                                                  create: true)
                .appendingPathComponent("images")
            try FileManager.default.createDirectory(atPath: dir.path,
                                                    withIntermediateDirectories: true,
                                                    attributes: nil)
            return dir.appendingPathComponent(fileName)
        } catch(let e) {
            Logging.shared.log.error("[ERROR] creating file image: \(e.localizedDescription)")
            return nil
        }
    }
}

extension DeviceDetailViewController: UpdateDeviceNameDelegate {
    func update(name: String?, sender: UIViewController) {
        sender.dismiss(animated: true)
        updateCurrentDeviceName(with: name)
    }

    func cancel(sender: UIViewController) {
        sender.dismiss(animated: true)
    }
}

extension DeviceDetailViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.editedImage] as? UIImage else {
            Logging.shared.log.info("Edited image not found")
            return
        }

        if let deviceUUID = deviceUUID,
            let filePath = getImageFilePath(fileName: deviceUUID) {
            do {
                try image.pngData()?.write(to: filePath)
                try setupDeviceImage()
            } catch(let e) {
                Logging.shared.log.error("[ERROR] save image: \(e.localizedDescription)")
            }
        }
    }
}

final class ChartValueFormatter: IAxisValueFormatter {

    weak var chart: LineChartView?

    init(chart: LineChartView) {
        self.chart = chart
    }

    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        guard let chart = self.chart else { return "" }
        switch axis {
        case chart.xAxis:
            let date = Date(timeIntervalSince1970: value)
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm:ss"
            return formatter.string(from: date)
        default: return ""
        }
    }
}

private func dataValue(of model: ModelInfo.ModelType, data: Data) -> Float? {
    switch model {
    case .valve where data.count >= 16,
         .vlit where data.count >= 16:
        let value = data[12...15].toFloat()
        Logging.shared.log.info("[INFO] data value of valve | \(String(describing: value))")
        
        return value
    case .pressure, .temperature:
        let value = data.toFloat()
        Logging.shared.log.info("[INFO]  data value of pressure/temperature | \(String(describing: value))")
        return value
    default:
        Logging.shared.log.info("[INFO] data value of unknown type | nil ")
        return nil
    }
}
