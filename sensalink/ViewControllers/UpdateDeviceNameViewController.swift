//
//  UpdateDeviceNameViewController.swift
//  sensalink
//
//  Created by Deni Zakya on 16/01/21.
//  Copyright © 2021 sensa. All rights reserved.
//

import UIKit

protocol UpdateDeviceNameDelegate: class {
    func update(name: String?, sender: UIViewController)
    func cancel(sender: UIViewController)
}

final class UpdateDeviceNameViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    var name: String?
    weak var delegate: UpdateDeviceNameDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.text = name
        nameTextField.delegate = self
    }

    @IBAction
    private func cancel() {
        delegate?.cancel(sender: self)
    }

    @IBAction
    private func update() {
        name = nameTextField.text ?? ""
        delegate?.update(name: name, sender: self)
    }

}

extension UpdateDeviceNameViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}
