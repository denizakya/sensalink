//
//  DeviceInfoFetcher.swift
//  sensalink
//
//  Created by Deni Zakya on 16/02/21.
//  Copyright © 2021 sensa. All rights reserved.
//

import Foundation

struct DeviceInfoFetcher {
    
    private let charStorage = CharacteristicStorage()
    private let deviceStorage = DeviceStorage()
    
    func getDeviceInfo(deviceId: Int64) -> DeviceInfo? {
        guard let serialNumberString = getSerialNumber(deviceId: deviceId) else { return nil }
        guard let deviceInfo = SerialNumberParser(serialNumber: serialNumberString).parse()
        else {
            Logging.shared.log.error("[ERROR] Unable to parse serial number")
            return nil
        }

        return deviceInfo
    }

    func getSerialNumber(deviceId: Int64) -> String? {
        let serialNumberCharacteristicUUID = BluetoothProfile.Characteristic.serialNumber.uuid
        let serialNumberValue = charStorage
            .getLatestRow(uuid: serialNumberCharacteristicUUID, deviceId: deviceId)
        return serialNumberValue?.value.toString(encoding: .utf8)
    }
    
    func getDeviceId(uuid: String) -> Int64? {
        deviceStorage.get(uuid: uuid)?.id
    }
    
    func getModelNumber(deviceId: Int64) -> String? {
        charStorage
            .getLatestRow(uuid: BluetoothProfile.Characteristic.modelNumber.uuid,
                          deviceId: deviceId)?
            .value.toString(encoding: .utf8)
    }
    
    func getModelInfo(deviceId: Int64) -> ModelInfo? {
        guard let modelNumber = getModelNumber(deviceId: deviceId) else { return nil }
        do {
            return try ModelNumberParser(modelNumber: modelNumber).parse()
        } catch {
            Logging.shared.log.error("[ERROR] Unable to parse > \(modelNumber)")
            Logging.shared.log.error(error.localizedDescription)
            
            return nil
        }
    }
}
