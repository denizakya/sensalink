//
//  ContainerViewController.swift
//  sensalink
//
//  Created by Deni Zakya on 10/05/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth

final class ContainerViewController : UIViewController {

    var peripheral: CBPeripheral?
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var measureLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var configLabel: UILabel!
    @IBOutlet weak var tabView: UIView!

    weak var pageDetailViewController: PageDetailViewController?

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? PageDetailViewController,
            segue.identifier == "showPageDetail" {
            viewController.peripheral = peripheral
            viewController.onViewControllerIndexChanged = { [weak self] index in
                self?.onIndexChanged(index: index)
            }
            pageDetailViewController = viewController
            pageDetailViewController?.setRootNavigationItem(item: self.navigationItem)
        }
    }

    @IBAction func measureTabTapped(_ sender: Any) {
        pageDetailViewController?.move(to: 0)
    }

    @IBAction func statusTabTapped(_ sender: Any) {
        pageDetailViewController?.move(to: 1)
    }

    @IBAction func configTabTapped(_ sender: Any) {
        pageDetailViewController?.move(to: 2)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "SENSALINK"
        navigationController?.navigationBar.shadowImage = UIImage()
        tabView.backgroundColor = UIColor(red: 115.0/255.0,
                                          green: 105.0/255.0,
                                          blue: 151.0/255.0,
                                          alpha: 1)

        navigationItem.hidesBackButton = true
        showCloseButton()
    }

    @objc
    private func close() {
        let alert = UIAlertController(title: nil,
                                      message: "Do you want to disconnect the device?",
                                      preferredStyle: .alert)
        alert.addAction(
            UIAlertAction(title: "Disconnect",
                          style: .destructive,
                          handler: { [unowned self] _ in
                            self.navigationController?.popViewController(animated: true)
                          }))
        alert.addAction(
            UIAlertAction(title: "Cancel",
                          style: .cancel,
                          handler: { _ in
                            alert.dismiss(animated: true)
                          })
        )
        self.present(alert, animated: true)
    }

    private func showCloseButton() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Disconnect",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(close))
    }

    private func onIndexChanged(index: Int) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 3,
                       options: .curveEaseOut,
                       animations: { [unowned self] in
                        switch index {
                        case 0:
                            self.indicatorView.transform = .identity
                        case 1:
                            self.indicatorView.transform =
                                CGAffineTransform(translationX: self.indicatorView.frame.width, y: 0)
                        case 2:
                            self.indicatorView.transform =
                                CGAffineTransform(translationX: self.indicatorView.frame.width * 2, y: 0)
                        default: break
                        }

                        self.measureLabel.textColor = index == 0 ? .white : .lightGray
                        self.statusLabel.textColor = index == 1 ? .white : .lightGray
                        self.configLabel.textColor = index == 2 ? .white : .lightGray
            })
    }
}
