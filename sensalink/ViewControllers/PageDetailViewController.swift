//
//  PageDetailViewController.swift
//  sensalink
//
//  Created by Deni Zakya on 01/05/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth

protocol ViewDeviceValueRefreshable {
    func refresh()
    func setDeviceUUID(_ uuid: String)
}

extension UIViewController {
    @objc func setRootNavigationItem(item: UINavigationItem?) {}
    @objc func backToMain() {}
}

final class PageDetailViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    var peripheral: CBPeripheral?

    private let charStorage = CharacteristicStorage()
    private let deviceInfoFetcher = DeviceInfoFetcher()

    lazy var detailDeviceView: DeviceDetailViewController = {
        let detailDeviceView = storyboard!
            .instantiateViewController(withIdentifier: "DeviceDetailViewController") as! DeviceDetailViewController
        return detailDeviceView
    }()
    lazy var additionalInfoView: DeviceAdditionalInfoViewController = {
        let additionalInfoView = storyboard!
            .instantiateViewController(withIdentifier: "DeviceAdditionalInfoViewController") as! DeviceAdditionalInfoViewController
        return additionalInfoView
    }()
    lazy var deviceConfigView: DeviceConfigViewController = {
        let configView = storyboard!
            .instantiateViewController(withIdentifier: "DeviceConfigViewController") as! DeviceConfigViewController
        return configView
    }()

    var knownPeripheralServices: [BluetoothService]!
    var allViewControllers: [UIViewController]!

    var onViewControllerIndexChanged: (Int) -> Void = { _ in }
    var sendUpdateCharValue: (CBCharacteristic, Data) -> Void = { _,_ in }

    private var configCharacteristic: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.motionConfig.uuid]
    }

    private var counterAngleCharacteristic: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.counterAngle.uuid]
    }

    private var calibrationStatusCharacteristic: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.calibrationStatus.uuid]
    }
    
    private var temperatureCalibrationCharacteristic: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.temperatureCalibration.uuid]
    }

    private var pressureValueCharacteristic: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.pressure.uuid]
    }

    private var temperatureValueCharacteristic: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.temperature.uuid]
    }

    private var modelNumberCharacteristic: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.modelNumber.uuid]
    }

    private var serialNumberCharacteristic: CBCharacteristic? {
        AvailableCharacteristic.shared.characteristics[BluetoothProfile.Characteristic.serialNumber.uuid]
    }

    private var rootNavigationItem: UINavigationItem?

    private var calibrationLaterTap = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        deviceConfigView.setRootNavigationItem(item: rootNavigationItem)
        allViewControllers = [detailDeviceView, additionalInfoView, deviceConfigView]
        dataSource = self
        delegate = self
        setViewControllers([allViewControllers.first!], direction: .forward, animated: false, completion: nil)

        knownPeripheralServices = BluetoothServiceFactory.create()

        BluetoothManagerWrapper.shared.onUpdateCharacteristicValue = { [weak self] characteristic, data in
            guard let `self` = self else { return }
            self.onUpdateCharacteristicValue(characteristic, data: data)
        }

        BluetoothManagerWrapper.shared.discoverServices(uuids: nil)

        additionalInfoView.executeCalibrate = { [unowned self] in
            self.showMotionCalibration()
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        BluetoothManagerWrapper.shared.cancelCurrentPeripheralConnection()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        prepareForCalibration(segue.destination)
    }

    func move(to index: Int) {
        guard index < allViewControllers.count else { return }
        
        setViewControllers([allViewControllers[index]], direction: .forward, animated: false, completion: nil)
        onViewControllerIndexChanged(index)
    }

    override func setRootNavigationItem(item: UINavigationItem?) {
        rootNavigationItem = item
    }

    private func getDeviceId(uuid: String?) -> Int64? {
        guard let uuid = uuid else { return nil }
        return deviceInfoFetcher.getDeviceId(uuid: uuid)
    }

    private func prepareForCalibration(_ destination: UIViewController) {
        guard let deviceUUID = peripheral?.identifier.uuidString.lowercased() else { return }
        if let deviceId = getDeviceId(uuid: deviceUUID),
            let destination = destination as? MotionCalibrationViewController,
            let serialNumber = getSerialNumber(deviceId: deviceId) {
            
            if let deviceInfo = SerialNumberParser(serialNumber: serialNumber).parse() {
                switch deviceInfo.type {
                case .sensaio: destination.deviceType = .sensaio
                case .lite: destination.deviceType = .lite
                case .sat: break
                }
            }
        }
        
        if let destination = destination as? TemperatureCalibrationRTDViewController {
            destination.deviceUUID = deviceUUID
        }
        
        if let charValueRefreshable = destination as? CharacteristicValueRefreshable {
            sendUpdateCharValue =  { [weak charValueRefreshable] characteristic, data in
                charValueRefreshable?.onUpdateCharacteristicValue(characteristic: characteristic, data: data)
            }
        }
    }

    private func getSerialNumber(deviceId: Int64) -> String? {
        deviceInfoFetcher.getSerialNumber(deviceId: deviceId)
    }
    
    private func getModelNumber(deviceId: Int64) -> String? {
        deviceInfoFetcher.getModelNumber(deviceId: deviceId)
    }

    private func onUpdateCharacteristicValue(_ characteristic: CBCharacteristic, data: Data) {
        guard let deviceUUID = BluetoothManagerWrapper.shared.connectedPeripheral?.identifier.uuidString.lowercased() else { return }
        let characteristicUUID = characteristic.uuid.uuidString.lowercased()
        updateCharacteristicValue(deviceUUID: deviceUUID,
                                  characteristicUUID: characteristicUUID,
                                  data: data)
        updateView()

        checkForDeviceCalibration()
        sendUpdateCharValue(characteristic, data)
    }

    private func showMotionCalibration() {
        guard let _ = calibrationStatusCharacteristic else {
            Logging.shared.log.error("[ERROR] Can't show Motion Calibration when calibration char is nil")
            return
        }
        performSegue(withIdentifier: "showMotionCalibration", sender: nil)
    }
    
    private func showTemperatureCalibration() {
        guard let _ = temperatureCalibrationCharacteristic else {
            Logging.shared.log.error("[ERROR] Can't show Temperature Calibration when calibration char is nil")
            return
        }
        
        switch getTemperatureType() {
        case .TYK: calibrateTYK()
        case .RTD: performSegue(withIdentifier: "showTemperatureCalibration", sender: nil)
        case .none: Logging.shared.log.error("[ERROR] Temperature type undefined")
        }
    }
    
    private func calibrateTYK() {
        guard let characteristic = AvailableCharacteristic.shared
                .characteristics[BluetoothProfile.Characteristic.temperatureCalibrationCMD.uuid]
        else { return }
        
        let loadingView = LoadingView.instance
        loadingView.show(in: self.view)
        
        BluetoothManagerWrapper.shared
            .writeCharacteristicValue(value: Data([0x01]),
                                      characteristic: characteristic)
        
        loadingView.hide(after: 1) { [weak self] in
            Logging.shared.log.info("[Temp] TYK Calibration done")
            
            if let deviceUUID = self?.peripheral?.identifier.uuidString.lowercased(),
               let deviceId = self?.getDeviceId(uuid: deviceUUID) {
                CalibrationStatusStorage()
                    .insert(deviceId: deviceId, isCalibrated: true)
            }
        }
    }
    
    private func getTemperatureType() -> TemperatureType? {
        guard let deviceUUID = getDeviceUUID(),
            let deviceId = deviceInfoFetcher.getDeviceId(uuid: deviceUUID)
        else { return nil }
        return deviceInfoFetcher.getModelInfo(deviceId: deviceId)?.getTemperatureType()
    }

    private func checkForDeviceCalibration() {
        guard let deviceId = getDeviceId(uuid: getDeviceUUID()),
              let modelNumber = getModelNumber(deviceId: deviceId)
        else {
            Logging.shared.log.error("[ERROR] Unable to parse device serial number")
            return
        }
        
        do {
            let deviceInfo = try ModelNumberParser(modelNumber: modelNumber).parse()
            if !isDeviceCalibrated() && calibrationLaterTap == false {
                switch deviceInfo?.model {
                case .valve, .vlit: showAlertCalibration()
                case .temperature: showAlertTemperatureCalibration()
                default: Logging.shared.log.info("[INFO] Device can't be calibrated")
                }
            } else if calibrationLaterTap {
                Logging.shared.log.info("[INFO] Calibration is skipped by user")
            }
        } catch {
            Logging.shared.log.error("[ERROR] Unable to parse model number.")
        }
    }
    
    private func getDeviceUUID() -> String? { peripheral?.identifier.uuidString.lowercased() }

    private func isDeviceCalibrated() -> Bool {
        let calibrationStatusStorage = CalibrationStatusStorage()
        if  let deviceUUID = peripheral?.identifier.uuidString.lowercased(),
            let deviceId = getDeviceId(uuid: deviceUUID) {
            return calibrationStatusStorage.isCalibrated(for: deviceId)
        }
        
        return false
    }

    private func showAlertCalibration() {
        guard self.presentedViewController == nil else { return }

        let alert = UIAlertController(title: nil, message: "Please calibrate your device",
                                      preferredStyle: .alert)
        alert.addAction(.init(title: "Calibrate Now", style: .default, handler: { _ in
            self.showMotionCalibration()
        }))
        alert.addAction(.init(title: "Later", style: .default, handler: { _ in
            self.calibrationLaterTap = true
            alert.dismiss(animated: true, completion: nil)
        }))

        self.present(alert, animated: true, completion: nil)
    }
    
    private func showAlertTemperatureCalibration() {
        guard self.presentedViewController == nil else { return }

        let alert = UIAlertController(title: nil, message: "Please calibrate your device",
                                      preferredStyle: .alert)
        alert.addAction(.init(title: "Calibrate Now", style: .default, handler: { _ in
            self.showTemperatureCalibration()
        }))
        alert.addAction(.init(title: "Later", style: .default, handler: { _ in
            self.calibrationLaterTap = true
            alert.dismiss(animated: true, completion: nil)
        }))

        self.present(alert, animated: true, completion: nil)
    }

    private func updateCharacteristicValue(deviceUUID: String, characteristicUUID: String, data: Data) {
        if let deviceId = getDeviceId(uuid: deviceUUID) {
            let model = CharacteristicModel(uuid: characteristicUUID,
                                            id: 0,
                                            deviceId: deviceId,
                                            updateAt: Date(),
                                            value: data)
            charStorage.insert(model)
        } else { Logging.shared.log.error("[ERROR] Unable insert characteristic value to fetch device id ") }
    }

    func updateView() {
        DispatchQueue.main.async { [weak self]  in
            guard let `self` = self else { return }
            
            for view in self.allViewControllers {
                if let refreshable = view as? ViewDeviceValueRefreshable {
                    refreshable.setDeviceUUID(self.peripheral?.identifier.uuidString.lowercased() ?? "")
                    refreshable.refresh()
                }
            }
        }
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let currentIndex = allViewControllers.firstIndex(of: viewController) {
            let index = currentIndex.advanced(by: -1)
            if index > -1 && index < allViewControllers.count {
                return allViewControllers[index]
            }
        }

        return nil
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let currentIndex = allViewControllers.firstIndex(of: viewController) {
            let index = currentIndex.advanced(by: 1)
            if index > -1 && index < allViewControllers.count {
                return allViewControllers[index]
            }
        }

        return nil
    }

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let currentIndex = getIndex(from: pageViewController.viewControllers?.first),
            completed {
            onViewControllerIndexChanged(currentIndex)
            if currentIndex < 2 {
                rootNavigationItem?.leftBarButtonItem = nil
                deviceConfigView.backToMain()
            }
        }
    }

    private func getIndex(from view: UIViewController?) -> Int? {
        guard let view = view else { return nil }

        if let _ = view as? DeviceDetailViewController {
            return 0
        }
        if let _ = view as? DeviceAdditionalInfoViewController {
            return 1
        }
        if let _ = view as? DeviceConfigViewController {
            return 2
        }

        return nil
    }
}
