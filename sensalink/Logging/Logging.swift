//
//  Logging.swift
//  sensalink
//
//  Created by Deni Zakya on 26/04/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import XCGLogger

let useDeveloperMode = false

struct Logging {

    static let shared = Logging()
    private let logger = XCGLogger.default
    var log: XCGLogger {
        return logger
    }

    #if DEBUG
    private let logLevel = XCGLogger.Level.debug
    #else
    private let logLevel = XCGLogger.Level.info
    #endif

    func setup() {

        guard let logPath = logPath else { return }

        let fileDestination = AutoRotatingFileDestination(
            writeToFile: logPath as Any,
            identifier: "SensaLink.Log",
            shouldAppend: true)
        fileDestination.logQueue = XCGLogger.logQueue
        fileDestination.targetMaxFileSize = 20 * 1024 * 1024
        fileDestination.targetMaxLogFiles = 3
        fileDestination.outputLevel = .info
        fileDestination.showLogIdentifier = false
        fileDestination.showFunctionName = false
        fileDestination.showThreadName = false
        fileDestination.showLevel = false
        fileDestination.showFileName = false
        fileDestination.showLineNumber = false
        fileDestination.showDate = true

        logger.setup(level: logLevel, fileLevel: .info)
        logger.add(destination: fileDestination)
        logger.verbose("Log Path \(String(describing: logPath))")
    }

    private func createLogPath(for filename: String) -> URL? {
        guard let logDirectory = logDirectory else { return nil }
        return logDirectory.appendingPathComponent(filename)
    }

    var logPath : URL? {
        createLogPath(for: "sensalink.log")
    }

    var logDirectory : URL? {
        do {
            let dir = try FileManager.default.url(for: .documentDirectory,
                                                  in: .userDomainMask,
                                                  appropriateFor: nil,
                                                  create: true)
                .appendingPathComponent("logs")
            try FileManager.default.createDirectory(atPath: dir.path,
                                                    withIntermediateDirectories: true,
                                                    attributes: nil)
            return dir
        } catch(let e) {
            log.error("[ERROR] creating file log: \(e.localizedDescription)")
            return nil
        }
    }

}
