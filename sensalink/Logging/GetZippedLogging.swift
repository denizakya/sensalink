//
//  GetZippedLogging.swift
//  sensalink
//
//  Created by Deni Zakya on 13/06/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import ZIPFoundation

typealias ZipFilePath = (URL?) -> Void
final class GetZippedLogging {

    func get(_ completion: @escaping ZipFilePath) throws {
        DispatchQueue.global(qos: .userInitiated)
            .async { [unowned self] in
                do {
                    let filePath = try self.zipLogFile()
                    completion(filePath)
                } catch {
                    print(error.localizedDescription)
                    completion(nil)
                }
        }
    }

    private func zipLogFile() throws -> URL {
        let zippedDir = try FileManager.default.url(for: .cachesDirectory,
                                                    in: .userDomainMask,
                                                    appropriateFor: nil,
                                                    create: true)
        let zippedLogFilePath = zippedDir.appendingPathComponent("zippedLog.zip",
                                                                 isDirectory: false)

        if FileManager.default.fileExists(atPath: zippedLogFilePath.path) {
            try FileManager.default.removeItem(at: zippedLogFilePath)
        }

        let documentDir = try FileManager.default.url(for: .documentDirectory,
                                                      in: .userDomainMask,
                                                      appropriateFor: nil,
                                                      create: false)

        try FileManager().zipItem(at: documentDir, to: zippedLogFilePath)

        return zippedLogFilePath
    }
}
