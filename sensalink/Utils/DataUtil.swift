//
//  DataUtil.swift
//  sensalink
//
//  Created by Deni Zakya on 22/04/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation

extension Data {
    func toFloat() -> Float? {
        self.count < 4 ? nil
            : withUnsafeBytes { $0.load(as: Float.self) }
    }

    func toUInt8() -> UInt8? {
        self.count == 0 ? nil
            : withUnsafeBytes { $0.load(as: UInt8.self) }
    }

    func toUInt16() -> UInt16? {
        self.count == 0 ? nil
            : withUnsafeBytes { $0.load(as: UInt16.self) }
    }
    
    func toUInt32() -> UInt32? {
        self.count == 0 ? nil
            : withUnsafeBytes { $0.load(as: UInt32.self) }
    }

    func toUInt64() -> UInt64? {
        self.count == 0 ? nil
            : withUnsafeBytes { $0.load(as: UInt64.self) }
    }

    func toInt16() -> Int16? {
        self.count == 2 ? withUnsafeBytes { $0.load(as: Int16.self) } : nil
    }

    func toString(encoding: String.Encoding) -> String? {
        String(data: self, encoding: .utf8)
    }

    func toHexString() -> String {
        map { String(format: "%02x", $0) }.joined()
    }

    static func from(_ value: Float) -> Data {
        var tempValue = value
        return .init(bytes: &tempValue, count: MemoryLayout<Float>.size)
    }

    static func from(_ value: UInt16) -> Data {
        var tempValue = value
        return .init(bytes: &tempValue, count: MemoryLayout<UInt16>.size)
    }

    static func from<T>(_ value: T) -> Data {
        var tempValue = value
        return .init(bytes: &tempValue, count: MemoryLayout<T>.size)
    }

    mutating func replaced(with newValue: Data, from offset: Int) {
        guard self.count > offset + (newValue.count - 1) else { return }

        for index in newValue.indices {
            self[offset + index] = newValue[index]
        }
    }

    public init(hex: String) {
        self.init(Array<UInt8>(hex: hex))
    }
}

extension Array where Element == UInt8 {
    public init(hex: String) {
        self.init()

        var buffer: UInt8?
        var skip = hex.hasPrefix("0x") ? 2 : 0
        for char in hex.unicodeScalars.lazy {
            guard skip == 0 else {
                skip -= 1
                continue
            }
            guard char.value >= 48 && char.value <= 102 else {
                removeAll()
                return
            }
            let v: UInt8
            let c: UInt8 = UInt8(char.value)
            switch c {
            case let c where c <= 57:
                v = c - 48
            case let c where c >= 65 && c <= 70:
                v = c - 55
            case let c where c >= 97:
                v = c - 87
            default:
                removeAll()
                return
            }
            if let b = buffer {
                append(b << 4 | v)
                buffer = nil
            } else {
                buffer = v
            }
        }
        if let b = buffer {
            append(b)
        }
    }

    public func toHexString() -> String {
        `lazy`.reduce(into: "") {
            var s = String($1, radix: 16)
            if s.count == 1 {
                s = "0" + s
            }
            $0 += s
        }
    }
}

