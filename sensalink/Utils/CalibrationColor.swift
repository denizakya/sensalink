//
//  CalibrationColor.swift
//  sensalink
//
//  Created by Deni Zakya on 09/04/21.
//  Copyright © 2021 sensa. All rights reserved.
//

import Foundation
import UIKit

enum CalibrationColor: Int {
    case inProgress = 1, complete, failed
    
    var color: UIColor {
        switch self {
        case .inProgress: return .yellow
        case .complete: return .green
        case .failed: return .red
        }
    }
}
