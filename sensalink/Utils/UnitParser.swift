//
//  UnitParser.swift
//  sensalink
//
//  Created by Deni Zakya on 18/07/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation

enum UnitType: Equatable {
    case metric
    case imperial
}

extension UnitTemperature {
    func value(_ value: Double, to unit: UnitTemperature) -> Double? {
        let measurement = NSMeasurement(doubleValue: value, unit: self)
        guard measurement.canBeConverted(to: unit) else { return nil }

        var convertedValue = Decimal(floatLiteral:  measurement.converting(to: unit).value)
        var roundedValue = Decimal()
        NSDecimalRound(&roundedValue, &convertedValue, 3, .plain)

        return NSDecimalNumber(decimal: roundedValue).doubleValue
    }
}

extension UnitPressure {
    func value(_ value: Double, to unit: UnitPressure) -> Double? {
        let measurement = NSMeasurement(doubleValue: value, unit: self)
        guard measurement.canBeConverted(to: unit) else { return nil }

        var convertedValue = Decimal(floatLiteral:  measurement.converting(to: unit).value)
        var roundedValue = Decimal()
        NSDecimalRound(&roundedValue, &convertedValue, 3, .down)

        return NSDecimalNumber(decimal: roundedValue).doubleValue
    }
}
