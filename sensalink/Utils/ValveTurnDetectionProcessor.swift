//
//  ValveTurnDetectionProcessor.swift
//  sensalink
//
//  Created by Deni Zakya on 03/07/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation

struct ValveTurnDetectionProcessor {
    let totalAngle: Float

    func toString() -> String {
        let turnValue = totalAngle / 360

        let realNumber = Int(floor(turnValue))
        let decimalNumber = turnValue.truncatingRemainder(dividingBy: 1)
        let remainderString = getRemainderString(remainder: decimalNumber)

        if realNumber == 0 { return "\(remainderString)" }
        if remainderString == "1" { return "\(realNumber + 1)" }

        return "\(realNumber) \(remainderString)"
    }

    func getTurnValue() -> Float {
        return totalAngle / 360
    }

    private func getRemainderString(remainder: Float) -> String {
        if remainder > 0.125 && remainder < 0.375 { return "1/4" }
        if remainder >= 0.375 && remainder < 0.625 { return "1/2" }
        if remainder >= 0.625 && remainder < 0.875 { return "3/4" }
        if remainder >= 0.875 { return "1" }
        return "0"
    }
}
