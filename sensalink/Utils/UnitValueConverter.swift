//
//  UnitConverter.swift
//  sensalink
//
//  Created by Deni Zakya on 19/07/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation

enum UnitValueKind {
    case pressure
    case temperature
}

final class UnitValueConverter {
    static let shared = UnitValueConverter()

    var selectedUnitType: UnitType? {
        get {
            return UnitTypeSelection().get()
        }
        set {
            if let newValue = newValue {
                UnitTypeSelection().set(newValue)
            }
        }
    }

    private var currentUnit: Unit?
    private(set) var currentUnitType: UnitType?

    func updateDeviceUnit(_ kind: UnitValueKind) {
        switch kind {
        case .pressure: currentUnit = UnitPressure.bars
        case .temperature: currentUnit = UnitTemperature.kelvin
        }

        currentUnitType = .metric
    }

    func getConvertedValue(_ value: Double) -> Double {
        switch selectedUnitType {
        case .imperial: return calculateToImperial(value)
        case .metric: return calculateToMetric(value)
        case .none: return value
        }
    }

    func getConvertedUnitSymbol() -> String {
        switch selectedUnitType {
        case .imperial: return symbolOfImperial()
        case .metric: return symbolOfMetric()
        case .none: return ""
        }
    }

    private func symbolOfImperial() -> String {
        if let _ = currentUnit as? UnitTemperature {
            return UnitTemperature.fahrenheit.symbol
        } else if let _ = currentUnit as? UnitPressure {
            return UnitPressure.poundsForcePerSquareInch.symbol
        } else {
            return ""
        }
    }

    private func symbolOfMetric() -> String {
        if let _ = currentUnit as? UnitTemperature {
            return UnitTemperature.celsius.symbol
        } else if let _ = currentUnit as? UnitPressure {
            return UnitPressure.bars.symbol
        } else {
            return ""
        }
    }

    private func calculateToImperial(_ value: Double) -> Double {
        guard let currentUnit = currentUnit else { return value }
        if let unit = currentUnit as? UnitTemperature {
            return unit.value(value, to: .fahrenheit) ?? value
        } else if let unit = currentUnit as? UnitPressure {
            return unit.value(value, to: .poundsForcePerSquareInch) ?? value
        } else {
            return value
        }
    }

    private func calculateToMetric(_ value: Double) -> Double {
        guard let currentUnit = currentUnit else { return value }

        if let unit = currentUnit as? UnitTemperature {
            return unit.value(value, to: .celsius) ?? value
        } else if let unit = currentUnit as? UnitPressure {
            return (unit.value(value, to: .bars) ?? value)
        } else {
            return value
        }
    }

}
