//
//  DeviceItemCell.swift
//  sensalink
//
//  Created by Deni Zakya on 25/04/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import UIKit

final class DeviceItemCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var identifierLabel: UILabel!
    @IBOutlet weak var signalStrengthView: SignalStrengthView!

    func setup(name: String, identifier: String, rssi: NSNumber) {
        nameLabel.text = name
        identifierLabel.text = identifier
        signalStrengthView.setup(rssi: rssi)
    }

}
