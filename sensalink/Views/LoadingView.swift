//
//  LoadingView.swift
//  sensalink
//
//  Created by Deni Zakya on 31/08/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import UIKit

final class LoadingView: UIView {

    init(rect: CGRect) {
        super.init(frame: rect)

        let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.startAnimating()
        addSubview(activityIndicator)

        activityIndicator.center = self.center
        clipsToBounds = true

        layer.cornerRadius = 4
        backgroundColor = .lightGray
    }

    static var instance: LoadingView {
        return LoadingView(rect: .init(x: 0, y: 0, width: 100, height: 100))
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func show(in view: UIView) {
        view.addSubview(self)
        self.center = view.center
    }

    func hide(after second: TimeInterval = 1, postHideAction postExecute: @escaping () -> Void = {}) {
        DispatchQueue.main.asyncAfter(deadline: .now() + second) { [weak self] in
            self?.removeFromSuperview()
            postExecute()
        }
    }
}
