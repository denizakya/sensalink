//
//  SignalStrengthView.swift
//  sensalink
//
//  Created by Deni Zakya on 25/04/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import UIKit

final class SignalStrengthView: UIView {

    @IBOutlet weak var oneSignalView: UIView!
    @IBOutlet weak var twoSignalView: UIView!
    @IBOutlet weak var threeSignalView: UIView!
    @IBOutlet weak var fourSignalView: UIView!
    @IBOutlet weak var fiveSignalView: UIView!

    func setup(rssi RSSI: NSNumber) {
        switch RSSI.intValue.signalStrength {
        case .none: toggleSignalHidden(one: true, two: true, three: true, four: true, five: true)
        case .one: toggleSignalHidden(one: false, two: true, three: true, four: true, five: true)
        case .two: toggleSignalHidden(one: false, two: false, three: true, four: true, five: true)
        case .three: toggleSignalHidden(one: false, two: false, three: false, four: true, five: true)
        case .four: toggleSignalHidden(one: false, two: false, three: false, four: false, five: true)
        case .five: toggleSignalHidden(one: false, two: false, three: false, four: false, five: false)
        }
    }

    private func toggleSignalHidden(one: Bool, two: Bool, three: Bool, four: Bool, five: Bool) {
        oneSignalView.isHidden = one
        twoSignalView.isHidden = two
        threeSignalView.isHidden = three
        fourSignalView.isHidden = four
        fiveSignalView.isHidden = five
    }

}

enum SignalStrength {
    case none, one, two, three, four, five
}

private extension Int {
    var signalStrength: SignalStrength {
        if self < -100 {
            return .none
        } else if self < -80 {
            return .one
        } else if self < -60 {
            return .two
        } else if self < -40 {
            return .three
        } else if self < -20 {
            return .four
        } else if self >= -20 {
            return .five
        } else {
            return .none
        }
    }
}
