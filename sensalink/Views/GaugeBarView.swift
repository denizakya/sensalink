//
//  GaugeBarView.swift
//  sensalink
//
//  Created by Deni Zakya on 17/05/20.
//  Copyright © 2020 sensa. All rights reserved.
//

import Foundation
import UIKit

final class GaugeBarView: UIView {
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var rightMargin: NSLayoutConstraint!
    @IBOutlet weak var barView: UIView!

    private var minValue: Float = 0
    private var maxValue: Float = 1

    func set(minValue: Float, maxValue: Float) {
        assert(maxValue > minValue)
        
        self.minValue = minValue
        self.maxValue = maxValue
    }

    func updateValue(value: Float) {
        guard maxValue > minValue else {
            Logging.shared.log.warning("Can't update gauge value, maxValue is equal or less than minValue")
            return
        }

        if barView == nil { return }
        let maxWidth = barView.frame.width

        let percentage = (value - minValue) / (maxValue - minValue)

        if percentageLabel == nil { return }
        if percentage.isNaN {
            Logging.shared.log.error("[ERROR] Value percentage of gauge is Nan")
            return
        }
        if percentage.isInfinite {
            Logging.shared.log.error("[ERROR] Value percentage of gauge is Infinite")
            return
        }

        let marginValue = maxWidth - (maxWidth * CGFloat(percentage))
        if marginValue.isNaN == false &&
            marginValue.isInfinite == false &&
            rightMargin != nil {
            rightMargin.constant = marginValue
        }

        let value = normalizeValue(percentage * 100)
        percentageLabel.text = "\(String(format: "%.0f", value)) %"
    }
    
    private func normalizeValue(_ value: Float) -> Float {
        if value < -10 { return -10 }
        if value > 110 { return 110 }
        return value
    }
}
